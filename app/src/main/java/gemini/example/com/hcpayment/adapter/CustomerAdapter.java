package gemini.example.com.hcpayment.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.entity.CustomBean;

/**
 * Created by Gemini on 2018/5/21.
 */

public class CustomerAdapter  extends BaseAdapter<CustomBean.DataBean.ListBean,BaseAdapter.BaseViewHolder>{
    private String TAG="CustomerAdapter";
    public CustomerAdapter(List<CustomBean.DataBean.ListBean> data, int layoutId) {
        super(data, layoutId);
    }


    @Override
    void bindViewHolder(BaseAdapter.BaseViewHolder holder, CustomBean.DataBean.ListBean data, int position) {
        holder.setText(R.id.guestInfoName,data.getName());
        holder.setText(R.id.guestInfoAddress,data.getAddress());
        holder.setText(R.id.guestInfoPhone,data.getMobile());
    }

}
