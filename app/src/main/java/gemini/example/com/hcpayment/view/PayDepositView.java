package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.KeepDepositBean;
import gemini.example.com.hcpayment.entity.TransactionInfo;

/**
 * Created by Gemini on 2018/5/30.
 */

public interface PayDepositView  extends BaseView{
    void keepDeposit(int customId,String money,int customHouseId);
    void callbackDepositInfo(KeepDepositBean keepDepositBean);
    void callbackTransactionStatus(TransactionInfo transactionInfo);
    void callbackTransactionFail();
}
