package gemini.example.com.hcpayment.entity;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/3/30.
 */

public class LoginBean extends BaseResponse {


    /**
     * data : {"user":{"id":1,"username":"test","name":"江金鑫","addAuthority":"","reduceAuthority":"","createTime":"2018-05-16 08:04:05","updateTime":"2018-05-16 08:04:05"},"token":"4Ludnn79kP9rf7d4TvXFCw=="}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "LoginBean{" +
                "data=" + data +
                '}';
    }

    public static class DataBean {
        /**
         * user : {"id":1,"username":"test","name":"江金鑫","addAuthority":"","reduceAuthority":"","createTime":"2018-05-16 08:04:05","updateTime":"2018-05-16 08:04:05"}
         * token : 4Ludnn79kP9rf7d4TvXFCw==
         */

        private UserBean user;
        private String token;

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "user=" + user +
                    ", token='" + token + '\'' +
                    '}';
        }

        public static class UserBean {
            @Override
            public String toString() {
                return "UserBean{" +
                        "id=" + id +
                        ", username='" + username + '\'' +
                        ", name='" + name + '\'' +
                        ", addAuthority='" + addAuthority + '\'' +
                        ", reduceAuthority='" + reduceAuthority + '\'' +
                        ", createTime='" + createTime + '\'' +
                        ", updateTime='" + updateTime + '\'' +
                        '}';
            }

            /**
             * id : 1
             * username : test
             * name : 江金鑫
             * addAuthority :
             * reduceAuthority :
             * createTime : 2018-05-16 08:04:05
             * updateTime : 2018-05-16 08:04:05
             */

            private int id;
            private String username;
            private String name;
            private String addAuthority;
            private String reduceAuthority;
            private String createTime;
            private String updateTime;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAddAuthority() {
                return addAuthority;
            }

            public void setAddAuthority(String addAuthority) {
                this.addAuthority = addAuthority;
            }

            public String getReduceAuthority() {
                return reduceAuthority;
            }

            public void setReduceAuthority(String reduceAuthority) {
                this.reduceAuthority = reduceAuthority;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }
        }
    }
}
