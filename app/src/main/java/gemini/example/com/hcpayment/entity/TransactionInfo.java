package gemini.example.com.hcpayment.entity;

import java.io.Serializable;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/6/13.
 */

public class TransactionInfo extends BaseResponse implements Serializable {

    /**
     * data : {"customMobile":"","orderNo":"SP20180613174806421PWF","money":50000,"payTime":"","shopName":"杭州直营店","customName":"范建海","id":1,"shopNo":"HCZJ345","payStatus":1,"operator":"刘颜菘","serialNo":""}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * customMobile :
         * orderNo : SP20180613174806421PWF
         * money : 50000.0
         * payTime :
         * shopName : 杭州直营店
         * customName : 范建海
         * id : 1
         * shopNo : HCZJ345
         * payStatus : 1
         * operator : 刘颜菘
         * serialNo :
         */

        private String customMobile;
        private String orderNo;
        private double money;
        private String payTime;
        private String shopName;
        private String customName;
        private int id;
        private String shopNo;
        private int payStatus;
        private String operator;
        private String serialNo;
        private String card;
        public String getCard() {
            return card;
        }

        public void setCard(String card) {
            this.card = card;
        }

        public String getCustomMobile() {
            return customMobile;
        }

        public void setCustomMobile(String customMobile) {
            this.customMobile = customMobile;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

        public String getPayTime() {
            return payTime;
        }

        public void setPayTime(String payTime) {
            this.payTime = payTime;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getCustomName() {
            return customName;
        }

        public void setCustomName(String customName) {
            this.customName = customName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getShopNo() {
            return shopNo;
        }

        public void setShopNo(String shopNo) {
            this.shopNo = shopNo;
        }

        public int getPayStatus() {
            return payStatus;
        }

        public void setPayStatus(int payStatus) {
            this.payStatus = payStatus;
        }

        public String getOperator() {
            return operator;
        }

        public void setOperator(String operator) {
            this.operator = operator;
        }

        public String getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(String serialNo) {
            this.serialNo = serialNo;
        }
    }
}
