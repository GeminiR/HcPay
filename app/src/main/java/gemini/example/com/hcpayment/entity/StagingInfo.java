package gemini.example.com.hcpayment.entity;

import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/5/28.
 */

public class StagingInfo extends BaseResponse {


    /**
     * data : {"id":101,"no":"HC001-1597863255","createTime":"","updateTime":"2018-06-26 10:24:34","productId":32,"productName":"致青春","productPrice":2288,"customId":24,"customName":"蒋春美","houseId":71,"houseAddress":"杭州市西湖区大华西溪风情2-3-601","houseArea":225,"houseDecorationArea":225,"frontMoneyId":"","frontMoney":0,"discount":15678,"actualMoney":499122,"specialMoney":1333,"manager":"傅志红","stage":3,"shopId":1,"shopName":"杭州直营店","shopNo":"HCZJ345","status":0,"mobile":"","contractStages":"","specialMoneyListVOS":"","stages":[199648.8,199648.8,99824.4]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 101
         * no : HC001-1597863255
         * createTime :
         * updateTime : 2018-06-26 10:24:34
         * productId : 32
         * productName : 致青春
         * productPrice : 2288
         * customId : 24
         * customName : 蒋春美
         * houseId : 71
         * houseAddress : 杭州市西湖区大华西溪风情2-3-601
         * houseArea : 225.0
         * houseDecorationArea : 225.0
         * frontMoneyId :
         * frontMoney : 0
         * discount : 15678.0
         * actualMoney : 499122.0
         * specialMoney : 1333.0
         * manager : 傅志红
         * stage : 3
         * shopId : 1
         * shopName : 杭州直营店
         * shopNo : HCZJ345
         * status : 0
         * mobile :
         * contractStages :
         * specialMoneyListVOS :
         * stages : [199648.8,199648.8,99824.4]
         */

        private int id;
        private String no;
        private String createTime;
        private String updateTime;
        private int productId;
        private String productName;
        private int productPrice;
        private int customId;
        private String customName;
        private int houseId;
        private String houseAddress;
        private double houseArea;
        private double houseDecorationArea;
        private String frontMoneyId;
        private int frontMoney;
        private double discount;
        private double actualMoney;
        private double specialMoney;
        private String manager;
        private int stage;
        private int shopId;
        private String shopName;
        private String shopNo;
        private int status;
        private String mobile;
        private String contractStages;
        private String specialMoneyListVOS;
        private List<Double> stages;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNo() {
            return no;
        }

        public void setNo(String no) {
            this.no = no;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public int getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(int productPrice) {
            this.productPrice = productPrice;
        }

        public int getCustomId() {
            return customId;
        }

        public void setCustomId(int customId) {
            this.customId = customId;
        }

        public String getCustomName() {
            return customName;
        }

        public void setCustomName(String customName) {
            this.customName = customName;
        }

        public int getHouseId() {
            return houseId;
        }

        public void setHouseId(int houseId) {
            this.houseId = houseId;
        }

        public String getHouseAddress() {
            return houseAddress;
        }

        public void setHouseAddress(String houseAddress) {
            this.houseAddress = houseAddress;
        }

        public double getHouseArea() {
            return houseArea;
        }

        public void setHouseArea(double houseArea) {
            this.houseArea = houseArea;
        }

        public double getHouseDecorationArea() {
            return houseDecorationArea;
        }

        public void setHouseDecorationArea(double houseDecorationArea) {
            this.houseDecorationArea = houseDecorationArea;
        }

        public String getFrontMoneyId() {
            return frontMoneyId;
        }

        public void setFrontMoneyId(String frontMoneyId) {
            this.frontMoneyId = frontMoneyId;
        }

        public int getFrontMoney() {
            return frontMoney;
        }

        public void setFrontMoney(int frontMoney) {
            this.frontMoney = frontMoney;
        }

        public double getDiscount() {
            return discount;
        }

        public void setDiscount(double discount) {
            this.discount = discount;
        }

        public double getActualMoney() {
            return actualMoney;
        }

        public void setActualMoney(double actualMoney) {
            this.actualMoney = actualMoney;
        }

        public double getSpecialMoney() {
            return specialMoney;
        }

        public void setSpecialMoney(double specialMoney) {
            this.specialMoney = specialMoney;
        }

        public String getManager() {
            return manager;
        }

        public void setManager(String manager) {
            this.manager = manager;
        }

        public int getStage() {
            return stage;
        }

        public void setStage(int stage) {
            this.stage = stage;
        }

        public int getShopId() {
            return shopId;
        }

        public void setShopId(int shopId) {
            this.shopId = shopId;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getShopNo() {
            return shopNo;
        }

        public void setShopNo(String shopNo) {
            this.shopNo = shopNo;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getContractStages() {
            return contractStages;
        }

        public void setContractStages(String contractStages) {
            this.contractStages = contractStages;
        }

        public String getSpecialMoneyListVOS() {
            return specialMoneyListVOS;
        }

        public void setSpecialMoneyListVOS(String specialMoneyListVOS) {
            this.specialMoneyListVOS = specialMoneyListVOS;
        }

        public List<Double> getStages() {
            return stages;
        }

        public void setStages(List<Double> stages) {
            this.stages = stages;
        }
    }
}
