package gemini.example.com.hcpayment.presenter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.ContractListBean;
import gemini.example.com.hcpayment.model.OrderManagerModel;
import gemini.example.com.hcpayment.model.StandardFeeModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.StandardFeeListView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/6/12.
 */

public class StandardFeeListPresenter extends BasePresenter<StandardFeeListView> implements BaseAdapter.onItemClickListener {

    //与订单管理用一个接口，复用一个Model
    StandardFeeModel standardFeeModel;

    public StandardFeeListPresenter() {
        standardFeeModel = new StandardFeeModel();
    }

    public RecyclerView.OnScrollListener getOnScrollListener() {

        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d("LoadMoreRecyclerView", "run in onScrollStateChanged");
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.d("LoadMoreRecyclerView", "SCROLL_STATE_IDLE");
                    RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                    int lastVisiblePosition;
                    lastVisiblePosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                    Log.d("LoadMoreRecyclerView", "ChildCount: " + layoutManager.getChildCount() + " lastvisiblePosition: "
                            + lastVisiblePosition + " ItemCount: " + layoutManager.getItemCount());
                    if (layoutManager.getChildCount() > 0             //当当前显示的item数量>0
                            && lastVisiblePosition >= layoutManager.getItemCount() - 1           //当当前屏幕最后一个加载项位置>=所有item的数量
                            && layoutManager.getItemCount() > layoutManager.getChildCount()) { // 当当前总Item数大于可见Item数
                        Log.d("LoadMoreRecyclerView", "run onLoadMore");
                        getView().onLoadMore();
                    }else{
                        getView().onNoLoadMore();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        };
        return onScrollListener;
    }
    public void queryStandardFeeList(int stage, String mobile, int status,int page) {
        getView().showProgressDialog(true);
        standardFeeModel.queryOrder(getView().getToken(), stage, mobile, status,page, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                ContractListBean contractListBean = (ContractListBean) o;
                getView().onCallbackQueryResult(contractListBean);

            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();

            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }

    @Override
    public void onItemClick(int position, View v) {
        getView().onCallItemClick(getView().getStandardFeeAdapter().getData().get(position));

    }
}
