package gemini.example.com.hcpayment.contract;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.base.BaseView;

/**
 * Created by Gemini on 2018/5/25.
 */

public interface CreateOrderContract {
    interface  CreateOrderView extends BaseView{


    }
    abstract class CreateOrderPresenter extends BasePresenter<CreateOrderView>{

    }
}
