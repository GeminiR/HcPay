package gemini.example.com.hcpayment.model;

import android.content.Context;

import java.util.Map;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/5/28.
 */

public class CreateOrderTwoModel extends BaseModel {
    public void queryProduct(String token,ObserverResponseListener observerResponseListener) {
        subscribe( Api.getApiSevice().getProductInfo(token), observerResponseListener);
    }
    public void queryStagingInfo(String token,String stageInfoJson, ObserverResponseListener observerResponseListener){
        subscribe( Api.getApiSevice().getStagingInfo(token,stageInfoJson), observerResponseListener);
    }
    public void queryDepositInfo(String token,String mobile,int customId,int page,ObserverResponseListener observerResponseListener){
        subscribe(Api.getApiSevice().getDepositInfo(token,mobile,2,customId,page),observerResponseListener);
    }
}
