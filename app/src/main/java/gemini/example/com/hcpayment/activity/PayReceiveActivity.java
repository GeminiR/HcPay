package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.base.BaseView;

/**
 * Created by Gemini on 2018/5/31.
 * 收付款管理
 */

public class PayReceiveActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;

    @Override
    public BasePresenter createPresenter() {
        return null;
    }

    @Override
    public BaseView createView() {
        return null;
    }

    @Override
    public void init() {
        titleText.setText("收支查询");
        changeStatusBarTextColor(true);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_incomemanager;
    }

    @OnClick({R.id.customMadeRelative, R.id.productStandardRelative})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.customMadeRelative:
                skipActivity(CustomMadeListActivity.class, null);
                break;
            case R.id.productStandardRelative:
                skipActivity(StandardFeeListActivity.class, null);
                break;
            default:
                break;
        }
    }
}
