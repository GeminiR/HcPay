package gemini.example.com.hcpayment.utils;

import com.google.gson.JsonParseException;

import org.json.JSONException;

import java.net.ConnectException;

import javax.net.ssl.SSLHandshakeException;

import retrofit2.HttpException;

/**
 * Created by Gemini on 2018/3/28.
 */

public class ExceptionHandle {
    private static final int UNAUTHORIZED = 401;
    private static final int FORBIDDEN = 403;
    private static final int NOT_FOUND = 404;
    private static final int REQUEST_TIMEOUT = 408;
    private static final int INTERNAL_SERVER_ERROR = 500;
    private static final int BAD_GATEWAY = 502;
    private static final int SERVICE_UNAVAILABLE = 503;
    private static final int GATEWAY_TIMEOUT = 504;

    public static ResponseException handleException(Throwable e) {
        ResponseException re;
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            re = new ResponseException(e, ERROR.HTTP_ERROR);
            switch (httpException.code()) {
                case UNAUTHORIZED:
                case FORBIDDEN:
                case NOT_FOUND:
                case REQUEST_TIMEOUT:
                case INTERNAL_SERVER_ERROR:
                case BAD_GATEWAY:
                case SERVICE_UNAVAILABLE:
                case GATEWAY_TIMEOUT:
                default:
                    re.msg = "网络错误";
                    break;

            }
            return re;
        } else if (e instanceof ServerException) {
            ServerException exception = (ServerException) e;
            re = new ResponseException(exception, exception.code);
            return re;
        }else if(e instanceof JsonParseException ||e instanceof JSONException ||e instanceof android.net.ParseException){
            re=new ResponseException(e,ERROR.PARSE_ERROR);
            re.msg="解析错误";
            return re;
        }else if(e instanceof ConnectException){
            re=new ResponseException(e,ERROR.NETWORD_ERROR);
            re.msg="连接失败";
            return re;
        }else if(e instanceof SSLHandshakeException){
            re=new ResponseException(e,ERROR.SSL_ERROR);
            re.msg="证书验证失败";
            return re;
        }else{
            re=new ResponseException(e,ERROR.UNKNOWN);
            re.msg="未知错误";
            return re;
        }


    }

    /**
     * 约定异常
     */
    public class ERROR {
        /**
         * 未知错误
         */
        public static final int UNKNOWN = 1000;
        /**
         * 解析错误
         */
        public static final int PARSE_ERROR = 1001;
        /**
         * 网络错误
         */
        public static final int NETWORD_ERROR = 1002;
        /**
         * 协议出错
         */
        public static final int HTTP_ERROR = 1003;

        /**
         * 证书出错
         */
        public static final int SSL_ERROR = 1005;
    }

    public static class ResponseException extends Exception {
        public String msg;
        public int code;

        public ResponseException(Throwable cause, int code) {
            super(cause);
            this.code = code;
        }
    }

    class ServerException extends RuntimeException {
        public int code;
        public String message;
    }
}
