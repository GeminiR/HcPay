package gemini.example.com.hcpayment.presenter;

import android.util.Log;

import java.util.HashMap;
import java.util.List;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.QueryContractBean;
import gemini.example.com.hcpayment.entity.SubmitStageInfo;
import gemini.example.com.hcpayment.model.ContractDetailModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.ContractDetailView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/6/4.
 */

public class ContractDetailPresenter extends BasePresenter<ContractDetailView> {
    ContractDetailModel contractDetailModel;
    String TAG = "ContractDetailPresenter";
    HashMap<Integer,Integer> statusMap;
    HashMap<Integer,Double> priceMap;
    int defaultNoPayId;
    int defaultStage=-1;//判别是否是三期阶段或者是个性定制费用
    int tempSpecialMoney=0;

    public ContractDetailPresenter() {
        statusMap = new HashMap();
        priceMap = new HashMap();
        contractDetailModel = new ContractDetailModel();
    }

    public int getDefaultNoPayId() {
        return defaultNoPayId;
    }

    public void queryContractStageInfo(int contractId) {
        getView().showProgressDialog(true);
        contractDetailModel.queryContractStageInfo(getView().getToken(),contractId, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                QueryContractBean queryContractBean = (QueryContractBean) o;
                if (queryContractBean != null) {
                    List<QueryContractBean.DataBean.StageBean> stageBeanList = queryContractBean.getData().getStage();
                    List<QueryContractBean.DataBean.SpecialMoneyBean> specialMoneyBeanList = queryContractBean.getData().getSpecialMoney();
                    for (int i = 0; i < stageBeanList.size(); i++) {
                        QueryContractBean.DataBean.StageBean stageBean = stageBeanList.get(i);
                        statusMap.put(stageBean.getStage(), stageBean.getPayStatus());
                        priceMap.put(stageBean.getStage(), stageBean.getMoney());
                    }
                    for (int i = 0; i < stageBeanList.size(); i++) {
                        if (stageBeanList.get(i).getPayStatus() == 0) {
                            defaultNoPayId = stageBeanList.get(i).getId();
                            defaultStage=stageBeanList.get(i).getStage();
                            break;
                        }
                    }
                    getView().onCallbackStatusAndMoney(statusMap, priceMap);
                    if (specialMoneyBeanList != null) {
                        int specialMoney = 0;
                        //0未付，1已付，默认为1，如果有一个为0那么视为未付
                        int specialStatus = 1;
                        for (int i = 0; i < specialMoneyBeanList.size(); i++) {
                            specialMoney += (specialMoneyBeanList.get(i).getMoney());
                            if (specialMoneyBeanList.get(i).getPayStatus() == 0) {
                                specialStatus = 0;
                            }
                        }
                        tempSpecialMoney=specialMoney;
                        getView().onCallbackSpecailMoney(specialMoney, specialStatus);
                    }
                }

            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }


    public void submitCheck(int id) {
        getView().showProgressDialog(true);
        contractDetailModel.submitStageCheck(getView().getToken(),id, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                if(defaultStage>0){
                    statusMap.put(defaultStage,1);
                    getView().onCallbackStatusAndMoney(statusMap,priceMap);

                }else{
                    getView().onCallbackSpecailMoney(tempSpecialMoney,1);
                }
                getView().onCallbackSubmitSuccess();
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();

            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });

    }
}
