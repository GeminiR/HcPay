package gemini.example.com.hcpayment.entity;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/5/31.
 */

public class KeepDepositBean extends BaseResponse {

    /**
     * data : {"id":7,"contractId":"","customId":17,"money":123456.789,"status":1,"mark":"北京5环装修定金","orderNo":"FM20180518164442245PPY","serialNo":"","payTime":"","payStatus":0,"updateTime":"2018-05-18 08:44:42","createTime":"2018-05-18 08:44:42","operator":"刘双箫"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "KeepDepositBean{" +
                "data=" + data +
                '}';
    }

    public static class DataBean {
        /**
         * id : 7
         * contractId :
         * customId : 17
         * money : 123456.789
         * status : 1
         * mark : 北京5环装修定金
         * orderNo : FM20180518164442245PPY
         * serialNo :
         * payTime :
         * payStatus : 0
         * updateTime : 2018-05-18 08:44:42
         * createTime : 2018-05-18 08:44:42
         * operator : 刘双箫
         */

        private int id;
        private String contractId;
        private int customId;
        private double money;
        private int status;
        private String mark;
        private String orderNo;
        private String serialNo;
        private String payTime;
        private int payStatus;
        private String updateTime;
        private String createTime;
        private String operator;

        @Override
        public String toString() {
            return "DataBean{" +
                    "id=" + id +
                    ", contractId='" + contractId + '\'' +
                    ", customId=" + customId +
                    ", money=" + money +
                    ", status=" + status +
                    ", mark='" + mark + '\'' +
                    ", orderNo='" + orderNo + '\'' +
                    ", serialNo='" + serialNo + '\'' +
                    ", payTime='" + payTime + '\'' +
                    ", payStatus=" + payStatus +
                    ", updateTime='" + updateTime + '\'' +
                    ", createTime='" + createTime + '\'' +
                    ", operator='" + operator + '\'' +
                    '}';
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getContractId() {
            return contractId;
        }

        public void setContractId(String contractId) {
            this.contractId = contractId;
        }

        public int getCustomId() {
            return customId;
        }

        public void setCustomId(int customId) {
            this.customId = customId;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMark() {
            return mark;
        }

        public void setMark(String mark) {
            this.mark = mark;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(String serialNo) {
            this.serialNo = serialNo;
        }

        public String getPayTime() {
            return payTime;
        }

        public void setPayTime(String payTime) {
            this.payTime = payTime;
        }

        public int getPayStatus() {
            return payStatus;
        }

        public void setPayStatus(int payStatus) {
            this.payStatus = payStatus;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getOperator() {
            return operator;
        }

        public void setOperator(String operator) {
            this.operator = operator;
        }
    }
}
