package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.contract.CreateOrderContract;
import gemini.example.com.hcpayment.entity.CustomBean;
import gemini.example.com.hcpayment.presenter.CreateOrderPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.CreateOrderTwoView;

/**
 * Created by Gemini on 2018/5/25.
 */

public class CreateOrderActivity extends BaseActivity<CreateOrderContract.CreateOrderView, CreateOrderContract.CreateOrderPresenter> implements CreateOrderContract.CreateOrderView {
    @BindView(R.id.importInfo)
    TextView importInfo;
    @BindView(R.id.nextStep)
    Button nextStep;
    private static final int REQUEST_CODE = 1;
    private static final int RESULT_CODE = 3;
    private int customId = 0;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;

    @Override
    public CreateOrderContract.CreateOrderPresenter createPresenter() {
        return new CreateOrderPresenter();
    }

    @Override
    public CreateOrderContract.CreateOrderView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("创建订单");
        changeStatusBarTextColor(true);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_createorder_one;
    }

    @OnClick({R.id.importInfo, R.id.nextStep})
    public void onViewOnClick(View view) {
        switch (view.getId()) {
            case R.id.importInfo:
                Intent intent = new Intent(CreateOrderActivity.this, ImportGuestNoActivity.class);
                intent.putExtra(SystemArgs.CUSTOMID, customId);
                intent.putExtra(SystemArgs.BUILDINGAREA, buildingArea.getText() + "");
                intent.putExtra(SystemArgs.DECORATIONAREA, buildingArea.getText() + "");
                intent.putExtra(SystemArgs.CUSTOMMANAGER, guestManager.getText() + "");
                startActivityForResult(intent, REQUEST_CODE);
                break;
            case R.id.nextStep:
                String guestNameText = guestName.getText().toString();
                if (!TextUtils.isEmpty(guestNameText)) {
                    Intent createIntent = new Intent(CreateOrderActivity.this, CreateOrderTwoActivity.class);
                    createIntent.putExtra(SystemArgs.CUSTOMID, customId);
                    createIntent.putExtra(SystemArgs.BUILDINGAREA, buildingArea.getText() + "");
                    createIntent.putExtra(SystemArgs.DECORATIONAREA, decorateArea.getText() + "");
                    createIntent.putExtra(SystemArgs.CUSTOMMANAGER, guestManager.getText() + "");
                    createIntent.putExtra(SystemArgs.DECORATIONADD,decorateaddress.getText()+"");
                    startActivity(createIntent);
                } else {
                    ToastUtil.showShortToast("请导入客户信息");
                }
                break;
            default:
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_CODE) {
                if (data != null) {
                    Bundle bundle = data.getBundleExtra(SystemArgs.CUSTOMBUNDLE);
                    CustomBean.DataBean.ListBean listBean = (CustomBean.DataBean.ListBean) bundle.getSerializable(SystemArgs.CUSTOMINFO);
                    if (listBean != null) {
                        guestName.setText(listBean.getName());
                        guestPhone.setText(listBean.getMobile());
                        decorateaddress.setText(listBean.getAddress());
                        buildingArea.setText(listBean.getArea());
                        decorateArea.setText(listBean.getDecorationArea());
                        guestManager.setText(listBean.getManager());
                        customId = listBean.getId();
                    }
                }
            }
        }

    }

    @BindView(R.id.guestName)
    TextView guestName;
    @BindView(R.id.guestPhone)
    TextView guestPhone;
    @BindView(R.id.guestContact)
    TextView guestContact;
    @BindView(R.id.decorateAddress)
    TextView decorateaddress;
    @BindView(R.id.buildArea)
    TextView buildingArea;
    @BindView(R.id.decorateArea)
    TextView decorateArea;
    @BindView(R.id.guestManager)
    TextView guestManager;

}
