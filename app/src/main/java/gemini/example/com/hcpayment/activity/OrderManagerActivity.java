package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.adapter.QueryContractListAdapter;
import gemini.example.com.hcpayment.adapter.QueryCustommadeListAdapter;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.ContractListBean;
import gemini.example.com.hcpayment.entity.QueryContractStatusInfo;
import gemini.example.com.hcpayment.presenter.OrderManagerPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.OrderManagerView;

/**
 * Created by Gemini on 2018/6/1.
 */

public class OrderManagerActivity extends BaseActivity<OrderManagerView, OrderManagerPresenter> implements OrderManagerView,
        SwipeRefreshLayout.OnRefreshListener, TextView.OnEditorActionListener {
    private String TAG = "OrderManager";
    @BindView(R.id.orderManagerRecycler)
    RecyclerView orderManagerRecycler;
    QueryContractListAdapter queryContractListAdapter;
    private int status = 1;
    @BindView(R.id.noReceiveButton)
    TextView noReceive;
    @BindView(R.id.doneButton)
    TextView doneReceive;
    @BindView(R.id.haveReceivedButton)
    TextView haveReceive;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.orderManagerSwipe)
    SwipeRefreshLayout orderManagerSwipe;
    @BindView(R.id.searchGuestInfoEdit)
    EditText searchGuestInfoEdit;
    @BindView(R.id.depositNoDataLayout)
    RelativeLayout noDataLayout;
    private int currentPage = 1;

    @Override
    public OrderManagerPresenter createPresenter() {
        return new OrderManagerPresenter(this);
    }

    @Override
    public OrderManagerView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("订单管理");
        orderManagerSwipe.setOnRefreshListener(this);
        orderManagerSwipe.setColorSchemeResources(R.color.moregreen);
        orderManagerRecycler.setLayoutManager(new LinearLayoutManager(this));
        orderManagerRecycler.setOnScrollListener(getPresenter().getOnScrollListener());
        orderManagerRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        noReceive.performClick();
        searchGuestInfoEdit.setOnEditorActionListener(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_ordermanager;
    }


    @Override
    public void onCallbackContractList(QueryContractStatusInfo queryContractStatusInfo) {
        if (queryContractListAdapter == null) {
            queryContractListAdapter = new QueryContractListAdapter(queryContractStatusInfo.getData().getList(), R.layout.item_guestbookinfo);
            queryContractListAdapter.setItemClickListener(getPresenter());
            orderManagerRecycler.setAdapter(queryContractListAdapter);
        } else {
            if (currentPage > 1) {
                queryContractListAdapter.addData(queryContractStatusInfo.getData().getList());
            } else {
                queryContractListAdapter.setData(queryContractStatusInfo.getData().getList());
            }

        }
        queryContractListAdapter.setFootStatus(BaseAdapter.LOADOVER);
        queryContractListAdapter.notifyDataSetChanged();
        if(queryContractListAdapter.getData().size()>0){
            noDataLayout.setVisibility(View.GONE);
        }else{
            noDataLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        currentPage = 1;
        requestData(status);
        return true;
    }

    @Override
    public void onItemClick(QueryContractStatusInfo.DataBean.ListBean listBean) {
        Intent intent = new Intent(this, ContractDetailActivity.class);
        intent.putExtra(SystemArgs.CONTRACTINFO, listBean);
        startActivity(intent);
    }

    @Override
    public void requestData(int status) {
        getPresenter().queryContractList(1, searchGuestInfoEdit.getText().toString(), status, currentPage);
    }

    @Override
    public void onLoadMore() {
        if (queryContractListAdapter != null) {
            currentPage++;
            queryContractListAdapter.setFootStatus(BaseAdapter.ISLOADING);
            queryContractListAdapter.notifyDataSetChanged();
            requestData(status);
        }
    }

    @Override
    public void onNoLoadMore() {
        if (queryContractListAdapter != null) {
            queryContractListAdapter.setFootStatus(BaseAdapter.LOADOVER);
            queryContractListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public QueryContractListAdapter getAdapter() {
        return queryContractListAdapter;
    }


    @OnClick({R.id.noReceiveButton, R.id.doneButton, R.id.haveReceivedButton})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.noReceiveButton:
                status = 1;
                noReceive.setBackgroundResource(R.drawable.depositmanager_left_orange);
                noReceive.setTextColor(getResources().getColor(R.color.white));
                doneReceive.setBackgroundResource(R.drawable.depositmanager_right_gray);
                doneReceive.setTextColor(getResources().getColor(R.color.black));
                haveReceive.setBackgroundResource(R.drawable.depositmanager_middle_gray);
                haveReceive.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.haveReceivedButton:
                status = 2;
                haveReceive.setBackgroundResource(R.drawable.depositmanager_middle_orange);
                haveReceive.setTextColor(getResources().getColor(R.color.white));
                doneReceive.setBackgroundResource(R.drawable.depositmanager_right_gray);
                doneReceive.setTextColor(getResources().getColor(R.color.black));
                noReceive.setBackgroundResource(R.drawable.depositmanager_left_gray);
                noReceive.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.doneButton:
                status = 3;
                doneReceive.setBackgroundResource(R.drawable.depositmanager_right_orange);
                doneReceive.setTextColor(getResources().getColor(R.color.white));
                noReceive.setBackgroundResource(R.drawable.depositmanager_left_gray);
                noReceive.setTextColor(getResources().getColor(R.color.black));
                haveReceive.setBackgroundResource(R.drawable.depositmanager_middle_gray);
                haveReceive.setTextColor(getResources().getColor(R.color.black));
                break;

            default:
                break;
        }
        currentPage = 1;
        requestData(status);
    }

    @Override
    public void onRefresh() {
        orderManagerSwipe.setRefreshing(false);
        currentPage = 1;
        requestData(status);
    }


}
