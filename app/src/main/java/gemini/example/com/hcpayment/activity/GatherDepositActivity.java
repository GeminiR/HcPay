package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.CustomBean;
import gemini.example.com.hcpayment.presenter.GatherDepositPresenter;
import gemini.example.com.hcpayment.utils.MoneyTextWatcher;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.GatherDepositView;

/**
 * Created by Gemini on 2018/5/30.
 */

public class GatherDepositActivity extends BaseActivity<GatherDepositView,GatherDepositPresenter> implements GatherDepositView {
    @BindView(R.id.customName)
    TextView customName;
    @BindView(R.id.decorationAddress)
    TextView customAddress;
    @BindView(R.id.loginPhoneNum)
    TextView customMobile;
    @BindView(R.id.inputMondyEdit)
    EditText inputMoney;
    CustomBean.DataBean.ListBean listBean=null;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    private int customHouseId=0;
    @Override
    public GatherDepositPresenter createPresenter() {
        return new GatherDepositPresenter();
    }

    @Override
    public GatherDepositView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("收取订金");
        changeStatusBarTextColor(true);
        Intent intent=getIntent();
        if(intent!=null){
            Bundle bundle=intent.getBundleExtra(SystemArgs.CUSTOMBUNDLE);
            listBean= (CustomBean.DataBean.ListBean) bundle.getSerializable(SystemArgs.CUSTOMINFO);
            customHouseId=listBean.getCustomHouseId();
            customName.setText(listBean.getName());
            customAddress.setText(listBean.getAddress());
            customMobile.setText(listBean.getMobile());
        }
        inputMoney.addTextChangedListener(new MoneyTextWatcher(inputMoney));
        inputMoney.setOnEditorActionListener((TextView.OnEditorActionListener) getPresenter());

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_getdeposit_inputmoney;
    }

    @Override
    public void sureInput() {
        String money=inputMoney.getText().toString();
        if(!TextUtils.isEmpty(money)){
            Intent intent=new Intent(this,PayDepositActivity.class);
            Bundle bundle=new Bundle();
            if(listBean!=null){
                bundle.putSerializable(SystemArgs.CUSTOMINFO,listBean);
            }
            bundle.putSerializable(SystemArgs.CUSTOMHOUSEID,customHouseId);
            bundle.putString(SystemArgs.INPUTDEPOSIT, money);
            intent.putExtra(SystemArgs.CUSTOMBUNDLE,bundle);
            startActivity(intent);
        }else{
            ToastUtil.showShortToast("请输入金额");
        }

    }
}
