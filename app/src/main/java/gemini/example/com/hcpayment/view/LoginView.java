package gemini.example.com.hcpayment.view;

import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.LoginBean;

/**
 * Created by Gemini on 2018/6/5.
 */

public interface LoginView extends BaseView {
    void onCallbackLoginResult(LoginBean loginBean);

}
