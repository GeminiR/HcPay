package gemini.example.com.hcpayment.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.ReceiveInfoAdapter;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.ReceiveInfoBean;
import gemini.example.com.hcpayment.presenter.ReceiveInfoPresenter;
import gemini.example.com.hcpayment.view.ReceiveInfoView;

/**
 * Created by Gemini on 2018/6/16.
 */

public class ReceiveInfoActivity extends BaseActivity<ReceiveInfoView, ReceiveInfoPresenter> implements
        ReceiveInfoView, SwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    ReceiveInfoAdapter receiveInfoAdapter;
    private int status = 1;

    private View footView;
    private int currentPage = 1;

    @Override
    public ReceiveInfoPresenter createPresenter() {
        return new ReceiveInfoPresenter();
    }

    @Override
    public ReceiveInfoView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("收支查询");
        shouldIncome.performClick();
        receiveInfoListView.setGroupIndicator(null);
        receiveInfoListView.setOnScrollListener(this);
        receiveInfoSwipe.setOnRefreshListener(this);
        footView = LayoutInflater.from(this).inflate(R.layout.item_footview, null);

    }

    @Override
    public void onRefresh() {
        receiveInfoSwipe.setRefreshing(false);
        currentPage = 1;
        getPresenter().QueryReceiveInfo(status, "", currentPage);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_receiveinfo;
    }

    @Override
    public void onCallbackReciveInfo(ReceiveInfoBean receiveInfoBean) {
        if (receiveInfoAdapter == null) {
            receiveInfoAdapter = new ReceiveInfoAdapter(this);
            receiveInfoAdapter.setDataList(receiveInfoBean.getData().getList());
            receiveInfoAdapter.setStatus(status);
            footView.setVisibility(View.GONE);
            receiveInfoListView.addFooterView(footView);
            receiveInfoListView.setAdapter(receiveInfoAdapter);
            receiveInfoListView.setOnChildClickListener(getPresenter());
        } else {
            if (currentPage > 1) {
                receiveInfoAdapter.addData(receiveInfoBean.getData().getList());
                onLoadComplete();
            } else {
                receiveInfoAdapter.setDataList(receiveInfoBean.getData().getList());
            }

        }
        receiveInfoAdapter.notifyDataSetChanged();
        int groupCount = receiveInfoAdapter.getGroupCount();
        for (int i = 0; i < groupCount; i++) {
            receiveInfoListView.expandGroup(i);
        }
    }

    @OnClick({R.id.actualIncome, R.id.shoundPayButton})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.shoundPayButton:
                status = 1;
                shouldIncome.setTextColor(getResources().getColor(R.color.orange));
                actualIncome.setTextColor(getResources().getColor(R.color.mainContentTextColor));
                break;
            case R.id.actualIncome:
                status = 3;
                actualIncome.setTextColor(getResources().getColor(R.color.orange));
                shouldIncome.setTextColor(getResources().getColor(R.color.mainContentTextColor));
                break;
            default:
                break;
        }
        currentPage = 1;
        getPresenter().QueryReceiveInfo(status, "", currentPage);
    }

    @BindView(R.id.expandListView)
    ExpandableListView receiveInfoListView;
    @BindView(R.id.receiveSwipe)
    SwipeRefreshLayout receiveInfoSwipe;
    @BindView(R.id.shoundPayButton)
    TextView shouldIncome;
    @BindView(R.id.actualIncome)
    TextView actualIncome;
    public int last_index;
    public int total_index;
    public boolean isLoading = false;

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (last_index == total_index && (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE)) {
            //表示此时需要显示刷新视图界面进行新数据的加载(要等滑动停止)
            if (!isLoading) {
                //不处于加载状态的话对其进行加载
                isLoading = true;
                //设置刷新界面可见

                footView.setVisibility(View.VISIBLE);
                onLoad();
            }
        }

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        last_index = firstVisibleItem + visibleItemCount;
        total_index = totalItemCount;

    }

    private void onLoad() {
        currentPage++;
        getPresenter().QueryReceiveInfo(status, "", currentPage);
    }

    private void onLoadComplete() {
        isLoading = false;
        footView.setVisibility(View.GONE);
    }
}
