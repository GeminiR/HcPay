package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;


import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.ContractListBean;
import gemini.example.com.hcpayment.entity.QueryContractBean;
import gemini.example.com.hcpayment.entity.QueryContractStatusInfo;
import gemini.example.com.hcpayment.model.OrderManagerModel;
import gemini.example.com.hcpayment.model.QueryContractInfoModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.CalculateRecyclerViewHeight;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.OrderManagerView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/6/1.
 */

public class OrderManagerPresenter extends BasePresenter<OrderManagerView> implements BaseAdapter.onItemClickListener {
    Context context;
    QueryContractInfoModel queryContractInfoModel;
    QueryContractStatusInfo queryContractStatusInfo;
    String TAG = "OrderManagerPresenter";

    public OrderManagerPresenter(Context context) {
        this.context = context;
        queryContractInfoModel = new QueryContractInfoModel();
    }

    public RecyclerView.OnScrollListener getOnScrollListener() {

        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d("LoadMoreRecyclerView", "run in onScrollStateChanged");
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.d("LoadMoreRecyclerView", "SCROLL_STATE_IDLE");
                    RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                    int lastVisiblePosition;
                    lastVisiblePosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                    Log.d("LoadMoreRecyclerView", "ChildCount: " + layoutManager.getChildCount() + " lastvisiblePosition: "
                            + lastVisiblePosition + " ItemCount: " + layoutManager.getItemCount());
                    if (layoutManager.getChildCount() > 0             //当当前显示的item数量>0
                            && lastVisiblePosition >= layoutManager.getItemCount() - 1           //当当前屏幕最后一个加载项位置>=所有item的数量
                            && layoutManager.getItemCount() > layoutManager.getChildCount()) { // 当当前总Item数大于可见Item数
                        Log.d("LoadMoreRecyclerView", "run onLoadMore");
                        getView().onLoadMore();
                    }else{
                        getView().onNoLoadMore();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        };
        return onScrollListener;
    }

    public void queryContractList(int stage, String mobile, int status, int page) {
        getView().showProgressDialog(true);
        queryContractInfoModel.quertContractInfo(getView().getToken(), status, page, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                queryContractStatusInfo = (QueryContractStatusInfo) o;
                getView().onCallbackContractList(queryContractStatusInfo);
            }


            @Override
            public void onError(ExceptionHandle.ResponseException e) {

            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }

    @Override
    public void onItemClick(int position, View v) {
        getView().onItemClick(getView().getAdapter().getData().get(position));
    }

}
