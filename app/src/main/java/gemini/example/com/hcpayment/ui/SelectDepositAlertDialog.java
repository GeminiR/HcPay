package gemini.example.com.hcpayment.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.adapter.QueryDepositDialogAdapter;
import gemini.example.com.hcpayment.entity.DepositBean;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.utils.WheelView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/5/29.
 */

public class SelectDepositAlertDialog extends AlertDialog {
    Context context;
    BaseAdapter.onItemClickListener onItemClickListener;
    @BindView(R.id.depositInfoRecycler)
    RecyclerView queryDepositRecycler;
    @BindView(R.id.queryDepositSwipe)
    SwipeRefreshLayout queryDepositSwipe;
    @BindView(R.id.depositNoDataLayout)
    RelativeLayout depositNoDataLayout;
    DepositBean depositBean;
    int currentPage = 1;
    DialogListLoadMoreListener dialogListLoadMoreListener;
    SwipeRefreshLayout.OnRefreshListener onRefreshListener;
    private String TAG = "SelectDepositAlerDialog";
    private QueryDepositDialogAdapter queryDepositDialogAdapter;

    public SwipeRefreshLayout getQueryDepositSwipe() {
        return queryDepositSwipe;
    }

    public QueryDepositDialogAdapter getQueryDepositDialogAdapter() {
        return queryDepositDialogAdapter;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setSwipeRefrshFalse(){
        queryDepositSwipe.setRefreshing(false);
    }
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    protected SelectDepositAlertDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    protected SelectDepositAlertDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public SelectDepositAlertDialog(Context context, DepositBean depositBean
            , BaseAdapter.onItemClickListener onItemClickListener,
                                    DialogListLoadMoreListener dialogListLoadMoreListener,
                                    SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
        super(context);
        this.context = context;
        this.depositBean = depositBean;
        this.onItemClickListener = onItemClickListener;
        this.dialogListLoadMoreListener = dialogListLoadMoreListener;
        this.onRefreshListener = onRefreshListener;
    }


    public void init() {
        View view = LayoutInflater.from(context).inflate(R.layout.alertdialog_querydeposit, null);
        ButterKnife.bind(this, view);

        queryDepositSwipe.setOnRefreshListener(onRefreshListener);
        queryDepositRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        queryDepositRecycler.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL));
        searchData(depositBean);
        Window window = getWindow();
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        layoutParams.width = display.getWidth();
        layoutParams.height = display.getHeight();
        window.setAttributes(layoutParams);
        setView(view);
        show();
    }

    public void searchData(DepositBean depositBean) {
        if (queryDepositDialogAdapter == null) {
            queryDepositDialogAdapter = new QueryDepositDialogAdapter(depositBean.getData().getList(), R.layout.item_guestbookinfo);
            queryDepositDialogAdapter.setItemClickListener(onItemClickListener);
            queryDepositRecycler.setAdapter(queryDepositDialogAdapter);
        } else {
            if (currentPage > 1) {
                queryDepositDialogAdapter.addData(depositBean.getData().getList());
            } else {
                queryDepositDialogAdapter.setData(depositBean.getData().getList());
            }

        }
        queryDepositDialogAdapter.setFootStatus(BaseAdapter.LOADOVER);
        queryDepositDialogAdapter.notifyDataSetChanged();
        queryDepositDialogAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if (queryDepositDialogAdapter.getData().size() == 0) {
                    depositNoDataLayout.setVisibility(View.VISIBLE);
                } else {
                    depositNoDataLayout.setVisibility(View.GONE);
                }
            }
        });
        if (queryDepositDialogAdapter.getData().size() == 0) {
            depositNoDataLayout.setVisibility(View.VISIBLE);
        } else {
            depositNoDataLayout.setVisibility(View.GONE);
        }
    }

    public RecyclerView.OnScrollListener getOnScrollListener() {

        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d("LoadMoreRecyclerView", "run in onScrollStateChanged");
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.d("LoadMoreRecyclerView", "SCROLL_STATE_IDLE");
                    RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                    int lastVisiblePosition;
                    lastVisiblePosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                    Log.d("LoadMoreRecyclerView", "ChildCount: " + layoutManager.getChildCount() + " lastvisiblePosition: "
                            + lastVisiblePosition + " ItemCount: " + layoutManager.getItemCount());
                    if (layoutManager.getChildCount() > 0             //当当前显示的item数量>0
                            && lastVisiblePosition >= layoutManager.getItemCount() - 1           //当当前屏幕最后一个加载项位置>=所有item的数量
                            && layoutManager.getItemCount() > layoutManager.getChildCount()) { // 当当前总Item数大于可见Item数
                        Log.d("LoadMoreRecyclerView", "run onLoadMore");
//                        getView().onLoadMore();
                        if (dialogListLoadMoreListener != null) {
                            dialogListLoadMoreListener.loadMore();
                        }
                    } else {
//                        getView().onNoLoadMore();
                        if (dialogListLoadMoreListener != null) {
                            dialogListLoadMoreListener.noLoadMore();
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        };
        return onScrollListener;
    }

    public interface DialogListLoadMoreListener {
        void loadMore();

        void noLoadMore();
    }
}
