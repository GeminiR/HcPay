package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.adapter.CustomerAdapter;
import gemini.example.com.hcpayment.adapter.QueryCustomAdapter;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.QueryCustomBean;
import gemini.example.com.hcpayment.presenter.QueryCustomPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.QueryCustomView;

/**
 * Created by Gemini on 2018/6/14.
 */

public class QueryCustomActivity extends BaseActivity<QueryCustomView, QueryCustomPresenter> implements QueryCustomView, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    int status = 0;
    QueryCustomAdapter queryCustomAdapter;
    int currentPage = 1;

    @Override
    public QueryCustomPresenter createPresenter() {
        return new QueryCustomPresenter();
    }

    @Override
    public QueryCustomView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("客户查询");
        customRecycler.addOnScrollListener(getPresenter().getOnScrollListener());
        customRecycler.setLayoutManager(new LinearLayoutManager(this));
        customRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        customSwipe.setOnRefreshListener(this);
        getPresenter().queryData(currentPage);
    }


//    @OnClick({R.id.intenttionText, R.id.designText, R.id.constructionText, R.id.doneworkText})
//    public void onViewClick(View view) {
//        switch (view.getId()) {
//            case R.id.intenttionText:
//                status = 0;
//                updateTitle((TextView) view);
//                break;
//            case R.id.designText:
//                status = 1;
//                updateTitle((TextView) view);
//                break;
//            case R.id.constructionText:
//                status = 2;
//                updateTitle((TextView) view);
//                break;
//            case R.id.doneworkText:
//                status = 3;
//                updateTitle((TextView) view);
//                break;
//            default:
//                break;
//        }
//        getPresenter().queryData(status);
//    }

//    public void updateTitle(TextView textView) {
//        intenttionText.setTextColor(getResources().getColor(R.color.customTitleGray));
//        designText.setTextColor(getResources().getColor(R.color.customTitleGray));
//        constructionText.setTextColor(getResources().getColor(R.color.customTitleGray));
//        doneworkText.setTextColor(getResources().getColor(R.color.customTitleGray));
//        textView.setTextColor(getResources().getColor(R.color.moregreen));
//    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_customquery;
    }

    @BindView(R.id.customRecycler)
    RecyclerView customRecycler;
    @BindView(R.id.customSwipe)
    SwipeRefreshLayout customSwipe;
    //    @BindView(R.id.intenttionText)
//    TextView intenttionText;
//    @BindView(R.id.designText)
//    TextView designText;
//    @BindView(R.id.constructionText)
//    TextView constructionText;
//    @BindView(R.id.doneworkText)
//    TextView doneworkText;
    @BindView(R.id.searchGuestEdit)
    EditText searchEdit;
    @BindView(R.id.depositNoDataLayout)
    RelativeLayout noDataLayout;

    @Override
    public void onRefresh() {
        currentPage = 1;
        customSwipe.setRefreshing(false);

        getPresenter().queryData(currentPage);
    }

    @Override
    public String getMobile() {
        return searchEdit.getText().toString();
    }

    @Override
    public void onCallbackCustomInfo(QueryCustomBean queryCustomBean) {
        if (queryCustomAdapter == null) {
            queryCustomAdapter = new QueryCustomAdapter(queryCustomBean.getData().getList(), R.layout.item_guestinfo);
            queryCustomAdapter.setItemClickListener(getPresenter());
            customRecycler.setAdapter(queryCustomAdapter);
        } else {
            if (currentPage > 1) {
                queryCustomAdapter.addData(queryCustomBean.getData().getList());
            } else {
                queryCustomAdapter.setData(queryCustomBean.getData().getList());
            }
        }
        queryCustomAdapter.setFootStatus(BaseAdapter.LOADOVER);
        queryCustomAdapter.notifyDataSetChanged();
        if (queryCustomAdapter.getData().size() > 0) {
            noDataLayout.setVisibility(View.GONE);
        } else {
            noDataLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCallbackItemClick(QueryCustomBean.DataBean.ListBean listBean) {
        Intent intent = new Intent(this, CustomDetailActivity.class);
        intent.putExtra(SystemArgs.CUSTOMINFO, (Serializable) listBean);
        intent.putExtra(SystemArgs.STATUS, status);
        startActivity(intent);
    }

    @Override
    public QueryCustomAdapter getAdapter() {
        return queryCustomAdapter;
    }

    @Override
    public void onLoadMore() {
        if (queryCustomAdapter != null) {
            currentPage++;
            queryCustomAdapter.setFootStatus(BaseAdapter.ISLOADING);
            queryCustomAdapter.notifyDataSetChanged();
            getPresenter().queryData(currentPage);
        }

    }

    @Override
    public void onNoLoadMore() {
        if (queryCustomAdapter != null) {
            queryCustomAdapter.setFootStatus(BaseAdapter.LOADOVER);
            queryCustomAdapter.notifyDataSetChanged();
        }

    }
}
