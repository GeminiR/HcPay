package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.adapter.StandardFeeAdapter;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.ContractListBean;

/**
 * Created by Gemini on 2018/6/12.
 */

public interface StandardFeeListView extends BaseView {
    void onCallbackQueryResult(ContractListBean contractListBean);
    void onCallItemClick(ContractListBean.DataBean.ListBean listBean);
    StandardFeeAdapter getStandardFeeAdapter();
    void onLoadMore();
    void onNoLoadMore();
}
