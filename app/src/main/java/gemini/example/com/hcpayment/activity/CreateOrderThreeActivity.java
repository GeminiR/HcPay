package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.KeepContractInfoBean;
import gemini.example.com.hcpayment.presenter.CreateOrderThreePresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.CreateOrderThreeView;
import gemini.example.com.hcpayment.view.CreateOrderTwoView;

/**
 * Created by Gemini on 2018/5/30.
 */

public class CreateOrderThreeActivity extends BaseActivity<CreateOrderThreeView, CreateOrderThreePresenter> implements CreateOrderThreeView {
    private String TAG = "CreateOrderThree";
    @BindView(R.id.oneStepDeposit)
    TextView oneStepDeposit;
    @BindView(R.id.twoStepDeposit)
    TextView twoStepDeposit;
    @BindView(R.id.threeStepDeposit)
    TextView threeStepDeposit;
    @BindView(R.id.actualMoney)
    TextView actualMoneyText;
    @BindView(R.id.customeMadeMoney)
    TextView customMadeMoneyText;
    @BindView(R.id.sureSubmit)
    Button sureSubmit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    private int contractId = 0;

    @Override
    public CreateOrderThreePresenter createPresenter() {
        return new CreateOrderThreePresenter();
    }

    @Override
    public CreateOrderThreeView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("创建订单");
        changeStatusBarTextColor(true);
        Intent intent = getIntent();
        if (intent != null) {
            contractId = intent.getIntExtra(SystemArgs.CONTRACTID, 0);
            List<Double> stageList = (List<Double>) intent.getSerializableExtra(SystemArgs.STAGELIST);
            double actualMoney = intent.getDoubleExtra(SystemArgs.ACTUALMONEY, 0);
            double customMadeMoney = intent.getDoubleExtra(SystemArgs.CUSTOMMADMONEY, 0);
            actualMoneyText.setText(actualMoney + "");
            customMadeMoneyText.setText(customMadeMoney + "");
            if (stageList != null) {
                switch (stageList.size()) {
                    case 1:
                        oneStepDeposit.setText(stageList.get(0) + "");
                        break;
                    case 2:
                        oneStepDeposit.setText(stageList.get(0) + "");
                        twoStepDeposit.setText(stageList.get(1) + "");
                        break;
                    case 3:
                        oneStepDeposit.setText(stageList.get(0) + "");
                        twoStepDeposit.setText(stageList.get(1) + "");
                        threeStepDeposit.setText(stageList.get(2) + "");
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_createorder_three;
    }

    @OnClick({R.id.sureSubmit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sureSubmit:
                keepOrderInfo(contractId);
                break;
            default:
                break;
        }

    }

    @Override
    public void keepOrderInfo(int contractId) {
        getPresenter().keepOrderInfo(token, contractId);
    }

    @Override
    public void callbackKeepResult(KeepContractInfoBean keepContractInfoBean) {
        int data = keepContractInfoBean.getData().getResult();//1成功，0失败
        if (data == 1) {
            Intent intent = new Intent(this, HcMainPageActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            ToastUtil.showShortToast("保存合同信息失败");
        }
    }

}
