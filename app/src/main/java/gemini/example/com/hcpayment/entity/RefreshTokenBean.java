package gemini.example.com.hcpayment.entity;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/5/31.
 */

public class RefreshTokenBean extends BaseResponse {

    /**
     * data : 4Ludnn79kP9rf7d4TvXFCw==
     */

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RefreshTokenBean{" +
                "data='" + data + '\'' +
                '}';
    }
}
