package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.TransactionInfo;
import gemini.example.com.hcpayment.presenter.PayDepositPresenter;
import gemini.example.com.hcpayment.presenter.PayDepositSuccessPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.PayDepositSuccessView;
import gemini.example.com.hcpayment.view.PayDepositView;

/**
 * Created by Gemini on 2018/6/14.
 */

public class PayDepositSuccessActivity extends BaseActivity<PayDepositSuccessView, PayDepositSuccessPresenter> implements PayDepositSuccessView {


    @Override
    public PayDepositSuccessPresenter createPresenter() {
        return new PayDepositSuccessPresenter();
    }

    @Override
    public PayDepositSuccessView createView() {
        return this;
    }

    @Override
    public void init() {
        tv_title.setText("收款成功");
        cardModeRelative.setVisibility(View.GONE);
        voucherNumRelative.setVisibility(View.GONE);
        batchNumRelative.setVisibility(View.GONE);
        Intent intent = getIntent();
        if (intent != null) {
            TransactionInfo.DataBean dataBean = (TransactionInfo.DataBean) intent.getSerializableExtra(SystemArgs.PAYINFO);
            int type = intent.getIntExtra(SystemArgs.moneyType, 0);
            switch (type) {
                case 1:
                    moneyType.setText("订金");
                    break;
                case 2:
                    moneyType.setText("订制费用");
                    break;
                case 3:
                    moneyType.setText("分期收费");
                    break;
                default:
                    break;
            }
            tenantText.setText(dataBean.getShopName());
            storeText.setText(dataBean.getShopNo());
            shouldPayText.setText(dataBean.getMoney() + "");
            actualPayText.setText(dataBean.getMoney() + "");
            orderNum.setText(dataBean.getOrderNo());
            flowNum.setText(dataBean.getSerialNo());
            dealTime.setText(dataBean.getPayTime());
            operator.setText(dataBean.getOperator());
            guestInfoName.setText(dataBean.getCustomName());
            mobileNum.setText(dataBean.getCustomMobile());
            cardNum.setText(dataBean.getCard());
            dealTime.setText(dataBean.getPayTime());
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_getdepositdetail;
    }

    @OnClick({R.id.backMain})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.backMain:
                Intent intent = new Intent(this, HcMainPageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
    }

    @BindView(R.id.tenantName)
    TextView tenantText;
    @BindView(R.id.storeNum)
    TextView storeText;
    @BindView(R.id.shouldPayMoney)
    TextView shouldPayText;
    @BindView(R.id.factIncome)
    TextView actualPayText;
    @BindView(R.id.moneyMode)
    TextView moneyType;
    @BindView(R.id.orderNum)
    TextView orderNum;
    @BindView(R.id.batchNum)
    TextView batchNum;
    @BindView(R.id.flowNum)
    TextView flowNum;
    @BindView(R.id.voucherNum)
    TextView voucherNum;
    @BindView(R.id.dealTime)
    TextView dealTime;
    @BindView(R.id.operater)
    TextView operator;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.cardNum)
    TextView cardNum;
    @BindView(R.id.cardModeRelative)
    RelativeLayout cardModeRelative;
    @BindView(R.id.batchNumRelative)
    RelativeLayout batchNumRelative;
    @BindView(R.id.voucherNumRelative)
    RelativeLayout voucherNumRelative;
    @BindView(R.id.guestInfoName)
    TextView guestInfoName;
    @BindView(R.id.mobileNum)
    TextView mobileNum;
}
