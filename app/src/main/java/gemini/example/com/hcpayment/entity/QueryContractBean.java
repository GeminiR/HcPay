package gemini.example.com.hcpayment.entity;

import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/5/31.
 */

public class QueryContractBean extends BaseResponse {


    /**
     * data : {"stage":[{"id":22,"contractId":19,"stage":1,"money":57598.6,"orderNo":"CS201806131PWFEOWCWU","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":1,"stagePercent":40,"operator":"","stageTitle":"隐蔽","contractStageDivides":""},{"id":23,"contractId":19,"stage":2,"money":43199.7,"orderNo":"CS201806132PWFEOWCWU","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":2,"stagePercent":30,"operator":"","stageTitle":"主体","contractStageDivides":""},{"id":24,"contractId":19,"stage":3,"money":43199.7,"orderNo":"CS201806133PWFEOWCWU","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":3,"stagePercent":30,"operator":"","stageTitle":"软装","contractStageDivides":""}],"specialMoney":[{"id":1,"contractId":19,"money":50000,"orderNo":"SP20180613174806421PWF","serialNo":"","payTime":"","payStatus":""}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "QueryContractBean{" +
                "data=" + data +
                '}';
    }

    public static class DataBean {
        private List<StageBean> stage;
        private List<SpecialMoneyBean> specialMoney;

        public List<StageBean> getStage() {
            return stage;
        }

        public void setStage(List<StageBean> stage) {
            this.stage = stage;
        }

        public List<SpecialMoneyBean> getSpecialMoney() {
            return specialMoney;
        }

        public void setSpecialMoney(List<SpecialMoneyBean> specialMoney) {
            this.specialMoney = specialMoney;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "stage=" + stage +
                    ", specialMoney=" + specialMoney +
                    '}';
        }

        public static class StageBean {
            /**
             * id : 22
             * contractId : 19
             * stage : 1
             * money : 57598.6
             * orderNo : CS201806131PWFEOWCWU
             * serialNo :
             * payTime :
             * payStatus : 0
             * status : 1
             * completeTime :
             * createTime : 2018-06-13 09:49:12
             * updateTime : 2018-06-13 09:49:12
             * stageId : 1
             * stagePercent : 40
             * operator :
             * stageTitle : 隐蔽
             * contractStageDivides :
             */

            private int id;
            private int contractId;
            private int stage;
            private double money;
            private String orderNo;
            private String serialNo;
            private String payTime;
            private int payStatus;
            private int status;
            private String completeTime;
            private String createTime;
            private String updateTime;
            private int stageId;
            private int stagePercent;
            private String operator;
            private String stageTitle;
            private String contractStageDivides;

            @Override
            public String toString() {
                return "StageBean{" +
                        "id=" + id +
                        ", contractId=" + contractId +
                        ", stage=" + stage +
                        ", money=" + money +
                        ", orderNo='" + orderNo + '\'' +
                        ", serialNo='" + serialNo + '\'' +
                        ", payTime='" + payTime + '\'' +
                        ", payStatus=" + payStatus +
                        ", status=" + status +
                        ", completeTime='" + completeTime + '\'' +
                        ", createTime='" + createTime + '\'' +
                        ", updateTime='" + updateTime + '\'' +
                        ", stageId=" + stageId +
                        ", stagePercent=" + stagePercent +
                        ", operator='" + operator + '\'' +
                        ", stageTitle='" + stageTitle + '\'' +
                        ", contractStageDivides='" + contractStageDivides + '\'' +
                        '}';
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getContractId() {
                return contractId;
            }

            public void setContractId(int contractId) {
                this.contractId = contractId;
            }

            public int getStage() {
                return stage;
            }

            public void setStage(int stage) {
                this.stage = stage;
            }

            public double getMoney() {
                return money;
            }

            public void setMoney(double money) {
                this.money = money;
            }

            public String getOrderNo() {
                return orderNo;
            }

            public void setOrderNo(String orderNo) {
                this.orderNo = orderNo;
            }

            public String getSerialNo() {
                return serialNo;
            }

            public void setSerialNo(String serialNo) {
                this.serialNo = serialNo;
            }

            public String getPayTime() {
                return payTime;
            }

            public void setPayTime(String payTime) {
                this.payTime = payTime;
            }

            public int getPayStatus() {
                return payStatus;
            }

            public void setPayStatus(int payStatus) {
                this.payStatus = payStatus;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getCompleteTime() {
                return completeTime;
            }

            public void setCompleteTime(String completeTime) {
                this.completeTime = completeTime;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public int getStageId() {
                return stageId;
            }

            public void setStageId(int stageId) {
                this.stageId = stageId;
            }

            public int getStagePercent() {
                return stagePercent;
            }

            public void setStagePercent(int stagePercent) {
                this.stagePercent = stagePercent;
            }

            public String getOperator() {
                return operator;
            }

            public void setOperator(String operator) {
                this.operator = operator;
            }

            public String getStageTitle() {
                return stageTitle;
            }

            public void setStageTitle(String stageTitle) {
                this.stageTitle = stageTitle;
            }

            public String getContractStageDivides() {
                return contractStageDivides;
            }

            public void setContractStageDivides(String contractStageDivides) {
                this.contractStageDivides = contractStageDivides;
            }
        }

        public static class SpecialMoneyBean {
            /**
             * id : 1
             * contractId : 19
             * money : 50000.0
             * orderNo : SP20180613174806421PWF
             * serialNo :
             * payTime :
             * payStatus :
             */

            private int id;
            private int contractId;
            private double money;
            private String orderNo;
            private String serialNo;
            private String payTime;
            private int payStatus;

            @Override
            public String toString() {
                return "SpecialMoneyBean{" +
                        "id=" + id +
                        ", contractId=" + contractId +
                        ", money=" + money +
                        ", orderNo='" + orderNo + '\'' +
                        ", serialNo='" + serialNo + '\'' +
                        ", payTime='" + payTime + '\'' +
                        ", payStatus='" + payStatus + '\'' +
                        '}';
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getContractId() {
                return contractId;
            }

            public void setContractId(int contractId) {
                this.contractId = contractId;
            }

            public double getMoney() {
                return money;
            }

            public void setMoney(double money) {
                this.money = money;
            }

            public String getOrderNo() {
                return orderNo;
            }

            public void setOrderNo(String orderNo) {
                this.orderNo = orderNo;
            }

            public String getSerialNo() {
                return serialNo;
            }

            public void setSerialNo(String serialNo) {
                this.serialNo = serialNo;
            }

            public String getPayTime() {
                return payTime;
            }

            public void setPayTime(String payTime) {
                this.payTime = payTime;
            }

            public int getPayStatus() {
                return payStatus;
            }

            public void setPayStatus(int payStatus) {
                this.payStatus = payStatus;
            }
        }
    }
}
