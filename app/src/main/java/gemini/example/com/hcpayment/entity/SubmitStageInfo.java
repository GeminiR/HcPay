package gemini.example.com.hcpayment.entity;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/6/11.
 */

public class SubmitStageInfo extends BaseResponse {

    /**
     * data : {"id":13,"contractId":1,"no":"","createTime":"","customId":"","customName":"","mobile":"","houseAddress":"","money":13000,"manager":"","stage":1,"status":4,"shopName":"","shopNo":"","orderNo":"CS201805211GWHXSZTNI","serialNo":""}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SubmitStageInfo{" +
                "data=" + data +
                '}';
    }

    public static class DataBean {
        /**
         * id : 13
         * contractId : 1
         * no :
         * createTime :
         * customId :
         * customName :
         * mobile :
         * houseAddress :
         * money : 13000
         * manager :
         * stage : 1
         * status : 4
         * shopName :
         * shopNo :
         * orderNo : CS201805211GWHXSZTNI
         * serialNo :
         */

        private int id;
        private int contractId;
        private String no;
        private String createTime;
        private String customId;
        private String customName;
        private String mobile;
        private String houseAddress;
        private int money;
        private String manager;
        private int stage;
        private int status;
        private String shopName;
        private String shopNo;
        private String orderNo;
        private String serialNo;

        @Override
        public String toString() {
            return "DataBean{" +
                    "id=" + id +
                    ", contractId=" + contractId +
                    ", no='" + no + '\'' +
                    ", createTime='" + createTime + '\'' +
                    ", customId='" + customId + '\'' +
                    ", customName='" + customName + '\'' +
                    ", mobile='" + mobile + '\'' +
                    ", houseAddress='" + houseAddress + '\'' +
                    ", money=" + money +
                    ", manager='" + manager + '\'' +
                    ", stage=" + stage +
                    ", status=" + status +
                    ", shopName='" + shopName + '\'' +
                    ", shopNo='" + shopNo + '\'' +
                    ", orderNo='" + orderNo + '\'' +
                    ", serialNo='" + serialNo + '\'' +
                    '}';
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getContractId() {
            return contractId;
        }

        public void setContractId(int contractId) {
            this.contractId = contractId;
        }

        public String getNo() {
            return no;
        }

        public void setNo(String no) {
            this.no = no;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCustomId() {
            return customId;
        }

        public void setCustomId(String customId) {
            this.customId = customId;
        }

        public String getCustomName() {
            return customName;
        }

        public void setCustomName(String customName) {
            this.customName = customName;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getHouseAddress() {
            return houseAddress;
        }

        public void setHouseAddress(String houseAddress) {
            this.houseAddress = houseAddress;
        }

        public int getMoney() {
            return money;
        }

        public void setMoney(int money) {
            this.money = money;
        }

        public String getManager() {
            return manager;
        }

        public void setManager(String manager) {
            this.manager = manager;
        }

        public int getStage() {
            return stage;
        }

        public void setStage(int stage) {
            this.stage = stage;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getShopNo() {
            return shopNo;
        }

        public void setShopNo(String shopNo) {
            this.shopNo = shopNo;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(String serialNo) {
            this.serialNo = serialNo;
        }
    }
}
