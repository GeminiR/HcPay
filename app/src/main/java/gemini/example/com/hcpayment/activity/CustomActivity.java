package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.adapter.CustomerAdapter;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.contract.CustomContract;
import gemini.example.com.hcpayment.entity.CustomBean;
import gemini.example.com.hcpayment.presenter.CustomPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;

/**
 * Created by Gemini on 2018/6/15.
 */

public class CustomActivity extends BaseActivity<CustomContract.CustomView, CustomPresenter> implements CustomContract.CustomView, SwipeRefreshLayout.OnRefreshListener {
    private CustomerAdapter customerAdapter;
    private int skipFlag = 0;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.depositNoDataLayout)
    RelativeLayout noDataLayout;
    private int currentPage = 1;

    @Override
    public void result(CustomBean customBean) {
        if (customerAdapter == null) {
            customerAdapter = new CustomerAdapter(customBean.getData().getList(), R.layout.item_guestinfo);
            customerAdapter.setItemClickListener(getPresenter());
            customRecycler.setAdapter(customerAdapter);
        } else {
            if (currentPage > 1) {
                customerAdapter.addData(customBean.getData().getList());

            } else {
                customerAdapter.setData(customBean.getData().getList());
            }

        }
        customerAdapter.setFootStatus(BaseAdapter.LOADOVER);
        customerAdapter.notifyDataSetChanged();
        if(customerAdapter.getData().size()>0){
            noDataLayout.setVisibility(View.GONE);
        }else{
            noDataLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void dealCustomInfo(CustomBean.DataBean.ListBean listBean) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(SystemArgs.CUSTOMINFO, listBean);
        intent.putExtra(SystemArgs.CUSTOMBUNDLE, bundle);
        if (skipFlag == 1) {
            //主页面跳过来的，跳去收订金
            intent.setClass(this, GatherDepositActivity.class);
            startActivity(intent);
        } else {
            intent.setClass(this, ImportGuestNoActivity.class);
            setResult(3, intent);
            finish();
        }
    }

    @Override
    public String getMobile() {
        return mobileInput.getText().toString();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public void setCurrentPage(int pageNum) {
        currentPage = pageNum;
    }

    @Override
    public CustomPresenter createPresenter() {
        return new CustomPresenter();
    }

    @Override
    public CustomContract.CustomView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("订金收取");
        mobileInput.setOnEditorActionListener(getPresenter());
        Intent intent = getIntent();
        skipFlag = intent.getIntExtra(SystemArgs.MAINPAGEFLAG, 0);
        customRecycler.setLayoutManager(new LinearLayoutManager(this));
        customRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        customSwipe.setColorSchemeResources(R.color.moregreen);
        customSwipe.setOnRefreshListener(this);
        customRecycler.setOnScrollListener(getPresenter().getOnScrollListener());

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_custom;
    }


    @Override
    public void onRefresh() {
        customSwipe.setRefreshing(false);
        currentPage = 1;
        getPresenter().queryData(currentPage);
    }

    @Override
    public void onLoadMore() {
        if (customerAdapter != null) {
            currentPage++;
            customerAdapter.setFootStatus(BaseAdapter.ISLOADING);
            customerAdapter.notifyDataSetChanged();
            getPresenter().queryData(currentPage);
        }
    }

    @Override
    public void onNoLoadMore() {
        if (customerAdapter != null) {
            customerAdapter.setFootStatus(BaseAdapter.LOADOVER);
            customerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public CustomerAdapter getCustomerAdapter() {
        return customerAdapter;
    }

    @BindView(R.id.searchGuestInfoEdit)
    EditText mobileInput;
    @BindView(R.id.customRecycler)
    RecyclerView customRecycler;
    @BindView(R.id.customSwipe)
    SwipeRefreshLayout customSwipe;
}
