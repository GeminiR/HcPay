package gemini.example.com.hcpayment.presenter;

import android.text.TextUtils;
import android.util.Log;

import java.math.BigDecimal;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.TransactionInfo;
import gemini.example.com.hcpayment.model.PayDepositModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.PayContractFeeView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/6/15.
 */

public class PayContractPresenter extends BasePresenter<PayContractFeeView> {
    private String TAG="PayContractPresenter";
    PayDepositModel payDepositModel;

    public PayContractPresenter() {
        payDepositModel=new PayDepositModel();
    }

    public void checkTransaction( String qrCodeScanModel, String paymentType, String amount, String authNo, String cardNo, String countN, String terminalId,
                                  String acquire, String traceNo, String transId, String qrOrderNo, String batchNo, String merchantId, String referenceNo, String cardType,
                                  String operatorId, String payreason, String appId, String issue, String model, String transactionPlatform, String sumAmount,
                                  String errorCode, String version, String merchantName, String cardSecret, String orderNoSFT, String accountType, String packageName,
                                  String answerCode, String voucherNo, String transDate, String transName, String transTime, String transType, String transactionType,
                                  String qrCodeTransactionState, String barcodeType, int payState,int type) {
        getView().showProgressDialog(true);
        payDepositModel.checkTransactionInfo(getView().getToken(), qrCodeScanModel,  paymentType,  amount,   authNo,
                cardNo,   countN,   terminalId,   acquire,   traceNo,   transId,   qrOrderNo,
                batchNo,   merchantId,   referenceNo,   cardType,   operatorId,   payreason,   appId,
                issue,   model,   transactionPlatform,   sumAmount,   errorCode,   version,   merchantName,
                cardSecret,   orderNoSFT,   accountType,   packageName,   answerCode,   voucherNo,
                transDate,   transName,   transTime,   transType,   transactionType,   qrCodeTransactionState,
                barcodeType, payState,type,new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                TransactionInfo transactionInfo = (TransactionInfo) o;
                getView().callbackTransactionStatus(transactionInfo);
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });

    }
    public String dealMoney(String tempMoney) {
        BigDecimal bigDecimal = new BigDecimal(tempMoney);

        String money = rvZeroAndDot(tempMoney);
        if (money.contains(".")) {
            //判断是否有小数，
            int dotIndex = money.indexOf(".");
            double dotMoney = bigDecimal.doubleValue();
            return rvZeroAndDot(dotMoney * 100 + "");

        } else {
            int intMoney = bigDecimal.intValue();
//            Log.d(TAG, "money---int" + intMoney);
            return intMoney * 100 + "";
        }

    }

    private String rvZeroAndDot(String s) {
        if (s.isEmpty()) {
            return null;
        }
        if (s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }
    //将交易金额统一处理成12位，不足12位的在前面补0
    public  String getAmount(String amount) {
        if (TextUtils.isEmpty(amount)) {
            return null;
        }

        while (amount.length() < 12) {
            amount = "0" + amount;
        }
        return amount;
    }
}
