package gemini.example.com.hcpayment.base;

import cn.pedant.SweetAlert.SweetAlertDialog;
import gemini.example.com.hcpayment.progress.ProgressCancelListener;

/**
 * Created by Gemini on 2018/3/28.
 */

public class BasePresenter<V extends BaseView>{
    public  String TAG=getClass().getSimpleName();
    V view;

    public V getView() {
        return view;
    }

    public void setView(V view) {
        this.view = view;
    }

    public void attachView(V v) {
        view = v;
    }

    public void detachView() {
        view = null;
    }



}
