package gemini.example.com.hcpayment.base;

/**
 * Created by Gemini on 2018/3/29.
 */

public class BaseResponse<T> {


    /**
     * meta : {"success":true,"message":"请求成功"}
     * data : {}
     */
    private MetaBean meta;

    public MetaBean getMeta() {
        return meta;
    }

    public void setMeta(MetaBean meta) {
        this.meta = meta;
    }


    public static class MetaBean {
        /**
         * success : true
         * message : 请求成功
         */

        private boolean success;
        private String message;
        private int code;

        @Override
        public String toString() {
            return "MetaBean{" +
                    "success=" + success +
                    ", message='" + message + '\'' +
                    ", code=" + code +
                    '}';
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "meta=" + meta +
                '}';
    }
}
