package gemini.example.com.hcpayment.adapter;

import java.util.List;

import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.entity.DepositBean;

/**
 * Created by Gemini on 2018/5/29.
 */

public class QueryDepositDialogAdapter extends BaseAdapter<DepositBean.DataBean.ListBean, BaseAdapter.BaseViewHolder> {
    public QueryDepositDialogAdapter(List<DepositBean.DataBean.ListBean> data, int layoutId) {
        super(data, layoutId);
    }

//        1待支付 2支付完成 3申请退款 4退款通过 5成功退款 6已冲抵
    @Override
    void bindViewHolder(BaseAdapter.BaseViewHolder holder, DepositBean.DataBean.ListBean data, int position) {
        holder.setText(R.id.guestInfoName,data.getCustomName());
        holder.setText(R.id.guestInfoPhone,data.getMobile());
        holder.setText(R.id.guestInfoAddress,data.getMark());
        holder.setText(R.id.guestInfactFee,data.getMoney()+"");
        holder.setText(R.id.orderTime,data.getCreateTime());
        int status=data.getStatus();
        switch (status){
            case 1:
                holder.setText(R.id.guestStatus,"待支付");
                break;
            case 2:
                holder.setText(R.id.guestStatus,"支付完成");
                break;
            case 3:
                holder.setText(R.id.guestStatus,"申请退款");
                break;
            case 4:
                holder.setText(R.id.guestStatus,"退款通过");
                break;
            case 5:
                holder.setText(R.id.guestStatus,"成功退款");
                break;
            case 6:
                holder.setText(R.id.guestStatus,"已冲抵");
                break;
            default:
                holder.setText(R.id.guestStatus,"未知状态");
                break;
        }
    }
}
