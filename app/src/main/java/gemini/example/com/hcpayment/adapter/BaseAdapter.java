package gemini.example.com.hcpayment.adapter;

import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gemini.example.com.hcpayment.R;

/**
 * Created by Gemini on 2018/5/21.
 */

public abstract class BaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> implements OnClickListener, OnLongClickListener {
    private String TAG = "BaseAdapter";
    private List<T> datas;

    private int layoutId;
    private final int ITEM_NORMAL = 0;
    private final int ITEM_FOOT = 1;
    public static final int DEFAULT_STATUS = 1;
    public static final int ISLOADING = 2;
    public static final int LOADOVER = 3;
    private int footStatus = 1;

    /**
     * Item点击事件
     */
    private onItemClickListener clickListener;
    /**
     * Item长按事件
     */
    private onItemLongClickListener longListener;

    public List<T> getData() {
        return datas;
    }

    public void setData(List<T> data) {
        this.datas = data;
    }

    public void addData(List<T> data) {
        datas.addAll(data);
    }

    public BaseAdapter(List<T> data, int layoutId) {
        this.datas = data != null ? data : new ArrayList<T>();
        if (layoutId != 0) {
            this.layoutId = layoutId;
        } else {
            throw new NullPointerException("请设置Item资源id");
        }

    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_NORMAL:
                View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
                return (VH) new BaseViewHolder(view);
            case ITEM_FOOT:
                View footView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_footview, parent, false);
                return (VH) new FootViewHolder(footView);
            default:
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        switch (holder.getItemViewType()) {
            case ITEM_NORMAL:
                holder.itemView.setTag(position);
                holder.itemView.setOnClickListener(this);
                holder.itemView.setOnLongClickListener(this);
                bindViewHolder(holder, datas.get(position), position);
                break;
            case ITEM_FOOT:
                FootViewHolder viewHolder = (FootViewHolder) holder;
                holder.itemView.setTag(position);
                switch (footStatus) {
                    case DEFAULT_STATUS:
                        viewHolder.footRelativeLayout.setVisibility(View.GONE);
                        break;
                    case ISLOADING:
                        viewHolder.footRelativeLayout.setVisibility(View.VISIBLE);
                        viewHolder.footInfo.setText("正在加载");
                        break;
                    case LOADOVER:
                        viewHolder.footRelativeLayout.setVisibility(View.VISIBLE);
                        viewHolder.footInfo.setText("-----我也是有底线的-----");
                    viewHolder.progressBar.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    public int getFootStatus() {
        return footStatus;
    }

    public void setFootStatus(int footStatus) {
        this.footStatus = footStatus;
    }


    @Override
    public int getItemCount() {
        if (datas.size() > 0) {
            return datas.size() + 1;
        }
        return 0;
    }

    abstract void bindViewHolder(VH holder, T data, int position);


    class FootViewHolder extends RecyclerView.ViewHolder {
        TextView footInfo;
        ProgressBar progressBar;
        RelativeLayout footRelativeLayout;

        public FootViewHolder(View itemView) {
            super(itemView);
            footInfo = itemView.findViewById(R.id.footText);
            progressBar = itemView.findViewById(R.id.footProgress);
            footRelativeLayout=itemView.findViewById(R.id.footRelative);
        }
    }

    class BaseViewHolder extends RecyclerView.ViewHolder {
        private SparseArray<View> allViews;

        public BaseViewHolder(View itemView) {
            super(itemView);
            allViews = new SparseArray<>();
        }

        private <D extends View> D findViewById(int viewId) {
            View v = allViews.get(viewId);
            if (v == null) {
                v = itemView.findViewById(viewId);
                allViews.put(viewId, v);
            }
            return (D) v;
        }

        /**
         * 设置文本资源
         *
         * @param viewId view id
         * @param s      字符
         */
        public TextView setText(int viewId, CharSequence s) {
            TextView view = findViewById(viewId);
            view.setText(s);
            return view;
        }

        /**
         * 设置图片资源
         *
         * @param viewId     view id
         * @param imageResId 图片资源id
         */
        public ImageView setImageResource(int viewId, @DrawableRes int imageResId) {
            ImageView view = findViewById(viewId);
            view.setImageResource(imageResId);
            return view;
        }
    }

    @Override
    public void onClick(View v) {
        if (clickListener != null) {
            clickListener.onItemClick((Integer) v.getTag(), v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (longListener != null) {
            longListener.onItemLonClick((Integer) v.getTag(), v);
        }
        return true;
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= datas.size()) {
            return ITEM_FOOT;
        }
        return ITEM_NORMAL;
    }

    /**
     * 设置点击监听
     *
     * @param clickListener 监听器
     */
    public void setItemClickListener(onItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    /**
     * 设置长按监听
     *
     * @param longListener 监听器
     */
    public void setItemLongClickListener(onItemLongClickListener longListener) {
        this.longListener = longListener;
    }

    public interface onItemClickListener {
        void onItemClick(int position, View v);
    }

    public interface onItemLongClickListener {
        boolean onItemLonClick(int position, View v);
    }
}

