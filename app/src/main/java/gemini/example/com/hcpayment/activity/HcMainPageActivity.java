package gemini.example.com.hcpayment.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Stack;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.model.DepositManagerModel;
import gemini.example.com.hcpayment.presenter.HcMainPagePresenter;
import gemini.example.com.hcpayment.utils.ApplicationUtil;
import gemini.example.com.hcpayment.utils.SharePreUtils;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.HcMainPageView;

/**
 * Created by Gemini on 2018/5/30.
 */

public class HcMainPageActivity extends BaseActivity<HcMainPageView, HcMainPagePresenter> implements HcMainPageView {

    @BindView(R.id.tv_title)
    TextView titleText;
    private boolean isLogin;
    @BindView(R.id.accountName)
    TextView accountName;

    @Override
    public HcMainPagePresenter createPresenter() {
        return new HcMainPagePresenter();
    }

    @Override
    public HcMainPageView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("和宸之家商家端");
        String name= SharePreUtils.getString(this,SystemArgs.ACCOUNTNAME,"");
        accountName.setText(name);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_sellmain;
    }

    @OnClick({R.id.createOrderRelative, R.id.getDepositRelative, R.id.depositManager, R.id.payReceiveRelative, R.id.orderManager,
            R.id.customQueryRelative,R.id.receiveInfoRelative,R.id.changeAccount})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.createOrderRelative:
                //创建订单
                skipActivity(CreateOrderActivity.class, null);
                break;
            case R.id.getDepositRelative:
                Intent intent = new Intent(this, ImportGuestNoActivity.class);
                intent.putExtra(SystemArgs.MAINPAGEFLAG, 1);
                startActivity(intent);
                break;
            case R.id.depositManager:
                //订金管理
                skipActivity(DepositManagerActivity.class, null);
                break;
            case R.id.payReceiveRelative:
                skipActivity(PayReceiveActivity.class, null);
                break;
            case R.id.orderManager:
                skipActivity(OrderManagerActivity.class, null);
                break;
            case R.id.customQueryRelative:
                skipActivity(QueryCustomActivity.class, null);
                break;
            case R.id.receiveInfoRelative:
                skipActivity(ReceiveInfoActivity.class, null);
                break;
            case R.id.changeAccount:
                skipActivity(LoginActivity.class,null);
                finish();
                Stack<Activity> store= ApplicationUtil.store;
                if(store!=null){
                    for (int i = 0; i < store.size(); i++) {
                        Activity activity=store.get(i);
                        activity.finish();
                    }
                }
                break;
            default:
                break;
        }

    }

}
