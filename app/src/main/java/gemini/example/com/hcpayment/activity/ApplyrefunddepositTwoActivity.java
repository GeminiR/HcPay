package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.DepositBean;
import gemini.example.com.hcpayment.presenter.ApplyDepositTwoPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.ApplyDepositTwoView;

/**
 * Created by Gemini on 2018/7/12.
 */

public class ApplyrefunddepositTwoActivity extends BaseActivity<ApplyDepositTwoView, ApplyDepositTwoPresenter> implements ApplyDepositTwoView
,TextWatcher{
    int id = 0;

    @Override
    public ApplyDepositTwoPresenter createPresenter() {
        return new ApplyDepositTwoPresenter();
    }

    @Override
    public ApplyDepositTwoView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("申请退还订金");
        cardModeRelative.setVisibility(View.GONE);
        Intent intent = getIntent();
        if (intent != null) {
            DepositBean.DataBean.ListBean bean = (DepositBean.DataBean.ListBean) intent.getSerializableExtra(SystemArgs.DEPOSITINFO);
            guestInfoName.setText(bean.getCustomName());
            mobileNum.setText(bean.getMobile());
            operationMan.setText(bean.getOperator());
            cardNum.setText(bean.getCard());
            payMoneyNum.setText(bean.getMoney()+"");
            id = bean.getId();
        }
        cardNumInput.addTextChangedListener(this);
    }

    @OnClick({R.id.replyRefund})
    public void onViewClick(View v) {
        switch (v.getId()) {
            case R.id.replyRefund:
                String name=cardCustomName.getText().toString();
                String  mode=cardMode.getText().toString();
                String cardNum=cardNumInput.getText().toString();
                String remark=remarkInput.getText().toString();
                if(!TextUtils.isEmpty(name)){
                    if(!TextUtils.isEmpty(mode)){
                        if(!TextUtils.isEmpty(cardNum)){
                            getPresenter().applyRefound(token, id,cardNum,name,mode,remark);
                        }else{
                            ToastUtil.showShortToast("请填写银行卡号");
                        }
                    }else{
                        ToastUtil.showShortToast("请填写银行卡种");
                    }
                }else{
                    ToastUtil.showShortToast("请填写持卡人姓名");
                }

                break;
            default:
                break;
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_applyrefunddeposit;
    }

    @BindView(R.id.tv_title)
    TextView titleText;

    @Override
    public void callBackRefoundSucccess() {
        Intent intent = new Intent(this, ApplyDepositRefoundSuccessActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void callbackRefoundFail() {
        ToastUtil.showShortToast("申请退还订金失败");
    }

    @Override
    public void callbackBankName(String bankName) {
        cardMode.setText(bankName);
    }

    @BindView(R.id.cardNum)
    TextView cardNum;
    @BindView(R.id.cardModeRelative)
    RelativeLayout cardModeRelative;
    @BindView(R.id.guestInfoName)
    TextView guestInfoName;
    @BindView(R.id.mobileNum)
    TextView mobileNum;
    @BindView(R.id.operationMan)
    TextView operationMan;
    @BindView(R.id.cardCustomName)
    EditText cardCustomName;
    @BindView(R.id.cardModeInput)
    EditText cardMode;
    @BindView(R.id.cardNumInput)
    EditText cardNumInput;
    @BindView(R.id.remarkInput)
    EditText remarkInput;
    @BindView(R.id.payMoneyNum)
    TextView payMoneyNum;

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
       String content=s.toString().trim();
       if(content.length()==16){
           getPresenter().checkBankName(content);
       }
    }
}

