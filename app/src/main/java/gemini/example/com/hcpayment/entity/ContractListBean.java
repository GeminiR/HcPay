package gemini.example.com.hcpayment.entity;

import java.io.Serializable;
import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/6/1.
 */

public class ContractListBean  extends BaseResponse implements Serializable{


    /**
     * data : {"pages":1,"total":4,"next":"","prev":"","list":[{"id":13,"contractId":1,"no":"HC001-0000000001","createTime":"2018-05-18 10:18:54","customId":5,"customName":"","mobile":"13810426535","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","money":13000,"manager":"","stage":1,"status":1,"shopName":"","shopNo":"","orderNo":"CS201805211GWHXSZTNI","serialNo":""},{"id":14,"contractId":1,"no":"HC001-0000000001","createTime":"2018-05-18 10:18:54","customId":5,"customName":"","mobile":"13810426535","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","money":26000,"manager":"","stage":2,"status":1,"shopName":"","shopNo":"","orderNo":"CS201805212GWHXSZTNI","serialNo":""},{"id":15,"contractId":1,"no":"HC001-0000000001","createTime":"2018-05-18 10:18:54","customId":5,"customName":"","mobile":"13810426535","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","money":91000,"manager":"","stage":3,"status":1,"shopName":"","shopNo":"","orderNo":"CS201805213GWHXSZTNI","serialNo":""},{"id":36,"contractId":2,"no":"HC001-0000000002","createTime":"2018-05-25 03:38:13","customId":6,"customName":"","mobile":"15325812129","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","money":117654.21,"manager":"刘颜菘","stage":1,"status":1,"shopName":"","shopNo":"","orderNo":"CS201805301MCMFDUV3Q","serialNo":""}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ContractListBean{" +
                "data=" + data +
                '}';
    }

    public static class DataBean implements Serializable{
        /**
         * pages : 1
         * total : 4
         * next :
         * prev :
         * list : [{"id":13,"contractId":1,"no":"HC001-0000000001","createTime":"2018-05-18 10:18:54","customId":5,"customName":"","mobile":"13810426535","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","money":13000,"manager":"","stage":1,"status":1,"shopName":"","shopNo":"","orderNo":"CS201805211GWHXSZTNI","serialNo":""},{"id":14,"contractId":1,"no":"HC001-0000000001","createTime":"2018-05-18 10:18:54","customId":5,"customName":"","mobile":"13810426535","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","money":26000,"manager":"","stage":2,"status":1,"shopName":"","shopNo":"","orderNo":"CS201805212GWHXSZTNI","serialNo":""},{"id":15,"contractId":1,"no":"HC001-0000000001","createTime":"2018-05-18 10:18:54","customId":5,"customName":"","mobile":"13810426535","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","money":91000,"manager":"","stage":3,"status":1,"shopName":"","shopNo":"","orderNo":"CS201805213GWHXSZTNI","serialNo":""},{"id":36,"contractId":2,"no":"HC001-0000000002","createTime":"2018-05-25 03:38:13","customId":6,"customName":"","mobile":"15325812129","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","money":117654.21,"manager":"刘颜菘","stage":1,"status":1,"shopName":"","shopNo":"","orderNo":"CS201805301MCMFDUV3Q","serialNo":""}]
         */

        private int pages;
        private int total;
        private String next;
        private String prev;
        private List<ListBean> list;

        @Override
        public String toString() {
            return "DataBean{" +
                    "pages=" + pages +
                    ", total=" + total +
                    ", next='" + next + '\'' +
                    ", prev='" + prev + '\'' +
                    ", list=" + list +
                    '}';
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrev() {
            return prev;
        }

        public void setPrev(String prev) {
            this.prev = prev;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean  implements Serializable{
            /**
             * id : 13
             * contractId : 1
             * no : HC001-0000000001
             * createTime : 2018-05-18 10:18:54
             * customId : 5
             * customName :
             * mobile : 13810426535
             * houseAddress : 杭州市西湖区紫霞街188号西溪蝶园-北区
             * money : 13000
             * manager :
             * stage : 1
             * status : 1
             * shopName :
             * shopNo :
             * orderNo : CS201805211GWHXSZTNI
             * serialNo :
             */

            private int id;
            private int contractId;
            private String no;
            private String createTime;
            private int customId;
            private String customName;
            private String mobile;
            private String houseAddress;
            private double money;
            private String manager;
            private int stage;
            private int status;
            private String shopName;
            private String shopNo;
            private String orderNo;
            private String serialNo;

            @Override
            public String toString() {
                return "ListBean{" +
                        "id=" + id +
                        ", contractId=" + contractId +
                        ", no='" + no + '\'' +
                        ", createTime='" + createTime + '\'' +
                        ", customId=" + customId +
                        ", customName='" + customName + '\'' +
                        ", mobile='" + mobile + '\'' +
                        ", houseAddress='" + houseAddress + '\'' +
                        ", money=" + money +
                        ", manager='" + manager + '\'' +
                        ", stage=" + stage +
                        ", status=" + status +
                        ", shopName='" + shopName + '\'' +
                        ", shopNo='" + shopNo + '\'' +
                        ", orderNo='" + orderNo + '\'' +
                        ", serialNo='" + serialNo + '\'' +
                        '}';
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getContractId() {
                return contractId;
            }

            public void setContractId(int contractId) {
                this.contractId = contractId;
            }

            public String getNo() {
                return no;
            }

            public void setNo(String no) {
                this.no = no;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public int getCustomId() {
                return customId;
            }

            public void setCustomId(int customId) {
                this.customId = customId;
            }

            public String getCustomName() {
                return customName;
            }

            public void setCustomName(String customName) {
                this.customName = customName;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getHouseAddress() {
                return houseAddress;
            }

            public void setHouseAddress(String houseAddress) {
                this.houseAddress = houseAddress;
            }

            public double getMoney() {
                return money;
            }

            public void setMoney(double money) {
                this.money = money;
            }

            public String getManager() {
                return manager;
            }

            public void setManager(String manager) {
                this.manager = manager;
            }

            public int getStage() {
                return stage;
            }

            public void setStage(int stage) {
                this.stage = stage;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getShopName() {
                return shopName;
            }

            public void setShopName(String shopName) {
                this.shopName = shopName;
            }

            public String getShopNo() {
                return shopNo;
            }

            public void setShopNo(String shopNo) {
                this.shopNo = shopNo;
            }

            public String getOrderNo() {
                return orderNo;
            }

            public void setOrderNo(String orderNo) {
                this.orderNo = orderNo;
            }

            public String getSerialNo() {
                return serialNo;
            }

            public void setSerialNo(String serialNo) {
                this.serialNo = serialNo;
            }
        }
    }
}
