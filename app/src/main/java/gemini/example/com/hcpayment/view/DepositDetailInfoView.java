package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.ApplyRefoundInfoBean;

/**
 * Created by Gemini on 2018/5/31.
 */

public interface DepositDetailInfoView extends BaseView {
    void callBackApplyInfo(ApplyRefoundInfoBean applyRefoundInfoBean);
    void callBackRefoundSucccess();
    void callbackRefoundFail();
}
