package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.adapter.QueryCustomAdapter;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.QueryCustomBean;

/**
 * Created by Gemini on 2018/6/14.
 */

public interface QueryCustomView extends BaseView {

    String getMobile();
    void onCallbackCustomInfo(QueryCustomBean queryCustomBean);
    void onCallbackItemClick(QueryCustomBean.DataBean.ListBean listBean);
    QueryCustomAdapter getAdapter();
    void onLoadMore();
    void onNoLoadMore();

}
