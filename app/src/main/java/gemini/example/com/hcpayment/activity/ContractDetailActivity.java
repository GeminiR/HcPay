package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.QueryContractBean;
import gemini.example.com.hcpayment.entity.QueryContractStatusInfo;
import gemini.example.com.hcpayment.presenter.ContractDetailPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.ContractDetailView;

/**
 * Created by Gemini on 2018/6/4.
 */

public class ContractDetailActivity extends BaseActivity<ContractDetailView, ContractDetailPresenter> implements ContractDetailView {
    public int contractId = 0;

    @Override
    public ContractDetailPresenter createPresenter() {
        return new ContractDetailPresenter();
    }

    @Override
    public ContractDetailView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("合同详情");
        Intent intent = getIntent();
        if (intent != null) {
            QueryContractStatusInfo.DataBean.ListBean listBean =
                    (QueryContractStatusInfo.DataBean.ListBean) intent.getSerializableExtra(SystemArgs.CONTRACTINFO);
            contractId = listBean.getId();
            contractName.setText(listBean.getCustomName());
            contractNum.setText(listBean.getNo());
            decorateAddress.setText(listBean.getHouseAddress());
            decorateArea.setText(listBean.getHouseDecorationArea() + "");
            contractAllMoney.setText(listBean.getActualMoney() + "元");
            selectProduct.setText(listBean.getProductName());
            selectProductSingle.setText(listBean.getProductPrice() + "元");
            contractTime.setText("您在" + listBean.getCreateTime() + "于和宸之家-城市客厅-西溪店签约了一份整装合同");
            getPresenter().queryContractStageInfo(contractId);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_contract;
    }

    @Override
    public void onCallbackStatusAndMoney(HashMap<Integer, Integer> statusMap, HashMap<Integer, Double> moneyMap) {
        for (int key : statusMap.keySet()) {
            switch (key) {
                case 1:
                    updateContractUI(oneFeeStatus, oneStageFee, moneyMap.get(1), statusMap.get(1), 1);
                    break;
                case 2:
                    updateContractUI(twoFeeStatus, twoStageFee, moneyMap.get(2), statusMap.get(2), 2);
                    break;
                case 3:
                    updateContractUI(threeFeeStatus, threeStageFee, moneyMap.get(3), statusMap.get(3), 3);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onCallbackSpecailMoney(int specialMoney, int specialStatus) {
        updateContractUI(specialStatusText, specialMoneyText, specialMoney, specialStatus, 0);

    }

    @Override
    public void onCallbackSubmitSuccess() {
        getPresenter().queryContractStageInfo(contractId);
    }

    @Override
    public void onCallbackSubmitFail() {

    }


    public void updateContractUI(TextView statusText, TextView moneyText, double money, int status, int key) {
        if (status == 0) {
            switch (key) {
                case 0:
                    executionResult.setText("个性定制阶段");
                    break;
                case 1:
                    executionResult.setText("隐蔽工程验收");
                    break;
                case 2:
                    executionResult.setText("主体安装验收");
                    break;
                case 3:
                    executionResult.setText("软装进场验收");
                    break;
                default:
                    break;
            }
            statusText.setText("待付");
            statusText.setBackgroundResource(R.drawable.backorange);
            moneyText.setTextColor(getResources().getColor(R.color.backorange));
        } else {
            statusText.setText("已付");
            statusText.setBackgroundResource(R.drawable.backgreen);
            moneyText.setTextColor(getResources().getColor(R.color.backGreen));
        }
        moneyText.setText(money + "");
    }

    @OnClick({R.id.submitCheck})
    public void onClickView(View view) {
        switch (view.getId()) {
            case R.id.submitCheck:
                int noPayId = getPresenter().getDefaultNoPayId();
                getPresenter().submitCheck(noPayId);

                break;
            default:
                break;
        }
    }

    @BindView(R.id.oneFeeStatus)
    TextView oneFeeStatus;
    @BindView(R.id.twoFeeStatus)
    TextView twoFeeStatus;
    @BindView(R.id.threeFeeStatus)
    TextView threeFeeStatus;
    @BindView(R.id.oneStageFee)
    TextView oneStageFee;
    @BindView(R.id.twoStageFee)
    TextView twoStageFee;
    @BindView(R.id.threeStageFee)
    TextView threeStageFee;
    @BindView(R.id.specialStatus)
    TextView specialStatusText;
    @BindView(R.id.specialMoney)
    TextView specialMoneyText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.contractName)
    TextView contractName;
    @BindView(R.id.contractNum)
    TextView contractNum;//合同编号
    @BindView(R.id.decorateAddress)
    TextView decorateAddress;
    @BindView(R.id.decorateArea)
    TextView decorateArea;
    @BindView(R.id.contractAllMoney)
    TextView contractAllMoney;
    @BindView(R.id.selectProduct)
    TextView selectProduct;
    @BindView(R.id.selectProductSingle)
    TextView selectProductSingle;
    @BindView(R.id.executionResult)
    TextView executionResult;
    @BindView(R.id.contractTime)
    TextView contractTime;
}
