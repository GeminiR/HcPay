package gemini.example.com.hcpayment.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.activity.LoginActivity;
import gemini.example.com.hcpayment.activity.SplashActivity;
import gemini.example.com.hcpayment.progress.ProgressCancelListener;
import gemini.example.com.hcpayment.utils.SharePreUtils;
import gemini.example.com.hcpayment.utils.SystemArgs;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/3/28.
 */

public abstract class BaseActivity<V extends BaseView, P extends BasePresenter> extends AppCompatActivity implements BaseView, ProgressCancelListener {
    public final String TAG = getClass().getSimpleName();
    private V view;
    private P presenter;
    public SweetAlertDialog sweetAlertDialog;
    public Unbinder unbinder;
    public String token = null;
    public LinkedHashMap<Integer, Disposable> disposableHashMap;

    Toolbar toolbar;

    public abstract P createPresenter();

    public abstract V createView();

    public abstract void init();

    public P getPresenter() {
        return presenter;
    }

    public abstract int getLayoutId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        unbinder = ButterKnife.bind(this);
        if (presenter == null) {
            presenter = createPresenter();
        }

        if (view == null) {
            view = createView();
        }
        if (presenter != null && view != null) {
            presenter.attachView(view);
        }
        disposableHashMap = new LinkedHashMap<>();
        token = SharePreUtils.getString(this, SystemArgs.TOKEN, "");
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR|View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        if (!(this instanceof SplashActivity)) {
            toolbar = findViewById(R.id.toolbar);
            setHeight(toolbar);
            getWindow().getDecorView().setPadding(0,0,0,getNavigationBarHeight());
        }
        init();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.detachView();
        }
        unbinder.unbind();
        if (disposableHashMap != null) {
            disposableHashMap.clear();
            disposableHashMap = null;
        }

    }

    public void skipActivity(Class<? extends Activity> _class, Bundle bundle) {
        Intent intent = new Intent(this, _class);
        if (bundle != null) {
            intent.putExtra("bundle", bundle);
        }
        startActivity(intent);
    }


    public void onBackClick(View v) {
        onBackPressed();
    }

    @Override
    public void showProgressDialog(boolean isCancelable) {
        showProgress(isCancelable);
    }

    @Override
    public void hideProgressDialog() {
        hideProgress();
    }

    public void showProgress(boolean isCancelable) {
        if (sweetAlertDialog == null) {
            sweetAlertDialog = new SweetAlertDialog(this);
            sweetAlertDialog.setTitleText("正在加载中...");
            sweetAlertDialog.setCancelable(isCancelable);
            if (isCancelable) {
                sweetAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        cancelProgress();
                    }
                });
            }
            if (!sweetAlertDialog.isShowing()) {
                sweetAlertDialog.show();
            }
        }
    }


    @Override
    public void putDispose(Disposable disposable) {
        if (sweetAlertDialog != null&& disposable!=null) {
            disposableHashMap.put(sweetAlertDialog.hashCode(), disposable);
        }
    }

    public void hideProgress() {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.dismiss();
            sweetAlertDialog = null;
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public void cancelProgress() {
        if (sweetAlertDialog != null) {
            Disposable d = disposableHashMap.get(sweetAlertDialog.hashCode());
            if (d!=null&&!d.isDisposed()) {
                d.dispose();
            }
        }
    }

    public void setHeight(View view) {
        // 获取actionbar的高度
        TypedArray actionbarSizeTypedArray = obtainStyledAttributes(new int[]{
                android.R.attr.actionBarSize
        });
        float height = actionbarSizeTypedArray.getDimension(0, 0);
        // ToolBar的top值
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        double statusBarHeight = getStatusBarHeight(this);
        double navigationBarHight=getNavigationBarHeight();
        lp.height = (int) (statusBarHeight + height);
        view.setPadding(0, (int) statusBarHeight, 0, 0);
        view.setLayoutParams(lp);
    }

    private double getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen",
                "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
    public int getNavigationBarHeight() {

        boolean hasMenuKey = ViewConfiguration.get(this).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        //判断是否有虚拟按钮
        if (!hasMenuKey) {
            Resources resources = getResources();
            int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
            //获取NavigationBar的高度
            int height = resources.getDimensionPixelSize(resourceId);
            return height;
        } else {
            return 0;
        }
    }
    public void changeStatusBarTextColor(boolean isBlack) {
//        Window window=getWindow();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // 5.0 以上全透明状态栏
//            //取消设置透明状态栏,使 ContentView 内容不再覆盖状态栏 加下面几句可以去除透明状态栏的灰色阴影,实现纯透明
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            //需要设置这个 flag 才能调用 setStatusBarColor 来设置状态栏颜色
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.getDecorView().setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//            //6.0 以上可以设置状态栏的字体为黑色.使用下面注释的这行打开亮色状态栏模式,实现黑色字体,白底的需求用这句setStatusBarColor(Color.WHITE);
////            window.getDecorView().setSystemUiVisibility(
////                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//            window.setStatusBarColor(Color.TRANSPARENT);
//
//        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){//4.4 全透明状态栏
//            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        }

    }
}
