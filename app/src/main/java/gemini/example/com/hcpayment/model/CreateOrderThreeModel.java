package gemini.example.com.hcpayment.model;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import io.reactivex.Observable;

/**
 * Created by Gemini on 2018/5/30.
 */

public class CreateOrderThreeModel extends BaseModel {

    public void keepInfo(String token,int contractId,ObserverResponseListener observerResponseListener){
//        JsonObject jsonObject=new JsonObject();
//        jsonObject.addProperty("contractId",contractId);
//        String json=new Gson().toJson(jsonObject);
        subscribe( Api.getApiSevice().keepContractInfo(token,contractId),observerResponseListener);
    }
}
