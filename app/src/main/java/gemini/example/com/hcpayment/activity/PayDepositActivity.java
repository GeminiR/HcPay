package gemini.example.com.hcpayment.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.CustomBean;
import gemini.example.com.hcpayment.entity.KeepDepositBean;
import gemini.example.com.hcpayment.entity.TransactionInfo;
import gemini.example.com.hcpayment.presenter.PayDepositPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.PayDepositView;

/**
 * Created by Gemini on 2018/5/30.
 */

public class PayDepositActivity extends BaseActivity<PayDepositView, PayDepositPresenter> implements PayDepositView {
    String money;
    private String TAG = "PayDepositActivity";
    private String orderNum = "";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.payMoneyNum)
    TextView payMoneyNum;
    @BindView(R.id.customName)
    TextView customName;
    @BindView(R.id.decorateAddress)
    TextView decorateAdd;
    @BindView(R.id.loginMobile)
    TextView registerMobile;
    @BindView(R.id.surePay)
    Button surePay;
    private int transcationFlag = 0;
    private int customHouseId = 0;
     String qrCodeScanModel="";
     String paymentType="";
     String amount="";
     String authNo="";
     String cardNo="";
     String countN="";
     String terminalId="";
     String acquire="";
     String traceNo="";
     String transId="";
     String qrOrderNo="";
     String batchNo="";
     String merchantId="";
     String referenceNo="";
     String cardType="";
     String operatorId="";
     String payreason="";
     String appId="";
     String issue="";
     String model="";
     String transactionPlatform="";
     String sumAmount="";
     String errorCode="";
     String version="";
     String merchantName="";
     String cardSecret="";
     String orderNoSFT="";
     String accountType="";
     String packageName="";
     String answerCode="";
     String voucherNo="";
     String transDate="";
     String transName="";
     String transTime="";
     String transType="";
     String transactionType="";
     String qrCodeTransactionState="";
     String barcodeType="";


    @Override
    public PayDepositPresenter createPresenter() {
        return new PayDepositPresenter();
    }

    @Override
    public PayDepositView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("收取订金");
        changeStatusBarTextColor(true);
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getBundleExtra(SystemArgs.CUSTOMBUNDLE);
            CustomBean.DataBean.ListBean listBean = (CustomBean.DataBean.ListBean) bundle.getSerializable(SystemArgs.CUSTOMINFO);
            money = bundle.getString(SystemArgs.INPUTDEPOSIT);
            customHouseId = bundle.getInt(SystemArgs.CUSTOMHOUSEID);
            payMoneyNum.setText(money + "");
            customName.setText(listBean.getName());
            decorateAdd.setText(listBean.getAddress());
            registerMobile.setText(listBean.getMobile());
            keepDeposit(listBean.getId(), money, customHouseId);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_deposit_surepay;
    }

    @Override
    public void keepDeposit(int customId, String money, int customHouseId) {
        getPresenter().keepDeposit(customId, money, customHouseId);
    }

    @Override
    public void callbackDepositInfo(KeepDepositBean keepDepositBean) {
        orderNum = keepDepositBean.getData().getOrderNo();
    }

    @Override
    public void callbackTransactionStatus(TransactionInfo transactionInfo) {
        transcationFlag = 0;
        surePay.setText("确认付款");
        int status = transactionInfo.getData().getPayStatus();
        switch (status) {
            case 0:
                ToastUtil.showShortToast("支付中请稍候");
                transcationFlag = 1;
                surePay.setText("再次点击刷新交易信息");
                break;
            case 1:
                ToastUtil.showShortToast("支付成功");
                Intent intent = new Intent(this, PayDepositSuccessActivity.class);
                intent.putExtra(SystemArgs.PAYINFO, transactionInfo.getData());
                intent.putExtra(SystemArgs.moneyType, 1);
                startActivity(intent);
                break;
            case 2:
                ToastUtil.showShortToast("订单号错误");
                break;
            case 3:
                ToastUtil.showShortToast("系统错误");
                break;
            default:
                break;
        }
    }

    @Override
    public void callbackTransactionFail() {
        transcationFlag = 1;
        surePay.setText("交易失败,再次点击刷新交易信息");
    }

    @OnClick({R.id.surePay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.surePay:
//                if(transcationFlag==0){
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.shengpay.smartpos.shengpaysdk", "com.shengpay.smartpos.shengpaysdk.activity.MainActivity"));
                intent.putExtra("appId", getPackageName());
                intent.putExtra("transName", TRANSACTION);
                intent.putExtra("barcodeType", CREDIT);
                intent.putExtra("orderNoSFT", orderNum);
                String dealMoney = getPresenter().dealMoney(money);
                intent.putExtra("amount", getAmount(dealMoney));
                startActivityForResult(intent, 0);
//                }else{
//                    getPresenter().checkTransaction(orderNum, cardNo, terminalId,
//                            traceNo, merchantNameEn, merchantId,
//                            referenceNo,
//                            issue, merchantName, cardSecret, orderNoSFT,
//                            transDate, sftOrderNum, transTime, 1, 0);
//                }
                break;
            default:
                break;
        }
    }

    //将交易金额统一处理成12位，不足12位的在前面补0
    private String getAmount(String amount) {
        if (TextUtils.isEmpty(amount)) {
            return null;
        }

        while (amount.length() < 12) {
            amount = "0" + amount;
        }
        return amount;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {

            Bundle extras1 = data.getExtras();
            Set<String> keySet = extras1.keySet();
            for (String s : keySet) {
                Log.d(TAG, "onActivityResult: " + s + ":" + extras1.get(s));
            }
//            if (requestCode == Activity.RESULT_OK) {
            // 打印商户信息
            qrCodeScanModel=extras1.getString("qrCodeScanModel");
            paymentType =extras1.getString("paymentType");
            amount= extras1.getString("amount");
            authNo =extras1.getString("authNo");
            cardNo= extras1.getString("cardNo");
            countN =extras1.getString("countN");
            terminalId= extras1.getString("terminalId");
            acquire= extras1.getString("acquire");
            traceNo= extras1.getString("traceNo");
            transId= extras1.getString("transId");
            qrOrderNo= extras1.getString("qrOrderNo");
            batchNo= extras1.getString("batchNo");
            merchantId= extras1.getString("merchantId");
            referenceNo= extras1.getString("referenceNo");
            cardType= extras1.getString("cardType");
            operatorId= extras1.getString("operatorId");
            payreason= extras1.getString("payreason");
            appId= extras1.getString("appId");
            issue= extras1.getString("issue");
            model= extras1.getString("model");
            transactionPlatform= extras1.getString("transactionPlatform");
            sumAmount= extras1.getString("sumAmount");
            errorCode= extras1.getString("errorCode");
            version= extras1.getString("version");
            merchantName= extras1.getString("merchantName");
            cardSecret= extras1.getString("cardSecret");
            orderNoSFT= extras1.getString("orderNoSFT");
            accountType= extras1.getString("accountType");
            packageName= extras1.getString("packageName");
            answerCode= extras1.getString("answerCode");
            voucherNo= extras1.getString("voucherNo");
            transDate= extras1.getString("transDate");
            transName= extras1.getString("transName");
            transTime= extras1.getString("transTime");
            transType= extras1.getString("transType");
            transactionType= extras1.getString("transactionType");
            qrCodeTransactionState= extras1.getString("qrCodeTransactionState");
            barcodeType= extras1.getString("barcodeType");

            switch (resultCode) {
                case Activity.RESULT_OK:
                    getPresenter().checkTransaction(   qrCodeScanModel,
                    paymentType, amount, authNo , cardNo, countN , terminalId, acquire, traceNo,
                    transId, qrOrderNo, batchNo, merchantId, referenceNo, cardType, operatorId, payreason,
                    appId, issue, model, transactionPlatform, sumAmount, errorCode, version, merchantName,
                    cardSecret, orderNoSFT, accountType, packageName, answerCode, voucherNo, transDate,
                    transName, transTime, transType, transactionType, qrCodeTransactionState, barcodeType,
                    1,1);
                    break;
                case Activity.RESULT_CANCELED:
                    String reason = data.getStringExtra("reason");
                    ToastUtil.showShortToast(reason);
                    break;
                default:
                    break;
            }
        }
    }


    //交易类型
    static final String TRANSACTION = "0";
    //交易方式
    static final String CREDIT = "0";
    static final String WECHAT = "1";
    static final String ALIPAY = "2";
    static final String SDP = "3";
    static final String RICHSCAN = "4";
    static final String UNIONPAY = "5";
    static final String SCAN_WX = "6";
    static final String SCAN_ZFB = "7";
}
