package gemini.example.com.hcpayment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.entity.ReceiveInfoBean;

/**
 * Created by Gemini on 2018/6/16.
 */

public class ReceiveInfoAdapter extends BaseExpandableListAdapter {

    private  int  status;
    Context ctx;
    private List<ReceiveInfoBean.DataBean.ListBean> dataList;

    public void addData(List<ReceiveInfoBean.DataBean.ListBean> data){
        dataList.addAll(data);
    }
    public List<ReceiveInfoBean.DataBean.ListBean> getDataList() {
        return dataList;
    }

    public void setDataList(List<ReceiveInfoBean.DataBean.ListBean> dataList) {
        this.dataList = dataList;
    }

    public ReceiveInfoAdapter(Context ctx) {
        this.ctx=ctx;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    @Override
    public int getGroupCount() {
        return dataList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return dataList.get(groupPosition).getContractStages().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return dataList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return dataList.get(groupPosition).getContractStages().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder groupViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(R.layout.expand_groupview, parent, false);
            groupViewHolder = new GroupViewHolder();
            groupViewHolder.customName= convertView.findViewById(R.id.customName);
            groupViewHolder.no=convertView.findViewById(R.id.contractNo);
            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }
        groupViewHolder.customName.setText(dataList.get(groupPosition).getCustomName());
        groupViewHolder.no.setText(dataList.get(groupPosition).getNo());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder childViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(R.layout.expand_childview, parent, false);
            childViewHolder = new ChildViewHolder();
            childViewHolder.stageInfo = (TextView) convertView.findViewById(R.id.stage);
            childViewHolder.stageTitle= (TextView) convertView.findViewById(R.id.stageTitle);
            childViewHolder.money=convertView.findViewById(R.id.receiveMoney);
            convertView.setTag(childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }
        int stage=dataList.get(groupPosition).getContractStages().get(childPosition).getStage() ;
        String stageTitle=dataList.get(groupPosition).getContractStages().get(childPosition).getStageTitle();
        double money=dataList.get(groupPosition).getContractStages().get(childPosition).getMoney();
        childViewHolder.stageInfo.setText("第"+stage+"阶段");
        childViewHolder.stageTitle.setText("阶段主题:"+stageTitle);
        if(status==1){
            childViewHolder.money.setText("应付:"+money);
        }else{
            childViewHolder.money.setText("已付:"+money);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public class GroupViewHolder{
        TextView customName;

        TextView no;

    }
    public class ChildViewHolder{
        TextView stageInfo;

        TextView stageTitle;

        TextView money;

    }
}
