package gemini.example.com.hcpayment.api;

import java.util.List;
import java.util.Map;

import gemini.example.com.hcpayment.base.BaseResponse;
import gemini.example.com.hcpayment.entity.ApplyRefoundInfoBean;
import gemini.example.com.hcpayment.entity.ContractListBean;
import gemini.example.com.hcpayment.entity.CustomBean;
import gemini.example.com.hcpayment.entity.DepositBean;
import gemini.example.com.hcpayment.entity.KeepContractInfoBean;
import gemini.example.com.hcpayment.entity.KeepDepositBean;
import gemini.example.com.hcpayment.entity.LoginBean;
import gemini.example.com.hcpayment.entity.ProductInfoBean;
import gemini.example.com.hcpayment.entity.QueryContractBean;
import gemini.example.com.hcpayment.entity.QueryContractStatusInfo;
import gemini.example.com.hcpayment.entity.QueryCustomBean;
import gemini.example.com.hcpayment.entity.QueryCustommadeListBean;
import gemini.example.com.hcpayment.entity.ReceiveInfoBean;
import gemini.example.com.hcpayment.entity.StagingInfo;
import gemini.example.com.hcpayment.entity.SubmitStageInfo;
import gemini.example.com.hcpayment.entity.TransactionInfo;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Gemini on 2018/3/29.
 */

public interface ApiSevice {

    /**
     * 获取客户信息列表
     *
     * @param token
     * @param mobile
     * @return
     */
    @GET("api/v1/custom/list")
    Observable<CustomBean> getCustom(@Header("token") String token, @Query("mobile") String mobile, @Query("page") int page);


    @GET("api/v1/products")
    Observable<ProductInfoBean> getProductInfo(@Header("token") String token);

    /**
     * 保存合同返回分期信息
     */

    @Headers("Content-Type:application/json")
    @POST("api/v1/contract")
    Observable<StagingInfo> getStagingInfo(@Header("token") String token, @Body String contractInfoJson);

    /**
     * 和下面的那个区别在于有customId参数查询
     *
     * @param token
     * @param mobile   电话号码
     * @param status   订金状态
     * @param customId
     * @param page     页数
     * @return
     */
    @GET("api/v1/frontmoney/list")
    Observable<DepositBean> getDepositInfo(@Header("token") String token, @Query("mobile") String mobile, @Query("status") int status
            , @Query("customId") int customId, @Query("page") int page);

    /**
     * @param mobile 客户手机号
     *               查询客户信息并获取订金
     *               查询所有，
     * @return
     */
    @GET("api/v1/frontmoney/list")
    Observable<DepositBean> getAllDepositInfo(@Header("token") String token, @Query("mobile") String mobile, @Query("status") int status,
                                              @Query("page") int page);

    /**
     * 保存合同信息
     *
     * @param contractId
     * @return
     */
    @Headers("Content-Type:application/json")
    @POST("api/v1/contract/{contractId}/stage")
    Observable<KeepContractInfoBean> keepContractInfo(@Header("token") String token, @Path("contractId") int contractId);

    /**
     * 合同详情页面查询合同分期详情
     *
     * @param contractId
     * @return
     */


    @GET("api/v1/contract/{contractId}/info")
    Observable<QueryContractBean> queryContractStageInfo(@Header("token") String token, @Path("contractId") int contractId);

    /**
     * 保存订金
     *
     * @param customId 客户id
     * @param money    定金金额
     * @return
     */

    @Headers("Content-Type:application/json")
    @POST("api/v1/frontmoney")
    Observable<KeepDepositBean> keepDepositBean(@Header("token") String token, @Body String keepDepositJson);

    /**
     * 申请退还订金
     *
     * @param id 订金id
     * @return
     */
    @Headers("Content-Type:application/json")
    @POST("api/v1/frontmoney/{id}/refund")
    Observable<ApplyRefoundInfoBean> applyRefoundInfo(@Header("token") String token, @Path("id") int id,@Body String refundJson);

    /**
     * 查询个性定制费用
     *
     * @param mobile
     * @return
     */
    @GET("api/v1/special-money/list")
    Observable<QueryCustommadeListBean> queryCustomMadeList(@Header("token") String token,
                                                            @Query("mobile") String mobile,
                                                            @Query("page") int page);

    /**
     * 管理订单
     * 获取合同信息
     *
     * @param status
     * @param page   页数
     * @return
     */
    @GET("api/v1/contract/list")
    Observable<QueryContractStatusInfo> queryContractInfo(@Header("token") String token, @Query("status") int status, @Query("page") int page);

    /**
     * 登录
     *
     * @param fieldMap
     * @return
     */

    @Headers("Content-Type:application/json")
    @POST("api/v1/auth")
    Observable<LoginBean> login(@Body String userJson);


    /**
     * 获取合同列表信息----标准费用列表
     *
     * @param stage
     * @param mobile
     * @param status
     * @param page   页数
     * @return
     */
    @GET("api/v1/contract/stage/list")
    Observable<ContractListBean> queryStandardFeeList(@Header("token") String token, @Query("stage") int stage, @Query("mobile") String mobile,
                                                      @Query("status") int status, @Query("page") int page);


    /**
     * 分期信息验收
     *
     * @param id
     * @return
     */
    @Headers("Content-Type:application/json")
    @POST("api/v1/contract/stage/{id}/finish")
    Observable<SubmitStageInfo> submitStage(@Header("token") String token, @Path("id") int id);

    /**
     * 交易信息确认
     *
     * @param ordernum
     * @return 1定金 2定制费用 3分期收费
     */
    @Headers("Content-Type:application/json")
    @PUT("/api/v1/pay")
    Observable<TransactionInfo> checkTrancation(@Header("token") String token, @Body String transactionJson);


    /**
     * @param token
     * @param mobile
     * @param pageNum
     * @param pageSize
     * @param status   0-意向状态1-设计状态2-施工状态3-售后状态
     * @return
     */
    @GET("/api/v1/custom/list")
    Observable<QueryCustomBean> queryCustomInfo(@Header("token") String token, @Query("mobile") String mobile, @Query("page") int pageNum);


    /**
     * @param token
     * @param status
     * @param no     合同编号
     * @return
     */
    @GET("/api/v1/contract/stage/pay/list")
    Observable<ReceiveInfoBean> queryReceiveInfo(@Header("token") String token, @Query("status") int status,
                                                 @Query("no") String no, @Query("page") int page);

}
