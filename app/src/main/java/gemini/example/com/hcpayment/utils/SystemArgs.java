package gemini.example.com.hcpayment.utils;

/**
 * Created by Gemini on 2018/5/25.
 */

public interface SystemArgs {
    public final  String CUSTOMINFO="customInfo";
    String CUSTOMBUNDLE="customBundle";
    String CUSTOMHOUSEID="customHouseId";
    String CUSTOMID="customId";
    String BUILDINGAREA="buildingArea";
    String DECORATIONAREA="decorationArea";
    String CUSTOMMANAGER="customManager";
    String ORDERNO="orderNum";
    String DECORATIONADD="decorationAddress";
    String CONTRACTID="contractId";
    String STAGELIST="stageList";
    String ACTUALMONEY="actualMoney";
    String CUSTOMMADMONEY="customMadeMoney";
    String MAINPAGEFLAG="mainPageFlag";//由主页跳转的标记
    String INPUTDEPOSIT="inputDeposit";
    String DEPOSITINFO="depositInfo";
    String TOKEN="token";
    String FLAG="flag";
    String CUSTOMMADELISTBEAN="customMadeListBean";//个性定制费用列表
    String STATUS="status";
    String PAYINFO="payInfo";
    String moneyType="moneyType";
    String CONTRACTINFO="contractInfo";
    String USERNAME="userName";
    String PASSWORD="passWord";
    String ISLOGOIN="isLogin";//判别是否再次login
    String ACCOUNTNAME="name";

}
