package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.adapter.QueryCustommadeListAdapter;
import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.QueryCustommadeListBean;
import gemini.example.com.hcpayment.model.CustomMadeListModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.CustomMadeListView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/5/31.
 */

public class CustomMadeListPresenter extends BasePresenter<CustomMadeListView> implements BaseAdapter.onItemClickListener {
    Context context;
    CustomMadeListModel customMadeListModel;

    public CustomMadeListPresenter(Context context) {
        this.context = context;
        customMadeListModel = new CustomMadeListModel();
    }

    public RecyclerView.OnScrollListener getOnScrollListener() {

        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                    int lastVisiblePosition;
                    lastVisiblePosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                    if (layoutManager.getChildCount() > 0             //当当前显示的item数量>0
                            && lastVisiblePosition >= layoutManager.getItemCount() - 1           //当当前屏幕最后一个加载项位置>=所有item的数量
                            && layoutManager.getItemCount() > layoutManager.getChildCount()) { // 当当前总Item数大于可见Item数
                        getView().onLoadMore();
                    }else{
                        getView().onNoLoadMore();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        };
        return onScrollListener;
    }
    public void queryCustomMadeList(String token, String mobile,int page) {
        getView().showProgressDialog(true);
        customMadeListModel.queryCustomMadeList(token, mobile, page,new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                QueryCustommadeListBean queryCustommadeListBean = (QueryCustommadeListBean) o;
                getView().onCallbackCustomMadeList(queryCustommadeListBean);

            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }

    @Override
    public void onItemClick(int position, View v) {
        QueryCustommadeListAdapter queryCustommadeListAdapter = getView().getCustomAdapter();
        getView().onCallbackItemClick(queryCustommadeListAdapter.getData().get(position));
    }
}
