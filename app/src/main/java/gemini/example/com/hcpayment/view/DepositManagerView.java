package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.adapter.DepositManagerAdapter;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.DepositBean;

/**
 * Created by Gemini on 2018/5/31.
 */

public interface DepositManagerView extends BaseView {
    void callBackCustomData(DepositBean depositBean);
    void callItemClick(DepositBean.DataBean.ListBean bean);
    String getMobile();
    void onLoadMore();
    void onNoLoadMore();
    int getCurrentPage();
    DepositManagerAdapter getManagerAdapter();
}
