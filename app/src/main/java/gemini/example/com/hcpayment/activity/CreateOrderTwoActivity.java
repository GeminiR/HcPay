package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.contract.CreateOrderContract;
import gemini.example.com.hcpayment.entity.DepositBean;
import gemini.example.com.hcpayment.entity.StagingInfo;
import gemini.example.com.hcpayment.presenter.CreateOrderTwoPresenter;
import gemini.example.com.hcpayment.ui.SelectDepositAlertDialog;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.CreateOrderTwoView;

/**
 * Created by Gemini on 2018/5/28.
 */

public class CreateOrderTwoActivity extends BaseActivity<CreateOrderTwoView, CreateOrderTwoPresenter>
        implements CreateOrderTwoView, SwipeRefreshLayout.OnRefreshListener, SelectDepositAlertDialog.DialogListLoadMoreListener {
    private final String TAG = "CreateOrderTwo";
    @BindView(R.id.productInfoSelect)
    TextView productInfoSelect;
    @BindView(R.id.selectDepositRecord)
    TextView selectDepositRecord;
    @BindView(R.id.buildAreaText)
    TextView buildAreaText;
    @BindView(R.id.decorationAreaText)
    TextView decorationAreaText;
    @BindView(R.id.customManagerText)
    TextView customManagerText;
    @BindView(R.id.nextStep)
    Button nextStep;
    @BindView(R.id.orderNum)
    EditText orderNum;
    @BindView(R.id.discountMoneyText)
    EditText discountMoneyText;
    @BindView(R.id.productFee)
    TextView productFee;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.inputSpecialMoney)
    EditText inputSpecialMoney;
    @BindView(R.id.actuallyAllMoney)
    EditText actuallyAllMoney;
    private int customId;
    private int productId = 0;
    private int depositId = 0;
    private String decorationAdd = "";
    SelectDepositAlertDialog selectDepositAlertDialog;
    private int currentPage = 1;

    @Override
    public CreateOrderTwoPresenter createPresenter() {
        return new CreateOrderTwoPresenter(this);
    }

    @Override
    public CreateOrderTwoView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("创建订单");
        changeStatusBarTextColor(true);
        Intent intent = getIntent();
        if (intent != null) {
            customId = intent.getIntExtra(SystemArgs.CUSTOMID, 0);
            String buildingArea = intent.getStringExtra(SystemArgs.BUILDINGAREA);
            String decorationArea = intent.getStringExtra(SystemArgs.DECORATIONAREA);
            String customManager = intent.getStringExtra(SystemArgs.CUSTOMMANAGER);
            decorationAdd = intent.getStringExtra(SystemArgs.DECORATIONADD);
            buildAreaText.setText(buildingArea);
            decorationAreaText.setText(decorationArea);
            customManagerText.setText(customManager);
        }
        productFee.addTextChangedListener(getPresenter().getTextWatcher());
        discountMoneyText.addTextChangedListener(getPresenter().getTextWatcher());
        selectDepositRecord.addTextChangedListener(getPresenter().getTextWatcher());
        inputSpecialMoney.addTextChangedListener(getPresenter().getTextWatcher());

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_createorder_two;
    }

    @OnClick({R.id.productInfoSelect, R.id.selectDepositRecord, R.id.nextStep})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.productInfoSelect:
                showProductWheel();
                break;
            case R.id.selectDepositRecord:
                showDepositDialog(customId);
                break;
            case R.id.nextStep:
                String buildArea = buildAreaText.getText().toString();
                String disCountMoney = discountMoneyText.getText().toString();
                String decorationArea = decorationAreaText.getText().toString();
                String orderNumber = orderNum.getText().toString();
                String specialMoney = inputSpecialMoney.getText().toString();
                if (TextUtils.isEmpty(orderNumber)) {
                    ToastUtil.showShortToast("订单号不能为空");
                } else {
                    keepOrderInfo(token, "HC001-" + orderNum.getText().toString(), productId,
                            TextUtils.isEmpty(disCountMoney) ? 0 : Double.parseDouble(disCountMoney),
                            depositId, decorationAdd,
                            TextUtils.isEmpty(buildArea) ? 0 : Double.parseDouble(buildArea),
                            TextUtils.isEmpty(specialMoney) ? 0 : Double.parseDouble(specialMoney),
                            TextUtils.isEmpty(decorationArea) ? 0 : Double.parseDouble(decorationArea));
                }
                break;
            default:
                break;

        }
    }

    @Override
    public void showProductWheel() {
        getPresenter().getProductData(token);
    }

    /**
     * 显示定金冲抵dialog
     *
     * @param customId
     */
    @Override
    public void showDepositDialog(int customId) {
        getPresenter().showDepositInfo(customId, currentPage);
    }

    /**
     * 回调回订金信息
     *
     * @param depositBean
     */
    @Override
    public void callbackDepositData(DepositBean depositBean) {
        if (selectDepositAlertDialog == null) {
            selectDepositAlertDialog = new SelectDepositAlertDialog(this, depositBean, getPresenter(), this, this);
            selectDepositAlertDialog.getWindow().setWindowAnimations(R.style.dialog_anim);
            selectDepositAlertDialog.init();
        } else if (!selectDepositAlertDialog.isShowing()) {
            selectDepositAlertDialog.show();
        }
    }

    @Override
    public SelectDepositAlertDialog getSelectDepositAlertDialog() {
        if(selectDepositAlertDialog!=null){
            return selectDepositAlertDialog;
        }
        return null;
    }

    @Override
    public void updateProductInfo(int id, String style, String styleName, double price) {
        productId = id;
        productInfoSelect.setText(styleName + "系列" + style);
        String decorationArea=decorationAreaText.getText().toString();
        if(!TextUtils.isEmpty(decorationArea)){
            double area = Double.parseDouble(decorationAreaText.getText().toString());
            double allMoney = price * area;
            productFee.setText(allMoney + "");
        }else{
            productFee.setText("0");
            ToastUtil.showShortToast("装修面积为空");
        }

    }

    /**
     * 订金冲抵回调
     *
     * @param money
     * @param id
     */
    @Override
    public void updateDepositInfo(double money, int id) {
        depositId = id;
        selectDepositRecord.setText(money + "");
    }

    @Override
    public void keepOrderInfo(String token, String orderNo, int productId, double discount, int frontMoneyId, String decorationadd, double buildingArea, double specialMoney, double decorationArea) {
        getPresenter().keepOrderInfo(token, customId, orderNo, productId, discount, frontMoneyId, decorationadd, buildingArea, specialMoney, decorationArea);
    }

    /**
     * 保存合同信息成功，跳转下一页
     */
    @Override
    public void keepOrderSuccess(StagingInfo stagingInfo) {
        int contractId = stagingInfo.getData().getId();
        List<Double> stageList = stagingInfo.getData().getStages();
        Intent intent = new Intent(this, CreateOrderThreeActivity.class);
        intent.putExtra(SystemArgs.CONTRACTID, contractId);
        intent.putExtra(SystemArgs.STAGELIST, (Serializable) stageList);
        intent.putExtra(SystemArgs.ACTUALMONEY, stagingInfo.getData().getActualMoney());
        intent.putExtra(SystemArgs.CUSTOMMADMONEY, stagingInfo.getData().getSpecialMoney());
        startActivity(intent);
    }

    @Override
    public double getProductAllMoney() {
        String productFeeText = productFee.getText().toString();
        if (!TextUtils.isEmpty(productFeeText)) {
            return Double.parseDouble(productFeeText);
        }
        return 0;
    }

    @Override
    public double getDiscountMoney() {
        String discountMoney = discountMoneyText.getText().toString();
        if (!TextUtils.isEmpty(discountMoney)) {
            return Double.parseDouble(discountMoney);
        }
        return 0;
    }

    @Override
    public double getDepositMoney() {
        if (depositId != 0) {
            String depositMoney = selectDepositRecord.getText().toString();
            if (!TextUtils.isEmpty(depositMoney)) {
                return Double.parseDouble(depositMoney);
            }
            return 0;
        } else {
            //未选择订金冲抵
            return 0;
        }
    }

    @Override
    public double getSpecialMoeny() {
        String specialMoney = inputSpecialMoney.getText().toString();
        if (!TextUtils.isEmpty(specialMoney)) {
            return Double.parseDouble(specialMoney);
        }
        return 0;
    }

    @Override
    public void updateAllMoney(double money) {
        Log.d(TAG, "allMoney--->" + money);
        actuallyAllMoney.setText(money + "");
    }


    @Override
    public void onRefresh() {
        currentPage = 1;
        selectDepositAlertDialog.setSwipeRefrshFalse();
        selectDepositAlertDialog.setCurrentPage(currentPage);
        showDepositDialog(customId);
    }

    @Override
    public void loadMore() {
        currentPage = selectDepositAlertDialog.getCurrentPage();
        currentPage++;
        selectDepositAlertDialog.getQueryDepositDialogAdapter().setFootStatus(BaseAdapter.ISLOADING);
        selectDepositAlertDialog.getQueryDepositDialogAdapter().notifyDataSetChanged();
        showDepositDialog(customId);
    }

    @Override
    public void noLoadMore() {
        selectDepositAlertDialog.getQueryDepositDialogAdapter().setFootStatus(BaseAdapter.LOADOVER);
        selectDepositAlertDialog.getQueryDepositDialogAdapter().notifyDataSetChanged();
    }
}
