package gemini.example.com.hcpayment.contract;

import java.util.List;

import gemini.example.com.hcpayment.adapter.CustomerAdapter;
import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.base.BaseResponse;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.CustomBean;
import gemini.example.com.hcpayment.entity.LoginBean;

/**
 * Created by Gemini on 2018/5/9.
 */

public interface CustomContract {
    interface CustomView extends BaseView {
        void result(CustomBean customBean);
        void dealCustomInfo(CustomBean.DataBean.ListBean listBean);
        String getMobile();
        void finishActivity();
        int getCurrentPage();
        void  setCurrentPage(int pageNum);
        void onLoadMore();
        void onNoLoadMore();
        CustomerAdapter getCustomerAdapter();
    }

    abstract class CustomPresenter extends BasePresenter<CustomView> {
    }
}
