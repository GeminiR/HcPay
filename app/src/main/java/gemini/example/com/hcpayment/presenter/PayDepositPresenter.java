package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.KeepDepositBean;
import gemini.example.com.hcpayment.entity.TransactionInfo;
import gemini.example.com.hcpayment.model.PayDepositModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.PayDepositView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/5/30.
 */

public class PayDepositPresenter extends BasePresenter<PayDepositView> {
    String TAG = "PayDepositPresenter";
    PayDepositModel payDepositModel;

    public PayDepositPresenter() {
        payDepositModel = new PayDepositModel();
    }

    public void keepDeposit(int customId, String money, int customHouseId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("customId", customId);
        jsonObject.addProperty("money", money);
        jsonObject.addProperty("customHouseId", customHouseId);
        String depositJson = new Gson().toJson(jsonObject);
        Log.d(TAG, "depositJson--->" + depositJson);
        getView().showProgressDialog(true);
        payDepositModel.keepDeposit(getView().getToken(), depositJson, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                KeepDepositBean keepDepositBean = (KeepDepositBean) o;
                Log.d(TAG, "keepDepositBean--->" + keepDepositBean);
                getView().callbackDepositInfo(keepDepositBean);
                getView().hideProgressDialog();
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {

            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {

            }
        });
    }

    public void checkTransaction(
            String qrCodeScanModel, String paymentType, String amount, String authNo, String cardNo, String countN, String terminalId,
            String acquire, String traceNo, String transId, String qrOrderNo, String batchNo, String merchantId, String referenceNo, String cardType,
            String operatorId, String payreason, String appId, String issue, String model, String transactionPlatform, String sumAmount,
            String errorCode, String version, String merchantName, String cardSecret, String orderNoSFT, String accountType, String packageName,
            String answerCode, String voucherNo, String transDate, String transName, String transTime, String transType, String transactionType,
            String qrCodeTransactionState, String barcodeType, int payState, int type) {
        getView().showProgressDialog(true);
        payDepositModel.checkTransactionInfo(getView().getToken(), qrCodeScanModel, paymentType, amount, authNo,
                cardNo, countN, terminalId, acquire, traceNo, transId, qrOrderNo,
                batchNo, merchantId, referenceNo, cardType, operatorId, payreason, appId,
                issue, model, transactionPlatform, sumAmount, errorCode, version, merchantName,
                cardSecret, orderNoSFT, accountType, packageName, answerCode, voucherNo,
                transDate, transName, transTime, transType, transactionType, qrCodeTransactionState,
                barcodeType, payState, type, new ObserverResponseListener() {
                    @Override
                    public void onNext(Object o) {
                        TransactionInfo transactionInfo = (TransactionInfo) o;
                        Log.d(TAG, "transactionInfo--->" + transactionInfo);
                        getView().callbackTransactionStatus(transactionInfo);
                    }

                    @Override
                    public void onError(ExceptionHandle.ResponseException e) {
                        getView().hideProgressDialog();
                        getView().callbackTransactionFail();
                    }

                    @Override
                    public void onComplete() {
                        getView().hideProgressDialog();
                    }

                    @Override
                    public void onSubscribe(Disposable d) {
                        getView().putDispose(d);
                    }
                });

    }

    public String dealMoney(String tempMoney) {
        BigDecimal bigDecimal = new BigDecimal(tempMoney);

        String money = rvZeroAndDot(tempMoney);
        if (money.contains(".")) {
            //判断是否有小数，
            int dotIndex = money.indexOf(".");
            double dotMoney = bigDecimal.doubleValue();
            return rvZeroAndDot(dotMoney * 100 + "");

        } else {
            int intMoney = bigDecimal.intValue();
            Log.d(TAG, "money---int" + intMoney);
            return intMoney * 100 + "";
        }

    }

    private String rvZeroAndDot(String s) {
        if (s.isEmpty()) {
            return null;
        }
        if (s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }
}
