package gemini.example.com.hcpayment.entity;

import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/5/28.
 */

public class ProductInfoBean extends BaseResponse {

    /**
     * data : {"pages":1,"total":50,"next":"","prev":"","list":[{"id":45,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":13,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":44,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":87,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":4,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":68,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":69,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":71,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":64,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":13,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ProductInfoBean{" +
                "data=" + data +
                '}';
    }

    public static class DataBean {
        /**
         * pages : 1
         * total : 50
         * next :
         * prev :
         * list : [{"id":45,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":13,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":44,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":87,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":4,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":68,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":69,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":71,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":64,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"},{"id":13,"name":"雅1","style":"雅","styleUrl":"","price":1500,"operator":"刘双箫"}]
         */

        private int pages;
        private int total;
        private String next;
        private String prev;
        private List<ListBean> list;

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrev() {
            return prev;
        }

        public void setPrev(String prev) {
            this.prev = prev;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "pages=" + pages +
                    ", total=" + total +
                    ", next='" + next + '\'' +
                    ", prev='" + prev + '\'' +
                    ", list=" + list +
                    '}';
        }

        public static class ListBean {
            /**
             * id : 45
             * name : 雅1
             * style : 雅
             * styleUrl :
             * price : 1500
             * operator : 刘双箫
             */

            private int id;
            private String name;
            private String style;
            private String styleUrl;
            private double price;
            private String operator;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStyle() {
                return style;
            }

            public void setStyle(String style) {
                this.style = style;
            }

            public String getStyleUrl() {
                return styleUrl;
            }

            public void setStyleUrl(String styleUrl) {
                this.styleUrl = styleUrl;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public String getOperator() {
                return operator;
            }

            public void setOperator(String operator) {
                this.operator = operator;
            }

            @Override
            public String toString() {
                return "ListBean{" +
                        "id=" + id +
                        ", name='" + name + '\'' +
                        ", style='" + style + '\'' +
                        ", styleUrl='" + styleUrl + '\'' +
                        ", price=" + price +
                        ", operator='" + operator + '\'' +
                        '}';
            }
        }
    }
}
