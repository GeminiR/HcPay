package gemini.example.com.hcpayment.model;

import android.content.Context;

import java.util.Map;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.base.BaseResponse;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/3/30.
 */

public class LoginModel<T> extends BaseModel {

    public void login(String userJson, ObserverResponseListener observerResponseListener){
        subscribe(Api.getApiSevice().login(userJson),observerResponseListener);
    }
}
