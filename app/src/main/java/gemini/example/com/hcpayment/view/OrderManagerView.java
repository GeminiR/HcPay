package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.adapter.QueryContractListAdapter;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.ContractListBean;
import gemini.example.com.hcpayment.entity.QueryContractStatusInfo;

/**
 * Created by Gemini on 2018/6/1.
 */

public interface OrderManagerView   extends BaseView{
    void onCallbackContractList(QueryContractStatusInfo queryContractStatusInfo);
    void onItemClick(QueryContractStatusInfo.DataBean.ListBean listBean);
    void requestData(int status);
    void onLoadMore();
    void onNoLoadMore();
    QueryContractListAdapter getAdapter();
}
