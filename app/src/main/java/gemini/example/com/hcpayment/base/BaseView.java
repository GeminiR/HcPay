package gemini.example.com.hcpayment.base;

import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/3/28.
 */

public interface BaseView {
    void showProgressDialog(boolean isCancelable);
    void hideProgressDialog();
    void putDispose(Disposable disposable);
    String getToken();
}
