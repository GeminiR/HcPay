package gemini.example.com.hcpayment.progress;

import gemini.example.com.hcpayment.utils.ExceptionHandle;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/3/28.
 */

public interface ObserverResponseListener<T> {
    void onNext(T t);

    void onError(ExceptionHandle.ResponseException e);

    void onComplete();

    void onSubscribe(Disposable d);
}
