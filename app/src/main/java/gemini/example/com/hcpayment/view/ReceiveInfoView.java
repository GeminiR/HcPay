package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.ReceiveInfoBean;

/**
 * Created by Gemini on 2018/6/16.
 */

public interface ReceiveInfoView extends BaseView {
    void onCallbackReciveInfo(ReceiveInfoBean receiveInfoBean);
}
