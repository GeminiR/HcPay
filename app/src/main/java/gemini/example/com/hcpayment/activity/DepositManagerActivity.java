package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.adapter.DepositManagerAdapter;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.DepositBean;
import gemini.example.com.hcpayment.presenter.DepositManagerPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.DepositManagerView;

/**
 * Created by Gemini on 2018/5/31.
 */

public class DepositManagerActivity extends BaseActivity<DepositManagerView, DepositManagerPresenter> implements
        DepositManagerView, SwipeRefreshLayout.OnRefreshListener, TextView.OnEditorActionListener {

    DepositManagerAdapter depositManagerAdapter;

    private int status = 1;
    private int currentPage = 1;

    @Override
    public DepositManagerPresenter createPresenter() {
        return new DepositManagerPresenter();
    }

    @Override
    public DepositManagerView createView() {
        return this;
    }



    @Override
    public void init() {

        titleText.setText("订金管理");
        changeStatusBarTextColor(true);
        depositRecycler.setLayoutManager(new LinearLayoutManager(this));
        //已收是2 、已转6、3退款
        getPresenter().callbackDepositData(status, currentPage);
        depositSwipe.setOnRefreshListener(this);
        depositSwipe.setColorSchemeResources(R.color.moregreen);
        depositRecycler.setOnScrollListener(getPresenter().getOnScrollListener());
        searchGuestInfoEdit.setOnEditorActionListener(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_depositmanager;
    }


    @Override
    public void callBackCustomData(DepositBean depositBean) {
        if (depositManagerAdapter == null) {
            depositManagerAdapter = new DepositManagerAdapter(depositBean.getData().getList(), R.layout.item_guestbookinfo);
            depositManagerAdapter.setItemClickListener(getPresenter());
            depositRecycler.setAdapter(depositManagerAdapter);
        } else {
            if (currentPage > 1) {
                depositManagerAdapter.addData(depositBean.getData().getList());
            } else {
                depositManagerAdapter.setData(depositBean.getData().getList());
            }

        }
        depositManagerAdapter.setFootStatus(BaseAdapter.LOADOVER);
        depositManagerAdapter.notifyDataSetChanged();

        if (depositManagerAdapter.getData().size() > 0) {
            noDataLayout.setVisibility(View.GONE);
        } else {
            noDataLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void callItemClick(DepositBean.DataBean.ListBean bean) {
        if (status == 2) {
            Intent intent = new Intent(this, DepositDetailInfoActivity.class);
            intent.putExtra(SystemArgs.DEPOSITINFO, (Serializable) bean);
            startActivity(intent);
        }
    }

    @Override
    public String getMobile() {
        return searchGuestInfoEdit.getText().toString();
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        depositSwipe.setRefreshing(false);
        getPresenter().callbackDepositData(status, currentPage);
    }

    @Override
    public void onLoadMore() {
        if (depositManagerAdapter != null) {
            currentPage++;
            depositManagerAdapter.setFootStatus(BaseAdapter.ISLOADING);
            depositManagerAdapter.notifyDataSetChanged();
            getPresenter().callbackDepositData(status, currentPage);
        }
    }

    @Override
    public void onNoLoadMore() {
        if (depositManagerAdapter != null) {
            depositManagerAdapter.setFootStatus(BaseAdapter.LOADOVER);
            depositManagerAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        currentPage = 1;
        getPresenter().callbackDepositData(status, currentPage);
        return true;
    }

    @Override
    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public DepositManagerAdapter getManagerAdapter() {
        return depositManagerAdapter;
    }

    @OnClick({R.id.waitPay, R.id.havePayed, R.id.applyrefundDeposit, R.id.flushed})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.waitPay:
                status = 1;
                updateButtonView((TextView) view);
                break;
            case R.id.havePayed:
                status = 2;
                updateButtonView((TextView) view);
                break;
            case R.id.applyrefundDeposit:
                status = 3;
                updateButtonView((TextView) view);
                break;
            case R.id.flushed:
                status = 6;
                updateButtonView((TextView) view);
                break;
            default:
                break;
        }
        currentPage = 1;
        getPresenter().callbackDepositData(status, currentPage);
    }

    private void updateButtonView(TextView textView) {
        waitPayButton.setBackgroundResource(R.drawable.depositmanager_left_gray);
        waitPayButton.setTextColor(getResources().getColor(R.color.black));
        havePayButton.setBackgroundResource(R.drawable.depositmanager_middle_gray);
        havePayButton.setTextColor(getResources().getColor(R.color.black));
        applyRefund.setBackgroundResource(R.drawable.depositmanager_middle_gray);
        applyRefund.setTextColor(getResources().getColor(R.color.black));
        flushButton.setBackgroundResource(R.drawable.depositmanager_right_gray);
        flushButton.setTextColor(getResources().getColor(R.color.black));
        if (textView == waitPayButton) {
            waitPayButton.setBackgroundResource(R.drawable.depositmanager_left_orange);
            waitPayButton.setTextColor(getResources().getColor(R.color.white));
        } else if (textView == havePayButton) {
            havePayButton.setBackgroundResource(R.drawable.depositmanager_middle_orange);
            havePayButton.setTextColor(getResources().getColor(R.color.white));
        } else if (textView == applyRefund) {
            applyRefund.setBackgroundResource(R.drawable.depositmanager_middle_orange);
            applyRefund.setTextColor(getResources().getColor(R.color.white));
        } else {
            flushButton.setBackgroundResource(R.drawable.depositmanager_right_orange);
            flushButton.setTextColor(getResources().getColor(R.color.white));
        }
    }


    @BindView(R.id.depositRecycler)
    RecyclerView depositRecycler;
    @BindView(R.id.waitPay)
    TextView waitPayButton;
    @BindView(R.id.havePayed)
    TextView havePayButton;
    @BindView(R.id.applyrefundDeposit)
    TextView applyRefund;
    @BindView(R.id.flushed)
    TextView flushButton;
    @BindView(R.id.depositSwipe)
    SwipeRefreshLayout depositSwipe;
    @BindView(R.id.searchGuestInfoEdit)
    EditText searchGuestInfoEdit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.depositNoDataLayout)
    RelativeLayout noDataLayout;


}
