package gemini.example.com.hcpayment.api;

import gemini.example.com.hcpayment.base.BaseApi;

/**
 * Created by Gemini on 2018/3/29.
 */

public class Api {
//    private static String BASEURL = "http://mockapi.hczjia.com/";
//    private static String BASEDEVURL = "http://dev.pos.hczjia.com/";
//    private static String TESTURL = "http://192.168.10.42:8002/";
//    private static String TESTURL2 = "http://192.168.10.7:8002/";
    private static String DEVURL = "http://pos.hczjia.com/";
    private static ApiSevice apiSevice;

    public static ApiSevice getApiSevice() {
        if (apiSevice == null) {
            synchronized (Api.class) {
                if (apiSevice == null) {
                    apiSevice = newApiService();
                }
            }
        }
        return apiSevice;
    }

    private static ApiSevice newApiService() {
        BaseApi base = new BaseApi();
        apiSevice = base.getCacheTimeOutRetrofit(DEVURL).create(ApiSevice.class);
        return apiSevice;
    }
}
