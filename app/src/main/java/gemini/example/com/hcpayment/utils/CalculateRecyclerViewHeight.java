package gemini.example.com.hcpayment.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

/**
 * Created by Gemini on 2018/6/27.
 */

public class CalculateRecyclerViewHeight {
    RecyclerView recyclerView;

    public CalculateRecyclerViewHeight(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    private int getItemHeight(RecyclerView recyclerView) {
        int itemHeight = 0;
        View child = null;
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int firstPos = layoutManager.findFirstCompletelyVisibleItemPosition();
        int lastPos = layoutManager.findLastCompletelyVisibleItemPosition();
        child = layoutManager.findViewByPosition(lastPos);
        if (child != null) {
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            itemHeight = child.getHeight() + params.topMargin + params.bottomMargin;
        }
        return itemHeight;
    }


    private int getLinearScrollY(RecyclerView recyclerView) {
        int scrollY = 0;
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int headerCildHeight = getHeaderHeight(recyclerView);
        int firstPos = layoutManager.findFirstVisibleItemPosition();
        View child = layoutManager.findViewByPosition(firstPos);
        int itemHeight = getItemHeight(recyclerView);
        if (child != null) {
            int firstItemBottom = layoutManager.getDecoratedBottom(child);
            scrollY = headerCildHeight + itemHeight * firstPos - firstItemBottom;
            if (scrollY < 0) {
                scrollY = 0;
            }
        }
        return scrollY;
    }


    private int getLinearTotalHeight(RecyclerView recyclerView) {
        int totalHeight = 0;
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        View child = layoutManager.findViewByPosition(layoutManager.findFirstVisibleItemPosition());
        int headerCildHeight = getHeaderHeight(recyclerView);
        if (child != null) {
            int itemHeight = getItemHeight(recyclerView);
            int childCount = layoutManager.getItemCount();
            totalHeight = headerCildHeight + (childCount - 1) * itemHeight;
        }
        return totalHeight;
    }

    public boolean isLinearBottom() {
        boolean isBottom = true;
        int scrollY = getLinearScrollY(recyclerView);
        int totalHeight = getLinearTotalHeight(recyclerView);
        int height = recyclerView.getHeight();
        Log.e("height", "scrollY  " + scrollY + "  totalHeight  " + totalHeight + "  recyclerHeight  " + height);
        if (scrollY + height < totalHeight) {
            isBottom = false;
        }
        return isBottom;
    }

    private int getHeaderHeight(RecyclerView recyclerView) {
        int headerCildHeight = 0;

        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int firstHeaderPos = layoutManager.findFirstCompletelyVisibleItemPosition();
        View headerCild = layoutManager.findViewByPosition(firstHeaderPos);

        if (headerCild != null) {
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) headerCild.getLayoutParams();
            headerCildHeight = headerCild.getHeight() + params.topMargin + params.bottomMargin;
        }
        return headerCildHeight;
    }


}
