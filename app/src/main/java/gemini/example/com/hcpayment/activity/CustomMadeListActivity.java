package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.adapter.QueryCustommadeListAdapter;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.QueryCustommadeListBean;
import gemini.example.com.hcpayment.presenter.CustomMadeListPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.CustomMadeListView;

/**
 * Created by Gemini on 2018/5/31.
 */

public class CustomMadeListActivity extends BaseActivity<CustomMadeListView, CustomMadeListPresenter>
        implements CustomMadeListView, SwipeRefreshLayout.OnRefreshListener, TextView.OnEditorActionListener {
    QueryCustommadeListAdapter queryCustommadeListAdapter;
    int currentPage = 1;

    @Override
    public CustomMadeListPresenter createPresenter() {
        return new CustomMadeListPresenter(this);
    }

    @Override
    public CustomMadeListView createView() {
        return this;
    }


    @Override
    public void init() {
        titleText.setText("个性订制费用项");
        searchGuestEdit.setOnEditorActionListener(this);
        customMadeRecycler.setLayoutManager(new LinearLayoutManager(this));
        customMadeSwipe.setOnRefreshListener(this);
        queryCustommadeList();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_custommadelist;
    }

    void queryCustommadeList() {
        getPresenter().queryCustomMadeList(token, searchGuestEdit.getText().toString(), currentPage);
    }

    @Override
    public void onCallbackCustomMadeList(QueryCustommadeListBean queryCustommadeListBean) {
        if (queryCustommadeListAdapter == null) {
            queryCustommadeListAdapter = new QueryCustommadeListAdapter(queryCustommadeListBean.getData().getList(), R.layout.item_guestbookinfo);
            queryCustommadeListAdapter.setItemClickListener(getPresenter());
            customMadeRecycler.setAdapter(queryCustommadeListAdapter);
        } else {
            if (currentPage > 1) {
                queryCustommadeListAdapter.addData(queryCustommadeListBean.getData().getList());
            } else {
                queryCustommadeListAdapter.setData(queryCustommadeListBean.getData().getList());
            }
        }
        queryCustommadeListAdapter.setFootStatus(BaseAdapter.LOADOVER);
        queryCustommadeListAdapter.notifyDataSetChanged();
        if (queryCustommadeListAdapter.getData().size() > 0) {
            noDataLayout.setVisibility(View.GONE);
        } else {
            noDataLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCallbackItemClick(QueryCustommadeListBean.DataBean.ListBean listBean) {
        Intent intent = new Intent(this, PayContractFeeActivity.class);
        intent.putExtra(SystemArgs.FLAG, 1);
        intent.putExtra(SystemArgs.CUSTOMMADELISTBEAN, listBean);
        startActivity(intent);
    }

    @Override
    public QueryCustommadeListAdapter getCustomAdapter() {
        return queryCustommadeListAdapter;
    }

    @Override
    public void onLoadMore() {
        if (queryCustommadeListAdapter != null) {
            currentPage++;
            queryCustommadeListAdapter.setFootStatus(BaseAdapter.ISLOADING);
            queryCustommadeListAdapter.notifyDataSetChanged();
            queryCustommadeList();
        }
    }

    @Override
    public void onNoLoadMore() {
        if (queryCustommadeListAdapter != null) {
            queryCustommadeListAdapter.setFootStatus(BaseAdapter.LOADOVER);
            queryCustommadeListAdapter.notifyDataSetChanged();
        }
    }

    @BindView(R.id.customMadeRecycler)
    RecyclerView customMadeRecycler;
    @BindView(R.id.customMadeSwipe)
    SwipeRefreshLayout customMadeSwipe;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.searchGuestEdit)
    EditText searchGuestEdit;
    @BindView(R.id.depositNoDataLayout)
    RelativeLayout noDataLayout;

    @Override
    public void onRefresh() {
        customMadeSwipe.setRefreshing(false);
        currentPage = 1;
        queryCustommadeList();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        currentPage = 1;
        queryCustommadeList();
        return true;
    }
}
