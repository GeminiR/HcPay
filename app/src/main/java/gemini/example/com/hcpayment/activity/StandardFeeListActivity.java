package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.adapter.StandardFeeAdapter;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.ContractListBean;
import gemini.example.com.hcpayment.presenter.StandardFeeListPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.StandardFeeListView;

/**
 * Created by Gemini on 2018/6/12.
 */

public class StandardFeeListActivity extends BaseActivity<StandardFeeListView, StandardFeeListPresenter> implements
        StandardFeeListView, SwipeRefreshLayout.OnRefreshListener ,TextView.OnEditorActionListener{
    private int stage = 1;
    private StandardFeeAdapter standardFeeAdapter;
    private int currentPage = 1;
    @BindView(R.id.standardFeeSwipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public StandardFeeAdapter getStandardFeeAdapter() {
        return standardFeeAdapter;
    }

    @Override
    public void onLoadMore() {
        if (standardFeeAdapter != null) {
            currentPage++;
            standardFeeAdapter.setFootStatus(BaseAdapter.ISLOADING);
            standardFeeAdapter.notifyDataSetChanged();
            requestData();
        }
    }

    @Override
    public void onNoLoadMore() {
        if(standardFeeAdapter!=null){
            standardFeeAdapter.setFootStatus(BaseAdapter.LOADOVER);
            standardFeeAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public StandardFeeListPresenter createPresenter() {
        return new StandardFeeListPresenter();
    }

    @Override
    public StandardFeeListView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("标准费用项");
        searchGuestInfoEdit.setOnEditorActionListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        standardFeeList.addOnScrollListener(getPresenter().getOnScrollListener());
        standardFeeList.setLayoutManager(new LinearLayoutManager(this));
        standardFeeList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        oneStageButton.performClick();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_standardfeelist;
    }

    private void requestData() {
        getPresenter().queryStandardFeeList(stage, searchGuestInfoEdit.getText().toString(), 1, currentPage);
    }

    @OnClick({R.id.oneStageButton, R.id.twoStageButton, R.id.threeStageButton})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.oneStageButton:
                stage = 1;
                updateButtonView((TextView) view);
                break;
            case R.id.twoStageButton:
                stage = 2;
                updateButtonView((TextView) view);
                break;
            case R.id.threeStageButton:
                stage = 3;
                updateButtonView((TextView) view);
                break;
            default:
                break;
        }
        currentPage = 1;
        requestData();
    }

    private void updateButtonView(TextView textView) {
        oneStageButton.setBackgroundResource(R.drawable.depositmanager_left_gray);
        oneStageButton.setTextColor(getResources().getColor(R.color.black));
        twoStageButton.setBackgroundResource(R.drawable.depositmanager_middle_gray);
        twoStageButton.setTextColor(getResources().getColor(R.color.black));
        threeStageButtn.setBackgroundResource(R.drawable.depositmanager_right_gray);
        threeStageButtn.setTextColor(getResources().getColor(R.color.black));
        if (textView == oneStageButton) {
            oneStageButton.setBackgroundResource(R.drawable.depositmanager_left_orange);
            oneStageButton.setTextColor(getResources().getColor(R.color.white));
        } else if (textView == twoStageButton) {
            twoStageButton.setBackgroundResource(R.drawable.depositmanager_middle_orange);
            twoStageButton.setTextColor(getResources().getColor(R.color.white));
        } else {
            threeStageButtn.setBackgroundResource(R.drawable.depositmanager_right_orange);
            threeStageButtn.setTextColor(getResources().getColor(R.color.white));
        }
    }


    @Override
    public void onCallbackQueryResult(ContractListBean contractListBean) {
        if (standardFeeAdapter == null) {
            standardFeeAdapter = new StandardFeeAdapter(contractListBean.getData().getList(), R.layout.item_guestbookinfo);
            standardFeeAdapter.setItemClickListener(getPresenter());
            standardFeeList.setAdapter(standardFeeAdapter);
        } else {
            if (currentPage > 1) {
                standardFeeAdapter.addData(contractListBean.getData().getList());
            } else {
                standardFeeAdapter.setData(contractListBean.getData().getList());
            }

        }
        standardFeeAdapter.setFootStatus(BaseAdapter.LOADOVER);
        standardFeeAdapter.notifyDataSetChanged();

        if(standardFeeAdapter.getData().size()>0){
            noDataLayout.setVisibility(View.GONE);
        }else{
            noDataLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCallItemClick(ContractListBean.DataBean.ListBean listBean) {
        Intent intent = new Intent(this, PayContractFeeActivity.class);
        intent.putExtra(SystemArgs.FLAG, 2);

        intent.putExtra(SystemArgs.CUSTOMMADELISTBEAN, listBean);
        startActivity(intent);
    }

    @BindView(R.id.oneStageButton)
    TextView oneStageButton;
    @BindView(R.id.twoStageButton)
    TextView twoStageButton;
    @BindView(R.id.threeStageButton)
    TextView threeStageButtn;
    @BindView(R.id.standardFeeRecycler)
    RecyclerView standardFeeList;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.searchGuestInfoEdit)
    EditText searchGuestInfoEdit;
    @BindView(R.id.depositNoDataLayout)
    RelativeLayout noDataLayout;

    @Override
    public void onRefresh() {
        currentPage=1;
        swipeRefreshLayout.setRefreshing(false);
        requestData();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        currentPage=1;
        requestData();
        return true;
    }
}
