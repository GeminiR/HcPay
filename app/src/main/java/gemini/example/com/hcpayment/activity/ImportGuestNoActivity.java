package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.contract.ImportGuestNoContract;
import gemini.example.com.hcpayment.presenter.ImportGuestNoPresener;
import gemini.example.com.hcpayment.utils.SystemArgs;

/**
 * Created by Gemini on 2018/5/25.
 */

public class ImportGuestNoActivity extends BaseActivity<ImportGuestNoContract.ImportGuestNoView, ImportGuestNoContract.ImportGuestNoPresenter> implements ImportGuestNoContract.ImportGuestNoView {

    private final int REQUEST_CODE = 1;
    private final int RESULT_CODE=3;
    @BindView(R.id.searchRelative)
    RelativeLayout searchRelative;
    private  int skipFlag=0;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;

    @Override
    public ImportGuestNoContract.ImportGuestNoPresenter createPresenter() {
        return new ImportGuestNoPresener();
    }

    @Override
    public ImportGuestNoContract.ImportGuestNoView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("订金收取");
        changeStatusBarTextColor(true);
        Intent intent=getIntent();
        if(intent!=null){
            skipFlag=intent.getIntExtra(SystemArgs.MAINPAGEFLAG,0);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_getdeposit;
    }

    @OnClick(R.id.searchRelative)
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.searchRelative:
                if(skipFlag==0){
                    Intent intent = new Intent(this, CustomActivity.class);
                    startActivityForResult(intent, REQUEST_CODE);
                }else{
                    Intent intent=new Intent(this,CustomActivity.class);
                    intent.putExtra(SystemArgs.MAINPAGEFLAG,skipFlag);
                    startActivity(intent);
                }

                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE){
            if(resultCode==RESULT_CODE){
                if(data!=null){
                    Intent intent=new Intent(ImportGuestNoActivity.this,CreateOrderActivity.class);
                    Bundle bundle=data.getBundleExtra(SystemArgs.CUSTOMBUNDLE);
                    intent.putExtra(SystemArgs.CUSTOMBUNDLE,bundle);
                    setResult(RESULT_CODE,intent);
                    finish();
                }
            }
        }
    }
}
