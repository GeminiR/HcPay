package gemini.example.com.hcpayment.presenter;

import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.ReceiveInfoBean;
import gemini.example.com.hcpayment.model.ReceiveInfoModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.ReceiveInfoView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/6/16.
 */

public class ReceiveInfoPresenter extends BasePresenter<ReceiveInfoView> implements ExpandableListView.OnChildClickListener{
    ReceiveInfoModel receiveInfoModel;

    public ReceiveInfoPresenter() {
        receiveInfoModel = new ReceiveInfoModel();
    }

    public void QueryReceiveInfo(int status, String no,int page) {
        getView().showProgressDialog(true);
        receiveInfoModel.queryReceiveInfo(getView().getToken(), status, no,page, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                ReceiveInfoBean receiveInfoBean= (ReceiveInfoBean) o;
                getView().onCallbackReciveInfo(receiveInfoBean);

            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        return false;
    }
}
