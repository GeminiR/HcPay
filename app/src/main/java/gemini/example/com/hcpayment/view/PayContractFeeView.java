package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.TransactionInfo;

/**
 * Created by Gemini on 2018/6/15.
 */

public interface PayContractFeeView extends BaseView {
    void callbackTransactionStatus(TransactionInfo transactionInfo);
}
