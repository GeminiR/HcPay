package gemini.example.com.hcpayment.base;

import android.content.Context;

import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.progress.ProgressObserver;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Gemini on 2018/3/29.
 */

public class BaseModel<T> {

    public void subscribe(Observable observable, ObserverResponseListener<T> listener){
        final Observer<T> observer=new ProgressObserver<>(listener);
        observable.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}

