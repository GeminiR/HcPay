package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.util.Log;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.KeepContractInfoBean;
import gemini.example.com.hcpayment.model.CreateOrderThreeModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.CreateOrderThreeView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/5/30.
 */

public class CreateOrderThreePresenter extends BasePresenter<CreateOrderThreeView> {
    private String TAG = "crateOrderThreepresenter";

    CreateOrderThreeModel createOrderThreeModel;

    public CreateOrderThreePresenter() {
        createOrderThreeModel = new CreateOrderThreeModel();
    }

    public void keepOrderInfo(String token, int contractId) {
        getView().showProgressDialog(true);
        createOrderThreeModel.keepInfo(token, contractId, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                KeepContractInfoBean keepContractInfoBean = (KeepContractInfoBean) o;
                getView().callbackKeepResult(keepContractInfoBean);
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }
}
