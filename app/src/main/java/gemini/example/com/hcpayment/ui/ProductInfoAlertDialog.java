package gemini.example.com.hcpayment.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.entity.ProductInfoBean;


/**
 * Created by Gemini on 2018/5/28.
 */

public class ProductInfoAlertDialog extends AlertDialog implements View.OnClickListener {
    Context context;
    private String TAG = "ProductInfo";
    ProductInfoBean productInfoBean;
    ArrayList<String> styleList = new ArrayList<>();
    ArrayList<String> idsList = new ArrayList<>();
    WheelView styleInfo;
    WheelView contentInfo;
    Map<Integer, String> wheelDataMap;//包含全部系列的总数据，对应为id---sonstylename
    Map<String, ArrayList<String>> wheelStyleAllData;
    private int currentId;
    private String currentText;//系列下的子类别名称
    private String currentStyleText;//系列名字
    private OnSelectStyleWithContentListener onSelectStyleWithContentListener;

    protected ProductInfoAlertDialog(Context context) {
        super(context);
    }

    protected ProductInfoAlertDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    protected ProductInfoAlertDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public ProductInfoAlertDialog(Context context, ProductInfoBean productInfoBean, OnSelectStyleWithContentListener listener) {
        super(context);
        this.context = context;
        this.productInfoBean = productInfoBean;
        this.onSelectStyleWithContentListener = listener;
        dealProductData();


    }

    private void dealProductData() {
        wheelDataMap = new HashMap<>();
        wheelStyleAllData = new HashMap<>();
        styleList.clear();
        for (int i = 0; i < productInfoBean.getData().getList().size(); i++) {
            String style = productInfoBean.getData().getList().get(i).getStyle();
            String sonStyleName = productInfoBean.getData().getList().get(i).getName();
            int id = productInfoBean.getData().getList().get(i).getId();
            if (!styleList.contains(style)) {
                styleList.add(style);
            }
            wheelDataMap.put(id, sonStyleName);
        }
        for (int i = 0; i < styleList.size(); i++) {
            idsList = new ArrayList<>();
            String style = styleList.get(i);
            for (int j = 0; j < productInfoBean.getData().getList().size(); j++) {
                String id = productInfoBean.getData().getList().get(j).getId() + "";
                String sonStyle = productInfoBean.getData().getList().get(j).getStyle();
                if (sonStyle.equals(style)) {
                    idsList.add(id);
                }
            }
            wheelStyleAllData.put(style, idsList);
        }
    }

    public void init() {
        Button sure, cancel;
        View view = LayoutInflater.from(context).inflate(R.layout.alertdialog_productwheel, null);
        sure = view.findViewById(R.id.sureSelectProduct);
        cancel = view.findViewById(R.id.cancelSelectProduct);
        sure.setOnClickListener(this);
        cancel.setOnClickListener(this);

        styleInfo = view.findViewById(R.id.productWheelStyle);
        contentInfo = view.findViewById(R.id.productWheelContent);

        styleInfo.setItems(styleList);

        List<String> idslist=wheelStyleAllData.get(styleList.get(0));
//        Log.d(TAG,"idsList--->"+idslist+",map--->"+wheelDataMap);
        contentInfo.setItemsWithIds(wheelStyleAllData.get(styleList.get(0)), wheelDataMap);
        styleInfo.setOnWheelViewListener(new WheelView.OnWheelViewListener(){
            @Override
            public void onSelected(int selectedIndex, String item, int id) {
                super.onSelected(selectedIndex, item, id);
//                Log.d(TAG,"onSelect--->"+item+",contentMap--->"+wheelStyleAllData.get(item));
                contentInfo.setSeletion(0);
                contentInfo.setItemsWithIds(wheelStyleAllData.get(item), wheelDataMap);
            }
        });
        show();
        Window window = getWindow();
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        layoutParams.width = display.getWidth();
        window.setGravity(Gravity.BOTTOM);
        window.setAttributes(layoutParams);
        window.setContentView(view);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sureSelectProduct:
                double price=0;
                String styleText=styleInfo.getSeletedItem();
                TextView styleInfoText=contentInfo.getSelectItem();
                int id=styleInfoText.getId();
                String contentInfo=styleInfoText.getText().toString();
                for (int i = 0; i < productInfoBean.getData().getList().size(); i++) {
                    int tempId=productInfoBean.getData().getList().get(i).getId();
                    if(tempId==id){
                        price=productInfoBean.getData().getList().get(i).getPrice();
                    }
                }
//                Log.d(TAG,"wheel--->styleText--->"+styleText+",id--->"+id+",contentInfo--->"+contentInfo);
                onSelectStyleWithContentListener.callbackStyle(id, contentInfo, styleText,price);
                dismiss();
                break;
            case R.id.cancelSelectProduct:
                dismiss();
                break;
            default:
                break;
        }
    }

    public interface OnSelectStyleWithContentListener {
        void callbackStyle(int id, String style, String styleName,double price);
    }

}
