package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.KeepContractInfoBean;

/**
 * Created by Gemini on 2018/5/30.
 */

public interface CreateOrderThreeView extends BaseView {
    void keepOrderInfo(int contractId);
    void callbackKeepResult(KeepContractInfoBean keepContractInfoBean);
}
