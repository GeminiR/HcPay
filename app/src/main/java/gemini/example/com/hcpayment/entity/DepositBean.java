package gemini.example.com.hcpayment.entity;

import java.io.Serializable;
import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/5/29.
 */

public class DepositBean extends BaseResponse implements Serializable {


    /**
     * data : {"pages":1,"total":1,"next":"","prev":"","list":[{"id":5,"contractId":"","customId":5,"money":123456.79,"status":2,"mark":"北京5环装修定金","orderNo":"FM20180530095555307S3U","serialNo":"","payTime":"","payStatus":0,"updateTime":"2018-05-30 01:55:55","createTime":"2018-05-30 01:55:55","operator":"刘双箫","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","mobile":"13810426535","manager":"宋春雷","customName":"庄女士"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DepositBean{" +
                "data=" + data +
                '}';
    }

    public static class DataBean  implements Serializable{
        /**
         * pages : 1
         * total : 1
         * next :
         * prev :
         * list : [{"id":5,"contractId":"","customId":5,"money":123456.79,"status":2,"mark":"北京5环装修定金","orderNo":"FM20180530095555307S3U","serialNo":"","payTime":"","payStatus":0,"updateTime":"2018-05-30 01:55:55","createTime":"2018-05-30 01:55:55","operator":"刘双箫","houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","mobile":"13810426535","manager":"宋春雷","customName":"庄女士"}]
         */

        private int pages;
        private int total;
        private String next;
        private String prev;
        private List<ListBean> list;

        @Override
        public String toString() {
            return "DataBean{" +
                    "pages=" + pages +
                    ", total=" + total +
                    ", next='" + next + '\'' +
                    ", prev='" + prev + '\'' +
                    ", list=" + list +
                    '}';
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrev() {
            return prev;
        }

        public void setPrev(String prev) {
            this.prev = prev;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean  implements Serializable{
            /**
             * id : 5
             * contractId :
             * customId : 5
             * money : 123456.79
             * status : 2
             * mark : 北京5环装修定金
             * orderNo : FM20180530095555307S3U
             * serialNo :
             * payTime :
             * payStatus : 0
             * updateTime : 2018-05-30 01:55:55
             * createTime : 2018-05-30 01:55:55
             * operator : 刘双箫
             * houseAddress : 杭州市西湖区紫霞街188号西溪蝶园-北区
             * mobile : 13810426535
             * manager : 宋春雷
             * customName : 庄女士
             */

            private int id;
            private String contractId;
            private int customId;
            private double money;
            private int status;
            private String mark;
            private String orderNo;
            private String serialNo;
            private String payTime;
            private int payStatus;
            private String updateTime;
            private String createTime;
            private String operator;
            private String houseAddress;
            private String mobile;
            private String manager;
            private String customName;
            private String shopName;
            private String shopNo;
            private String card;

            @Override
            public String toString() {
                return "ListBean{" +
                        "id=" + id +
                        ", contractId='" + contractId + '\'' +
                        ", customId=" + customId +
                        ", money=" + money +
                        ", status=" + status +
                        ", mark='" + mark + '\'' +
                        ", orderNo='" + orderNo + '\'' +
                        ", serialNo='" + serialNo + '\'' +
                        ", payTime='" + payTime + '\'' +
                        ", payStatus=" + payStatus +
                        ", updateTime='" + updateTime + '\'' +
                        ", createTime='" + createTime + '\'' +
                        ", operator='" + operator + '\'' +
                        ", houseAddress='" + houseAddress + '\'' +
                        ", mobile='" + mobile + '\'' +
                        ", manager='" + manager + '\'' +
                        ", customName='" + customName + '\'' +
                        ", shopName='" + shopName + '\'' +
                        ", shopNo='" + shopNo + '\'' +
                        '}';
            }

            public String getCard() {
                return card;
            }

            public void setCard(String card) {
                this.card = card;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getContractId() {
                return contractId;
            }

            public void setContractId(String contractId) {
                this.contractId = contractId;
            }

            public int getCustomId() {
                return customId;
            }

            public void setCustomId(int customId) {
                this.customId = customId;
            }

            public double getMoney() {
                return money;
            }

            public void setMoney(double money) {
                this.money = money;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getMark() {
                return mark;
            }

            public void setMark(String mark) {
                this.mark = mark;
            }

            public String getOrderNo() {
                return orderNo;
            }

            public void setOrderNo(String orderNo) {
                this.orderNo = orderNo;
            }

            public String getSerialNo() {
                return serialNo;
            }

            public void setSerialNo(String serialNo) {
                this.serialNo = serialNo;
            }

            public String getPayTime() {
                return payTime;
            }

            public void setPayTime(String payTime) {
                this.payTime = payTime;
            }

            public int getPayStatus() {
                return payStatus;
            }

            public void setPayStatus(int payStatus) {
                this.payStatus = payStatus;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getOperator() {
                return operator;
            }

            public void setOperator(String operator) {
                this.operator = operator;
            }

            public String getHouseAddress() {
                return houseAddress;
            }

            public void setHouseAddress(String houseAddress) {
                this.houseAddress = houseAddress;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getManager() {
                return manager;
            }

            public void setManager(String manager) {
                this.manager = manager;
            }

            public String getCustomName() {
                return customName;
            }

            public void setCustomName(String customName) {
                this.customName = customName;
            }

            public String getShopName() {
                return shopName;
            }

            public void setShopName(String shopName) {
                this.shopName = shopName;
            }

            public String getShopNo() {
                return shopNo;
            }

            public void setShopNo(String shopNo) {
                this.shopNo = shopNo;
            }
        }
    }
}
