package gemini.example.com.hcpayment.presenter;

import android.util.Log;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.ApplyRefoundInfoBean;
import gemini.example.com.hcpayment.model.DepositInfoDetailModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.BankNumCheckUtil;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.ApplyDepositTwoView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/7/12.
 */

public class ApplyDepositTwoPresenter extends BasePresenter<ApplyDepositTwoView> {
    DepositInfoDetailModel depositInfoDetailModel;
    public ApplyDepositTwoPresenter() {
        depositInfoDetailModel = new DepositInfoDetailModel();
    }

    public void applyRefound(String token, int id, String refundCard, String refundName, String refundBank, String refundRemark) {
        getView().showProgressDialog(true);
        depositInfoDetailModel.applyRefoundDeposit(token, id, refundCard, refundName, refundBank, refundRemark, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                ApplyRefoundInfoBean bean = (ApplyRefoundInfoBean) o;
                getView().callBackRefoundSucccess();
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();
                getView().callbackRefoundFail();

            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }
    public void checkBankName(String cardNum){
        String bankName= BankNumCheckUtil.getname(cardNum);
        getView().callbackBankName(bankName);
    }

}
