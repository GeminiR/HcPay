package gemini.example.com.hcpayment.model;

import android.content.Context;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/5/31.
 */

public class DepositManagerModel extends BaseModel {
    public void queryDepositManagerList(String token,String mobile,int status,int page ,ObserverResponseListener observerResponseListener){
        subscribe( Api.getApiSevice().getAllDepositInfo(token,mobile,status,page),observerResponseListener);
    }
}
