package gemini.example.com.hcpayment.view;

import android.widget.TextView;

import java.util.HashMap;

import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.QueryContractBean;

/**
 * Created by Gemini on 2018/6/4.
 */

public interface ContractDetailView extends BaseView {

    void onCallbackStatusAndMoney(HashMap<Integer, Integer> statusMap, HashMap<Integer, Double> moneyMap);

    void onCallbackSpecailMoney(int specialMoney, int specialStatus);

    void onCallbackSubmitSuccess();

    void onCallbackSubmitFail();

}
