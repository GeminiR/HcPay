package gemini.example.com.hcpayment.progress;

/**
 * Created by Gemini on 2018/3/28.
 */

public interface ProgressCancelListener {
    void cancelProgress();
}
