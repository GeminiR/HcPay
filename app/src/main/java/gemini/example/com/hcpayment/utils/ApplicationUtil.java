package gemini.example.com.hcpayment.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import java.util.Stack;

/**
 * Application类 初始化各种配置
 */
public class ApplicationUtil extends Application {

    private static Context mContext;//全局上下文对象
    RefWatcher refWatcher;
    public static Stack<Activity> store;
    private static final int MAX_ACTIVITY_DETAIL_NUM = 5;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
//        refWatcher = setupLeakCanary();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        store=new Stack<>();
        registerActivityLifecycleCallbacks(new SwitchBackgroundCallbacks());

    }

    public static Context getContext() {
        return mContext;
    }


    private class SwitchBackgroundCallbacks implements ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {
            if (store.size() >= MAX_ACTIVITY_DETAIL_NUM) {
                store.peek().finish(); //移除栈底的详情页并finish,保证商品详情页个数最大不超过指定
            }
            store.add(activity);
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            store.remove(activity);
        }
    }

    /**
     * 获取当前的Activity
     *
     * @return
     */
    public static  Activity getCurActivity() {
        return store.lastElement(); //返回栈顶Activity
    }

}
