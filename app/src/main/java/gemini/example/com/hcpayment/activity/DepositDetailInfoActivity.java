package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.ApplyRefoundInfoBean;
import gemini.example.com.hcpayment.entity.DepositBean;
import gemini.example.com.hcpayment.presenter.DepositInfoDetailPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.DepositDetailInfoView;

/**
 * Created by Gemini on 2018/5/31.
 */

public class DepositDetailInfoActivity extends BaseActivity<DepositDetailInfoView, DepositInfoDetailPresenter> implements DepositDetailInfoView {
    private int id = 0;

    @Override
    public DepositInfoDetailPresenter createPresenter() {
        return new DepositInfoDetailPresenter(this);
    }

    @Override
    public DepositDetailInfoView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("申请退还订金");
        cardModeRelative.setVisibility(View.GONE);
        voucherNumRelative.setVisibility(View.GONE);
        flowNumRelative.setVisibility(View.GONE);
        batchNumRelative.setVisibility(View.GONE);
        Intent intent = getIntent();
        if (intent != null) {
            bean = (DepositBean.DataBean.ListBean) intent.getSerializableExtra(SystemArgs.DEPOSITINFO);
            tenantName.setText(bean.getShopName());
            tenantNum.setText(bean.getShopNo());
            factIncome.setText(bean.getMoney() + "");
            shouldPayMoney.setText(bean.getMoney() + "");
            orderNum.setText(bean.getOrderNo());
            batchNum.setText(bean.getOrderNo());
            flowNum.setText(bean.getOrderNo());
            voucherNum.setText(bean.getOrderNo());
            dealTime.setText(bean.getCreateTime());
            operator.setText(bean.getOperator());
            guestInfoName.setText(bean.getCustomName());
            mobileNum.setText(bean.getMobile());

            id = bean.getId();
            moneyMode.setText("订金");
            cardNum.setText(bean.getCard());
        }

    }

    @OnClick({R.id.applyrefund})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.applyrefund:
                Intent intent = new Intent(this, ApplyrefunddepositTwoActivity.class);
                intent.putExtra(SystemArgs.DEPOSITINFO, (Serializable) bean);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_applyrefunddeposit_detail;
    }

    @Override
    public void callBackApplyInfo(ApplyRefoundInfoBean applyRefoundInfoBean) {
        if (applyRefoundInfoBean.getData() == 1) {
            skipActivity(ApplyDepositRefoundSuccessActivity.class, null);
            finish();
        } else {
            ToastUtil.showShortToast("申请退还订金失败");
        }
    }

    @Override
    public void callBackRefoundSucccess() {

    }

    @Override
    public void callbackRefoundFail() {
    }

    @BindView(R.id.cardNum)
    TextView cardNum;
    @BindView(R.id.applyrefund)
    Button applyRefound;
    @BindView(R.id.operater)
    TextView operator;
    @BindView(R.id.dealTime)
    TextView dealTime;
    @BindView(R.id.voucherNum)
    TextView voucherNum;
    @BindView(R.id.flowNum)
    TextView flowNum;
    @BindView(R.id.batchNum)
    TextView batchNum;
    @BindView(R.id.orderNum)
    TextView orderNum;
    @BindView(R.id.mobileNum)
    TextView mobileNum;
    @BindView(R.id.guestInfoName)
    TextView guestInfoName;
    @BindView(R.id.factIncome)
    TextView factIncome;
    @BindView(R.id.shouldPayMoney)
    TextView shouldPayMoney;
    @BindView(R.id.tenantName)
    TextView tenantName;
    @BindView(R.id.storeNum)
    TextView tenantNum;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.moneyMode)
    TextView moneyMode;
    @BindView(R.id.cardModeRelative)
    RelativeLayout cardModeRelative;
    @BindView(R.id.voucherNumRelative)
    RelativeLayout voucherNumRelative;
    @BindView(R.id.flowNumRelative)
    RelativeLayout flowNumRelative;
    @BindView(R.id.batchNumRelative)
    RelativeLayout batchNumRelative;
    DepositBean.DataBean.ListBean bean;


}
