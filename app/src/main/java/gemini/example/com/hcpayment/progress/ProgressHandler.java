package gemini.example.com.hcpayment.progress;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.ProgressBar;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Gemini on 2018/3/28.
 */

public class ProgressHandler extends Handler {
    public static final int SHOW_PROGRESS = 1;
    public static final int DISSMISS_PROGRESS = 2;
    public SweetAlertDialog sweetAlertDialog;
    private Context context;
    private boolean isCancelable;
    private ProgressCancelListener progressCancelListener;


    public ProgressHandler(boolean isCancelable, ProgressCancelListener progressCancelListener) {
        this.context = context;
        this.isCancelable = isCancelable;
        this.progressCancelListener = progressCancelListener;
    }

    public void showProgressDialog() {
        if (sweetAlertDialog == null) {
            sweetAlertDialog = new SweetAlertDialog(context);
            sweetAlertDialog.setTitleText("正在加载中...");
            sweetAlertDialog.setCancelable(isCancelable);
            if (isCancelable) {
                progressCancelListener.cancelProgress();
            }
            if (!sweetAlertDialog.isShowing()) {
                sweetAlertDialog.show();
            }
        }

    }

    public void hideProgressDialog() {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.dismiss();
            sweetAlertDialog = null;
        }
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what) {
            case SHOW_PROGRESS:
                showProgressDialog();
                break;
            case DISSMISS_PROGRESS:
                hideProgressDialog();
                break;
        }
    }
}
