package gemini.example.com.hcpayment.contract;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.base.BaseView;

/**
 * Created by Gemini on 2018/5/25.
 */

public interface ImportGuestNoContract {
    interface ImportGuestNoView extends BaseView{

    }
    abstract  class ImportGuestNoPresenter extends BasePresenter<ImportGuestNoView>{

    }

}
