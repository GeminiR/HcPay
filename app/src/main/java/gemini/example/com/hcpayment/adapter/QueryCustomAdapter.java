package gemini.example.com.hcpayment.adapter;

import java.util.List;

import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.entity.QueryCustomBean;

/**
 * Created by Gemini on 2018/6/16.
 */

public class QueryCustomAdapter extends BaseAdapter<QueryCustomBean.DataBean.ListBean,BaseAdapter.BaseViewHolder> {
    public QueryCustomAdapter(List<QueryCustomBean.DataBean.ListBean> data, int layoutId) {
        super(data, layoutId);
    }

    @Override
    void bindViewHolder(BaseAdapter.BaseViewHolder holder, QueryCustomBean.DataBean.ListBean data, int position) {
        holder.setText(R.id.guestInfoName,data.getName());
        holder.setText(R.id.guestInfoAddress,data.getAddress());
        holder.setText(R.id.guestInfoPhone,data.getMobile());
    }
}
