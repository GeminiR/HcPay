package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.util.Log;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.ApplyRefoundInfoBean;
import gemini.example.com.hcpayment.model.DepositInfoDetailModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.DepositDetailInfoView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/5/31.
 */

public class DepositInfoDetailPresenter extends BasePresenter<DepositDetailInfoView> {
    Context context;
    DepositInfoDetailModel depositInfoDetailModel;
    private String TAG = "DepositInfoDetailPresenter";

    public DepositInfoDetailPresenter(Context context) {
        this.context = context;
        depositInfoDetailModel = new DepositInfoDetailModel();
    }

}
