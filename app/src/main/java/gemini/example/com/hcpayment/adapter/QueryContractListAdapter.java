package gemini.example.com.hcpayment.adapter;

import java.util.List;

import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.ContractListBean;
import gemini.example.com.hcpayment.entity.QueryContractStatusInfo;

/**
 * Created by Gemini on 2018/6/1.
 */

public class QueryContractListAdapter extends BaseAdapter<QueryContractStatusInfo.DataBean.ListBean,BaseAdapter.BaseViewHolder> {
    public QueryContractListAdapter(List<QueryContractStatusInfo.DataBean.ListBean> data, int layoutId) {
        super(data, layoutId);
    }


    @Override
    void bindViewHolder(BaseAdapter.BaseViewHolder holder, QueryContractStatusInfo.DataBean.ListBean data, int position) {
        holder.setText(R.id.guestInfoName,data.getCustomName());
        holder.setText(R.id.guestInfoPhone,data.getMobile());
        holder.setText(R.id.guestInfoAddress,data.getHouseAddress());
        holder.setText(R.id.guestInfactFee,data.getActualMoney()+"");
        holder.setText(R.id.orderTime,data.getCreateTime());
        holder.setText(R.id.guestStatus,data.getNo());
    }
}
