package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.DepositBean;
import gemini.example.com.hcpayment.entity.ProductInfoBean;
import gemini.example.com.hcpayment.entity.StagingInfo;
import gemini.example.com.hcpayment.model.CreateOrderTwoModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.ui.ProductInfoAlertDialog;
import gemini.example.com.hcpayment.ui.SelectDepositAlertDialog;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.utils.SharePreUtils;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.CreateOrderTwoView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/5/28.
 */

public class CreateOrderTwoPresenter extends BasePresenter<CreateOrderTwoView> implements
        ProductInfoAlertDialog.OnSelectStyleWithContentListener, BaseAdapter.onItemClickListener {
    private String TAG = "CreateOrderTwoPresenter";
    Context context;
    CreateOrderTwoModel createOrderTwoModel;
    private int productId = 0;//产品包id来计算产品包总额
    private int customId = 0;//客户id来获取订金

    public CreateOrderTwoPresenter(Context context) {
        this.context = context;
        createOrderTwoModel = new CreateOrderTwoModel();
    }

    //获取产品包信息
    public void getProductData(String token) {
        getView().showProgressDialog(true);
        createOrderTwoModel.queryProduct(token, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                ProductInfoBean productInfoBean = (ProductInfoBean) o;
                showProductWheel(productInfoBean);
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                Log.d(TAG, "e---->" + e.toString());
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });

    }

    public void showProductWheel(ProductInfoBean productInfoBean) {
        ProductInfoAlertDialog productInfoAlertDialog = new ProductInfoAlertDialog(context, productInfoBean, this);
        productInfoAlertDialog.init();
    }

    //显示订金获取Dialog
    public void showDepositInfo(int customId,int page) {
        getView().showProgressDialog(true);
        createOrderTwoModel.queryDepositInfo(getView().getToken(), "", customId, page, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                DepositBean depositBean = (DepositBean) o;
                getView().callbackDepositData(depositBean);

            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
//
    }

    /**
     * 保存合同信息进入下一步
     */
    public void keepOrderInfo(String token, int customId, String orderNo, int productId, double discount, int frontMoneyId, String decorationadd, double buildingArea, double specialMoney, double decorationArea) {
        getView().showProgressDialog(true);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("customId", customId);
        jsonObject.addProperty("no", orderNo);
        jsonObject.addProperty("productId", productId);
        jsonObject.addProperty("discount", discount);
        if (frontMoneyId != 0) {
            jsonObject.addProperty("frontMoneyId", frontMoneyId);
        }
        jsonObject.addProperty("houseAddress", decorationadd);
        jsonObject.addProperty("houseArea", buildingArea);
        jsonObject.addProperty("houseDecorationArea", decorationArea);
        Log.d(TAG,"SepcialMoney-----》"+specialMoney);
        jsonObject.addProperty("specialMoney", specialMoney);
        String stageInfo = new Gson().toJson(jsonObject);
        createOrderTwoModel.queryStagingInfo(token, stageInfo, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                StagingInfo stagingInfo = (StagingInfo) o;
                getView().keepOrderSuccess(stagingInfo);
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                Log.d(TAG, "e---->" + e.toString());
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {

            }
        });
    }

    @Override
    public void onItemClick(int position, View v) {
        SelectDepositAlertDialog selectDepositAlertDialog=getView().getSelectDepositAlertDialog();
        if(selectDepositAlertDialog!=null){
            List<DepositBean.DataBean.ListBean> data = selectDepositAlertDialog.getQueryDepositDialogAdapter().getData();
            double money = data.get(position).getMoney();
            int id = data.get(position).getId();
            getView().updateDepositInfo(money, id);
            if (selectDepositAlertDialog != null) {
                selectDepositAlertDialog.dismiss();
            }
        }
    }


    /**
     * 回调wheelView点击确定时候所选的产品包信息
     *
     * @param id
     * @param style
     * @param styleName
     * @param price
     */
    @Override
    public void callbackStyle(int id, String style, String styleName, double price) {
//        Log.d(TAG,"id---->"+id+",style---->"+style+",styleName--->"+styleName+",price--->"+price);
        getView().updateProductInfo(id, style, styleName, price);
        productId = id;
    }

    public TextWatcher getTextWatcher(){
        return  moneyTextChange;
    }
    public TextWatcher moneyTextChange = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            double productAllMoney = getView().getProductAllMoney();
            double discountMoney = getView().getDiscountMoney();
            double depositMoney = getView().getDepositMoney();
            double specialMoney = getView().getSpecialMoeny();
            getView().updateAllMoney((productAllMoney - discountMoney - depositMoney +specialMoney));

        }
    };
}
