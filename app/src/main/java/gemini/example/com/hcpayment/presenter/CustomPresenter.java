package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;


import java.util.List;

import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.base.BaseResponse;
import gemini.example.com.hcpayment.contract.CustomContract;
import gemini.example.com.hcpayment.entity.CustomBean;
import gemini.example.com.hcpayment.model.CustomModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/5/9.
 */

public class CustomPresenter extends CustomContract.CustomPresenter implements TextView.OnEditorActionListener, BaseAdapter.onItemClickListener {

    private String TAG = "CustomPresenter";
    CustomModel customModel;
    CustomBean customInfo;

    public CustomPresenter() {
        customModel = new CustomModel();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        getView().setCurrentPage(1);
        queryData(getView().getCurrentPage());
        return true;
    }

    public void queryData(int page) {
        getView().showProgressDialog(true);
        customModel.queryCustom(getView().getToken(), getView().getMobile(),page, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                CustomBean customBean = (CustomBean) o;
                customInfo = customBean;
                getView().result(customBean);
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {

            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }

    public RecyclerView.OnScrollListener getOnScrollListener() {
        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                    int lastVisiblePosition;
                    lastVisiblePosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                    if (layoutManager.getChildCount() > 0             //当当前显示的item数量>0
                            && lastVisiblePosition >= layoutManager.getItemCount() - 1           //当当前屏幕最后一个加载项位置>=所有item的数量
                            && layoutManager.getItemCount() > layoutManager.getChildCount()) { // 当当前总Item数大于可见Item数
                        getView().onLoadMore();
                    }else{
                        getView().onNoLoadMore();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        };
        return onScrollListener;
    }
    @Override
    public void onItemClick(int position, View v) {
        getView().dealCustomInfo(getView().getCustomerAdapter().getData().get(position));
        getView().finishActivity();
    }
}
