package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.LoginBean;
import gemini.example.com.hcpayment.model.LoginModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.LoginView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/3/30.
 */

public class LoginPresenter extends BasePresenter<LoginView> {
    LoginModel loginModel;
    private String TAG = "LoginPresenter";

    public LoginPresenter() {
        this.loginModel = new LoginModel();
    }

    public void login(String userName, String passWord) {
        getView().showProgressDialog(true);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", userName);
        jsonObject.addProperty("password", passWord);
        String json = new Gson().toJson(jsonObject);
        loginModel.login(json, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                LoginBean loginBean = (LoginBean) o;
                Log.d(TAG, "loginBean---->" + loginBean);
                getView().onCallbackLoginResult(loginBean);
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {
                getView().hideProgressDialog();
            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }
}
