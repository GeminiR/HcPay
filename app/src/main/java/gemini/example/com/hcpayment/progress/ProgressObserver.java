package gemini.example.com.hcpayment.progress;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.JsonObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import gemini.example.com.hcpayment.activity.LoginActivity;
import gemini.example.com.hcpayment.base.BaseResponse;
import gemini.example.com.hcpayment.utils.ApplicationUtil;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.utils.ToastUtil;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

/**
 * Created by Gemini on 2018/3/28.
 */

public class ProgressObserver<T> implements Observer<T> {
    private String TAG = ProgressObserver.class.getSimpleName();
    private Disposable d;
    //    private ProgressHandler progressHandler;
//    private Context ctx;
    private ObserverResponseListener observerResponseListener;
    private boolean isDialog;
    private boolean isCancelable;
    BaseResponse baseResponse;

    public ProgressObserver(ObserverResponseListener observerResponseListener) {
        this.observerResponseListener = observerResponseListener;
    }

    @Override
    public void onSubscribe(Disposable d) {
        this.d = d;
        observerResponseListener.onSubscribe(d);
    }

    @Override
    public void onNext(T t) {
        baseResponse = (BaseResponse) t;
        if (baseResponse.getMeta().isSuccess()) {
            observerResponseListener.onNext(t);
        } else {
            int code = baseResponse.getMeta().getCode();
            if (code == 1002) {
                final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ApplicationUtil.getCurActivity(), SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("错误");
                sweetAlertDialog.setContentText("登录超时");
                sweetAlertDialog.setConfirmText("登录");
                sweetAlertDialog.show();
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        Intent intent = new Intent(ApplicationUtil.getCurActivity(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        ApplicationUtil.getCurActivity().startActivity(intent);
                        if(ApplicationUtil.store!=null){
                            for (Activity a :ApplicationUtil.store) {
                                if(a!=null){
                                    a.finish();
                                }
                            }
                        }
                        sweetAlertDialog.cancel();
                    }
                });

            } else {
                ToastUtil.showShortToast(baseResponse.getMeta().getMessage());
            }

        }
    }

    @Override
    public void onError(Throwable e) {
        Log.e(TAG, "onError: ", e);
        //自定义异常处理
        if (e instanceof ExceptionHandle.ResponseException) {
            observerResponseListener.onError((ExceptionHandle.ResponseException) e);
        } else {
            observerResponseListener.onError(new ExceptionHandle.ResponseException(e, ExceptionHandle.ERROR.UNKNOWN));
        }

        if (e instanceof UnknownHostException) {
            ToastUtil.showLongToast("请打开网络");
        } else if (e instanceof SocketTimeoutException) {
            ToastUtil.showLongToast("请求超时");
        } else if (e instanceof ConnectException) {
            ToastUtil.showLongToast("连接失败");
        } else if (e instanceof HttpException) {
            ToastUtil.showLongToast("请求超时");
        } else {
            if (baseResponse != null) {
                ToastUtil.showLongToast(baseResponse.getMeta().getMessage());
            } else {
                ToastUtil.showLongToast("请求失败");
            }

        }
    }

    @Override
    public void onComplete() {
//        hideProgressDialog();
        observerResponseListener.onComplete();
    }

//    public void showProgressDialog() {
//        if (progressHandler != null) {
//            progressHandler.sendEmptyMessage(ProgressHandler.SHOW_PROGRESS);
//        }
//    }
//
//    public void hideProgressDialog() {
//        if (progressHandler != null) {
//            progressHandler.sendEmptyMessage(ProgressHandler.DISSMISS_PROGRESS);
//            progressHandler = null;
//        }
//    }

//    @Override
//    public void cancelProgress() {
//        if(!d.isDisposed()){
//            d.dispose();
//        }
//    }
}
