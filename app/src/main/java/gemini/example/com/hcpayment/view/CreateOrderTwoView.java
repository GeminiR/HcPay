package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.DepositBean;
import gemini.example.com.hcpayment.entity.StagingInfo;
import gemini.example.com.hcpayment.ui.SelectDepositAlertDialog;

/**
 * Created by Gemini on 2018/5/28.
 */

public interface CreateOrderTwoView extends BaseView {
    void showProductWheel();
    void showDepositDialog(int customId);
    void updateProductInfo(int id,String style,String styleName,double price);
    void updateDepositInfo(double money,int id);
    void keepOrderInfo(String token,String orderNo,int productId,double discount,int frontMoneyId,String decorationadd,double buildingArea,double specialMoney,double decorationArea);
    void keepOrderSuccess(StagingInfo stagingInfo);
    double getProductAllMoney();
    double getDiscountMoney();
    double getDepositMoney();
    double getSpecialMoeny();
    void updateAllMoney(double money);
    void callbackDepositData(DepositBean depositBean);
    SelectDepositAlertDialog getSelectDepositAlertDialog();
}
