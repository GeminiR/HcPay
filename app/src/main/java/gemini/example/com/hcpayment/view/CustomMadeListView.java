package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.adapter.QueryCustommadeListAdapter;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.entity.QueryCustommadeListBean;

/**
 * Created by Gemini on 2018/5/31.
 */

public interface CustomMadeListView extends BaseView {
    void onCallbackCustomMadeList(QueryCustommadeListBean queryCustommadeListBean);
    void onCallbackItemClick(QueryCustommadeListBean.DataBean.ListBean listBean);
    QueryCustommadeListAdapter getCustomAdapter();
    void onLoadMore();
    void onNoLoadMore();
}
