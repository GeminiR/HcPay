package gemini.example.com.hcpayment.model;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.api.ApiSevice;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/6/15.
 */

public class QueryCustomModel extends BaseModel{

    public void queryCustom(String token, String mobile, int pageNum,  ObserverResponseListener observerResponseListener){
        subscribe(Api.getApiSevice().queryCustomInfo(token,mobile,pageNum),observerResponseListener);
    }
}
