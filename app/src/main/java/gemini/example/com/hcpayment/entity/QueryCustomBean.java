package gemini.example.com.hcpayment.entity;

import java.io.Serializable;
import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/6/15.
 */

public class QueryCustomBean extends BaseResponse implements Serializable{

    /**
     * data : {"pages":1,"total":6,"next":"","prev":"","list":[{"id":1,"name":"测试3","mobile":"13655222222","openid":"","unionid":"","avatar":"","no":"2018033000009","manager":"余梦玲","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"89.00","decorationArea":"","address":"杭州市余杭区新湖菲林8-3-302"},{"id":2,"name":"测试客户4","mobile":"5","openid":"","unionid":"","avatar":"","no":"2018032900007","manager":"余梦玲","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"89.00","decorationArea":"","address":"杭州市西湖区大华西溪风情6-3-302"},{"id":3,"name":"测试客户3","mobile":"3","openid":"","unionid":"","avatar":"","no":"2018032700005","manager":"余梦玲","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"","decorationArea":"","address":""},{"id":4,"name":"虞女士","mobile":"15988191140","openid":"","unionid":"","avatar":"","no":"2017111100022","manager":"刘颜菘","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"105.00","decorationArea":"","address":"杭州市余杭区瑞金华丽嘉苑6-1-1601"},{"id":5,"name":"庄女士","mobile":"13810426535","openid":"","unionid":"","avatar":"","no":"2017101700011","manager":"宋春雷","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"74.00","decorationArea":"","address":"杭州市滨江区顺发康庄5-1-2303"},{"id":6,"name":"范建海","mobile":"15325812129","openid":"","unionid":"","avatar":"","no":"2017101100001","manager":"刘颜菘","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"","decorationArea":"","address":"杭州市余杭区EFC欧美金融城9-2-2402"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * pages : 1
         * total : 6
         * next :
         * prev :
         * list : [{"id":1,"name":"测试3","mobile":"13655222222","openid":"","unionid":"","avatar":"","no":"2018033000009","manager":"余梦玲","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"89.00","decorationArea":"","address":"杭州市余杭区新湖菲林8-3-302"},{"id":2,"name":"测试客户4","mobile":"5","openid":"","unionid":"","avatar":"","no":"2018032900007","manager":"余梦玲","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"89.00","decorationArea":"","address":"杭州市西湖区大华西溪风情6-3-302"},{"id":3,"name":"测试客户3","mobile":"3","openid":"","unionid":"","avatar":"","no":"2018032700005","manager":"余梦玲","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"","decorationArea":"","address":""},{"id":4,"name":"虞女士","mobile":"15988191140","openid":"","unionid":"","avatar":"","no":"2017111100022","manager":"刘颜菘","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"105.00","decorationArea":"","address":"杭州市余杭区瑞金华丽嘉苑6-1-1601"},{"id":5,"name":"庄女士","mobile":"13810426535","openid":"","unionid":"","avatar":"","no":"2017101700011","manager":"宋春雷","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"74.00","decorationArea":"","address":"杭州市滨江区顺发康庄5-1-2303"},{"id":6,"name":"范建海","mobile":"15325812129","openid":"","unionid":"","avatar":"","no":"2017101100001","manager":"刘颜菘","createTime":"2018-05-24 03:46:08","updateTime":"2018-05-24 03:46:08","area":"","decorationArea":"","address":"杭州市余杭区EFC欧美金融城9-2-2402"}]
         */

        private int pages;
        private int total;
        private String next;
        private String prev;
        private List<ListBean> list;

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrev() {
            return prev;
        }

        public void setPrev(String prev) {
            this.prev = prev;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean  implements Serializable{
            /**
             * id : 1
             * name : 测试3
             * mobile : 13655222222
             * openid :
             * unionid :
             * avatar :
             * no : 2018033000009
             * manager : 余梦玲
             * createTime : 2018-05-24 03:46:08
             * updateTime : 2018-05-24 03:46:08
             * area : 89.00
             * decorationArea :
             * address : 杭州市余杭区新湖菲林8-3-302
             */

            private int id;
            private String name;
            private String mobile;
            private String openid;
            private String unionid;
            private String avatar;
            private String no;
            private String manager;
            private String createTime;
            private String updateTime;
            private String area;
            private String decorationArea;
            private String address;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getOpenid() {
                return openid;
            }

            public void setOpenid(String openid) {
                this.openid = openid;
            }

            public String getUnionid() {
                return unionid;
            }

            public void setUnionid(String unionid) {
                this.unionid = unionid;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getNo() {
                return no;
            }

            public void setNo(String no) {
                this.no = no;
            }

            public String getManager() {
                return manager;
            }

            public void setManager(String manager) {
                this.manager = manager;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getDecorationArea() {
                return decorationArea;
            }

            public void setDecorationArea(String decorationArea) {
                this.decorationArea = decorationArea;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }
        }
    }
}
