package gemini.example.com.hcpayment.entity;

import java.io.Serializable;
import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/6/13.
 */

public class QueryContractStatusInfo extends BaseResponse implements Serializable
{

    /**
     * data : {"pages":1,"total":1,"next":"","prev":"","list":[{"id":2,"no":"HC001-0000000002","createTime":"2018-05-25 03:38:13","updateTime":"2018-05-30 03:27:40","productId":1,"productName":"雅66","productPrice":1500,"customId":6,"customName":"范建海","houseId":2,"houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","houseArea":108.22,"houseDecorationArea":90,"frontMoneyId":4,"frontMoney":12345.79,"discount":5000,"actualMoney":117654.21,"specialMoney":2000,"manager":"刘颜菘","stage":1,"shopId":"","shopName":"","shopNo":"","status":2,"contractStages":"","specialMoneyListVOS":"","stages":""}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * pages : 1
         * total : 1
         * next :
         * prev :
         * list : [{"id":2,"no":"HC001-0000000002","createTime":"2018-05-25 03:38:13","updateTime":"2018-05-30 03:27:40","productId":1,"productName":"雅66","productPrice":1500,"customId":6,"customName":"范建海","houseId":2,"houseAddress":"杭州市西湖区紫霞街188号西溪蝶园-北区","houseArea":108.22,"houseDecorationArea":90,"frontMoneyId":4,"frontMoney":12345.79,"discount":5000,"actualMoney":117654.21,"specialMoney":2000,"manager":"刘颜菘","stage":1,"shopId":"","shopName":"","shopNo":"","status":2,"contractStages":"","specialMoneyListVOS":"","stages":""}]
         */

        private int pages;
        private int total;
        private String next;
        private String prev;
        private List<ListBean> list;

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrev() {
            return prev;
        }

        public void setPrev(String prev) {
            this.prev = prev;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean  implements Serializable{
            /**
             * id : 2
             * no : HC001-0000000002
             * createTime : 2018-05-25 03:38:13
             * updateTime : 2018-05-30 03:27:40
             * productId : 1
             * productName : 雅66
             * productPrice : 1500
             * customId : 6
             * customName : 范建海
             * houseId : 2
             * houseAddress : 杭州市西湖区紫霞街188号西溪蝶园-北区
             * houseArea : 108.22
             * houseDecorationArea : 90
             * frontMoneyId : 4
             * frontMoney : 12345.79
             * discount : 5000
             * actualMoney : 117654.21
             * specialMoney : 2000
             * manager : 刘颜菘
             * stage : 1
             * shopId :
             * shopName :
             * shopNo :
             * status : 2
             * contractStages :
             * specialMoneyListVOS :
             * stages :
             */

            private int id;
            private String no;
            private String createTime;
            private String updateTime;
            private int productId;
            private String productName;
            private int productPrice;
            private int customId;
            private String customName;
            private int houseId;
            private String houseAddress;
            private double houseArea;
            private int houseDecorationArea;
            private int frontMoneyId;
            private double frontMoney;
            private int discount;
            private double actualMoney;
            private int specialMoney;
            private String manager;
            private int stage;
            private String shopId;
            private String shopName;
            private String shopNo;
            private int status;
            private String contractStages;
            private String specialMoneyListVOS;
            private String stages;
            private String mobile;

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getNo() {
                return no;
            }

            public void setNo(String no) {
                this.no = no;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public int getProductId() {
                return productId;
            }

            public void setProductId(int productId) {
                this.productId = productId;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public int getProductPrice() {
                return productPrice;
            }

            public void setProductPrice(int productPrice) {
                this.productPrice = productPrice;
            }

            public int getCustomId() {
                return customId;
            }

            public void setCustomId(int customId) {
                this.customId = customId;
            }

            public String getCustomName() {
                return customName;
            }

            public void setCustomName(String customName) {
                this.customName = customName;
            }

            public int getHouseId() {
                return houseId;
            }

            public void setHouseId(int houseId) {
                this.houseId = houseId;
            }

            public String getHouseAddress() {
                return houseAddress;
            }

            public void setHouseAddress(String houseAddress) {
                this.houseAddress = houseAddress;
            }

            public double getHouseArea() {
                return houseArea;
            }

            public void setHouseArea(double houseArea) {
                this.houseArea = houseArea;
            }

            public int getHouseDecorationArea() {
                return houseDecorationArea;
            }

            public void setHouseDecorationArea(int houseDecorationArea) {
                this.houseDecorationArea = houseDecorationArea;
            }

            public int getFrontMoneyId() {
                return frontMoneyId;
            }

            public void setFrontMoneyId(int frontMoneyId) {
                this.frontMoneyId = frontMoneyId;
            }

            public double getFrontMoney() {
                return frontMoney;
            }

            public void setFrontMoney(double frontMoney) {
                this.frontMoney = frontMoney;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public double getActualMoney() {
                return actualMoney;
            }

            public void setActualMoney(double actualMoney) {
                this.actualMoney = actualMoney;
            }

            public int getSpecialMoney() {
                return specialMoney;
            }

            public void setSpecialMoney(int specialMoney) {
                this.specialMoney = specialMoney;
            }

            public String getManager() {
                return manager;
            }

            public void setManager(String manager) {
                this.manager = manager;
            }

            public int getStage() {
                return stage;
            }

            public void setStage(int stage) {
                this.stage = stage;
            }

            public String getShopId() {
                return shopId;
            }

            public void setShopId(String shopId) {
                this.shopId = shopId;
            }

            public String getShopName() {
                return shopName;
            }

            public void setShopName(String shopName) {
                this.shopName = shopName;
            }

            public String getShopNo() {
                return shopNo;
            }

            public void setShopNo(String shopNo) {
                this.shopNo = shopNo;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getContractStages() {
                return contractStages;
            }

            public void setContractStages(String contractStages) {
                this.contractStages = contractStages;
            }

            public String getSpecialMoneyListVOS() {
                return specialMoneyListVOS;
            }

            public void setSpecialMoneyListVOS(String specialMoneyListVOS) {
                this.specialMoneyListVOS = specialMoneyListVOS;
            }

            public String getStages() {
                return stages;
            }

            public void setStages(String stages) {
                this.stages = stages;
            }
        }
    }
}
