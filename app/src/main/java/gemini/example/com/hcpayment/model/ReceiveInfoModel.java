package gemini.example.com.hcpayment.model;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/6/16.
 */

public class ReceiveInfoModel extends BaseModel {
    public void queryReceiveInfo(String token, int status, String no,int page, ObserverResponseListener observerResponseListener){
        subscribe(Api.getApiSevice().queryReceiveInfo(token,status,no,page),observerResponseListener);
    }

}
