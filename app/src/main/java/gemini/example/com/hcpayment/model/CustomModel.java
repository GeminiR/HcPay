package gemini.example.com.hcpayment.model;

import android.content.Context;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.api.ApiSevice;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/5/9.
 */

public class CustomModel<T> extends BaseModel{
    public void queryCustom(String token,String mobile,int page ,ObserverResponseListener observerResponseListener){
        subscribe( Api.getApiSevice().getCustom(token,mobile,page),observerResponseListener);
    }
}
