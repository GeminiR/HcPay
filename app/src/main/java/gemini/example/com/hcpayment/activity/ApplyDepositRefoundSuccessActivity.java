package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.presenter.ApplyRefoundDepositSuccessPresenter;
import gemini.example.com.hcpayment.view.ApplyRefoundDepositSuccessView;

/**
 * Created by Gemini on 2018/5/31.
 */

public class ApplyDepositRefoundSuccessActivity extends BaseActivity<ApplyRefoundDepositSuccessView, ApplyRefoundDepositSuccessPresenter> implements ApplyRefoundDepositSuccessView {
    @Override
    public ApplyRefoundDepositSuccessPresenter createPresenter() {
        return new ApplyRefoundDepositSuccessPresenter();
    }

    @Override
    public ApplyRefoundDepositSuccessView createView() {
        return this;
    }

    @Override
    public void init() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_applydrawbacksuccess;
    }

    @OnClick({R.id.backOrderManager})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backOrderManager:
                Intent intent=new Intent(this,DepositManagerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            default:
                break;
        }
    }


}
