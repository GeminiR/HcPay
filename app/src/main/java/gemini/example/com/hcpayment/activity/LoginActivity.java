package gemini.example.com.hcpayment.activity;

import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.base.BaseResponse;
import gemini.example.com.hcpayment.entity.LoginBean;
import gemini.example.com.hcpayment.presenter.LoginPresenter;
import gemini.example.com.hcpayment.utils.SharePreUtils;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.LoginView;

/**
 * Created by Gemini on 2018/3/30.
 */

public class LoginActivity extends BaseActivity<LoginView, LoginPresenter> implements LoginView {

    @BindView(R.id.userName)
    EditText userName;
    @BindView(R.id.passWord)
    EditText passWord;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    public LoginView createView() {
        return this;
    }

    @Override
    public void init() {
        toolbar.setVisibility(View.GONE);
    }

    @Override
    public int getLayoutId() {
        return R.layout.login;
    }


    @OnClick({R.id.login})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                String user = userName.getText().toString().trim();
                String pass = passWord.getText().toString().trim();
                if (!TextUtils.isEmpty(user)) {
                    if (!TextUtils.isEmpty(pass)) {
                        getPresenter().login(user, pass);
                    } else {
                        ToastUtil.showShortToast("登录密码不能为空");
                    }
                } else {
                    ToastUtil.showShortToast("商户名称不能为空");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCallbackLoginResult(LoginBean loginBean) {
        String token = loginBean.getData().getToken();
        String userName = loginBean.getData().getUser().getUsername();
        String pass = passWord.getText().toString().trim();
        SharePreUtils.putString(this, SystemArgs.USERNAME, userName);
        SharePreUtils.putString(this, SystemArgs.PASSWORD, pass);
        SharePreUtils.putString(this, SystemArgs.ACCOUNTNAME, loginBean.getData().getUser().getName());
        SharePreUtils.putString(this, SystemArgs.TOKEN, token);
        skipActivity(HcMainPageActivity.class, null);
        finish();
    }
}
