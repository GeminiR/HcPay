package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.LoginBean;
import gemini.example.com.hcpayment.model.LoginModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.HcMainPageView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/5/30.
 */

public class HcMainPagePresenter extends BasePresenter<HcMainPageView> {
    LoginModel loginModel;

    public HcMainPagePresenter() {
        loginModel=new LoginModel();
    }


}
