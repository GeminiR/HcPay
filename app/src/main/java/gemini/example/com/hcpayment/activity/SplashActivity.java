package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.base.BaseView;
import gemini.example.com.hcpayment.utils.SharePreUtils;
import gemini.example.com.hcpayment.utils.SystemArgs;

/**
 * Created by Gemini on 2018/6/25.
 */

public class SplashActivity extends BaseActivity {
    @Override
    public BasePresenter createPresenter() {
        return null;
    }

    @Override
    public BaseView createView() {
        return null;
    }

    @Override
    public void init() {
        View decorView = getWindow().getDecorView();
        int option = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(option);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String token = SharePreUtils.getString(this, SystemArgs.TOKEN, "");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(token)) {
            Intent intent = new Intent(this, HcMainPageActivity.class);
            intent.putExtra(SystemArgs.ISLOGOIN,true);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }
}
