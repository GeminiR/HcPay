package gemini.example.com.hcpayment.model;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Map;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/5/31.
 */

public class PayDepositModel extends BaseModel {
    public void keepDeposit(String token, String keepDepositJson, ObserverResponseListener observerResponseListener) {
        subscribe(Api.getApiSevice().keepDepositBean(token, keepDepositJson), observerResponseListener);
    }

    public void checkTransactionInfo(String token, String qrCodeScanModel, String paymentType, String amount, String authNo,
                                     String cardNo, String countN, String terminalId, String acquire, String traceNo, String transId, String qrOrderNo,
                                     String batchNo, String merchantId, String referenceNo, String cardType, String operatorId, String payreason, String appId,
                                     String issue, String model, String transactionPlatform, String sumAmount, String errorCode, String version, String merchantName,
                                     String cardSecret, String orderNoSFT, String accountType, String packageName, String answerCode, String voucherNo,
                                     String transDate, String transName, String transTime, String transType, String transactionType, String qrCodeTransactionState,
                                     String barcodeType, int payState,int type, ObserverResponseListener observerResponseListener) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ordernum",orderNoSFT);
        jsonObject.addProperty("qrCodeScanModel", qrCodeScanModel);
        jsonObject.addProperty("paymentType", paymentType);
        jsonObject.addProperty("amount", amount);
        jsonObject.addProperty("authNo", authNo);

        jsonObject.addProperty("cardNo", cardNo);
        jsonObject.addProperty("countN", countN);
        jsonObject.addProperty("terminalId", terminalId);
        jsonObject.addProperty("acquire", acquire);
        jsonObject.addProperty("traceNo", traceNo);
        jsonObject.addProperty("transId", transId);
        jsonObject.addProperty("qrOrderNo", qrOrderNo);

        jsonObject.addProperty("batchNo", batchNo);
        jsonObject.addProperty("merchantId", merchantId);
        jsonObject.addProperty("referenceNo", referenceNo);
        jsonObject.addProperty("cardType", cardType);
        jsonObject.addProperty("operatorId", operatorId);
        jsonObject.addProperty("payreason", payreason);
        jsonObject.addProperty("appId", appId);

        jsonObject.addProperty("issue", issue);
        jsonObject.addProperty("model", model);
        jsonObject.addProperty("transactionPlatform", transactionPlatform);
        jsonObject.addProperty("sumAmount", sumAmount);
        jsonObject.addProperty("errorCode", errorCode);
        jsonObject.addProperty("version", version);
        jsonObject.addProperty("merchantName", merchantName);

        jsonObject.addProperty("cardSecret", cardSecret);
        jsonObject.addProperty("orderNoSFT", orderNoSFT);
        jsonObject.addProperty("accountType", accountType);
        jsonObject.addProperty("packageName", packageName);
        jsonObject.addProperty("answerCode", answerCode);
        jsonObject.addProperty("voucherNo", voucherNo);

        jsonObject.addProperty("transDate", transDate);
        jsonObject.addProperty("transName", transName);
        jsonObject.addProperty("transTime", transTime);
        jsonObject.addProperty("transType", transType);
        jsonObject.addProperty("transactionType", transactionType);
        jsonObject.addProperty("qrCodeTransactionState", qrCodeTransactionState);
        jsonObject.addProperty("barcodeType", barcodeType);

        jsonObject.addProperty("state",payState);
        jsonObject.addProperty("type",type);

        String transcationJson = new Gson().toJson(jsonObject);
        Log.d("PayDepositModel","transcationJson---->"+transcationJson);
        subscribe(Api.getApiSevice().checkTrancation(token, transcationJson), observerResponseListener);

    }
}
