package gemini.example.com.hcpayment.presenter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import gemini.example.com.hcpayment.adapter.BaseAdapter;
import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.entity.DepositBean;
import gemini.example.com.hcpayment.model.DepositManagerModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;
import gemini.example.com.hcpayment.utils.ExceptionHandle;
import gemini.example.com.hcpayment.view.DepositManagerView;
import io.reactivex.disposables.Disposable;

/**
 * Created by Gemini on 2018/5/31.
 */

public class DepositManagerPresenter extends BasePresenter<DepositManagerView> implements BaseAdapter.onItemClickListener{
    private String TAG = "DepositManagerPresenter";

    DepositManagerModel depositManagerModel;
    DepositBean depositBean;

    public DepositManagerPresenter() {
        depositManagerModel = new DepositManagerModel();
    }

    public OnScrollListener getOnScrollListener() {
        OnScrollListener onScrollListener = new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                    int lastVisiblePosition;
                    lastVisiblePosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                    if (layoutManager.getChildCount() > 0             //当当前显示的item数量>0
                            && lastVisiblePosition >= layoutManager.getItemCount() - 1           //当当前屏幕最后一个加载项位置>=所有item的数量
                            && layoutManager.getItemCount() > layoutManager.getChildCount()) { // 当当前总Item数大于可见Item数
                        getView().onLoadMore();
                    }else{
                        getView().onNoLoadMore();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        };
        return onScrollListener;
    }

    public void callbackDepositData(int status, int page) {
        getView().showProgressDialog(true);
//        String token="4BGi0v0FDilt3UZFBuFw/Q==";
        depositManagerModel.queryDepositManagerList(getView().getToken(), getView().getMobile(),
                status, page, new ObserverResponseListener() {
            @Override
            public void onNext(Object o) {
                depositBean = (DepositBean) o;
                getView().callBackCustomData(depositBean);
            }

            @Override
            public void onError(ExceptionHandle.ResponseException e) {


            }

            @Override
            public void onComplete() {
                getView().hideProgressDialog();
            }

            @Override
            public void onSubscribe(Disposable d) {
                getView().putDispose(d);
            }
        });
    }

    @Override
    public void onItemClick(int position, View v) {
        if(getView().getManagerAdapter()!=null){
//            Log.d(TAG,"data--->"+getView().getManagerAdapter().getData());
//            Log.d(TAG,"position----->"+position);
            getView().callItemClick(getView().getManagerAdapter().getData().get(position));
        }
    }

}
