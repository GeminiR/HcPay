package gemini.example.com.hcpayment.model;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/5/31.
 */

public class DepositInfoDetailModel extends BaseModel {


    public void applyRefoundDeposit(String token,int id,String refundCard,String refundName,String refundBank,String refundRemark,
                                    ObserverResponseListener observerResponseListener){
        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("refundCard",refundCard);
        jsonObject.addProperty("refundName",refundName);
        jsonObject.addProperty("refundBank",refundBank);
        jsonObject.addProperty("refundRemark",refundRemark);
        String json=new Gson().toJson(jsonObject);
        subscribe(Api.getApiSevice().applyRefoundInfo(token,id,json),observerResponseListener);
    }
}
