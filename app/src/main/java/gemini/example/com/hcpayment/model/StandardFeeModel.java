package gemini.example.com.hcpayment.model;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/6/13.
 */

public class StandardFeeModel extends BaseModel {

    public void queryOrder(String token, int stage, String mobile, int status,int page, ObserverResponseListener observerResponseListener) {
        subscribe(Api.getApiSevice().queryStandardFeeList(token, stage, mobile, status,page), observerResponseListener);
    }
}
