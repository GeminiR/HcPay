package gemini.example.com.hcpayment.model;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/6/6.
 */

public class ContractDetailModel extends BaseModel {
    public void queryContractStageInfo(String token,int contractId, ObserverResponseListener observerResponseListener) {
        subscribe(Api.getApiSevice().queryContractStageInfo(token,contractId), observerResponseListener);
    }

    public void submitStageCheck(String token,int id, ObserverResponseListener observerResponseListener) {
        subscribe(Api.getApiSevice().submitStage(token,id), observerResponseListener);
    }

}
