package gemini.example.com.hcpayment.entity;

import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/6/16.
 */

public class ReceiveInfoBean extends BaseResponse {

    /**
     * data : {"pages":1,"total":3,"next":"","prev":"","list":[{"id":19,"no":"HC001-01","customName":"范建海","contractStages":[{"id":23,"contractId":19,"stage":2,"money":43199.7,"orderNo":"CS201806132PWFEOWCWU","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":2,"stagePercent":30,"operator":"","stageTitle":"主体","contractStageDivides":""},{"id":24,"contractId":19,"stage":3,"money":43199.7,"orderNo":"CS201806133PWFEOWCWU","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":3,"stagePercent":30,"operator":"","stageTitle":"软装","contractStageDivides":""}]},{"id":20,"no":"HC001-02","customName":"庄女士","contractStages":[{"id":25,"contractId":20,"stage":1,"money":61198.6,"orderNo":"CS201806131OJONIEFNH","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":1,"stagePercent":40,"operator":"","stageTitle":"隐蔽","contractStageDivides":""},{"id":26,"contractId":20,"stage":2,"money":45899.7,"orderNo":"CS201806132OJONIEFNH","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":2,"stagePercent":30,"operator":"","stageTitle":"主体","contractStageDivides":""},{"id":27,"contractId":20,"stage":3,"money":45899.7,"orderNo":"CS201806133OJONIEFNH","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":3,"stagePercent":30,"operator":"","stageTitle":"软装","contractStageDivides":""}]},{"id":22,"no":"HC001-03","customName":"范建海","contractStages":[{"id":30,"contractId":22,"stage":2,"money":43199.7,"orderNo":"CS201806132IRVEJAVKM","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 11:31:22","updateTime":"2018-06-13 11:31:22","stageId":2,"stagePercent":30,"operator":"","stageTitle":"主体","contractStageDivides":""},{"id":31,"contractId":22,"stage":3,"money":43199.7,"orderNo":"CS201806133IRVEJAVKM","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 11:31:22","updateTime":"2018-06-13 11:31:22","stageId":3,"stagePercent":30,"operator":"","stageTitle":"软装","contractStageDivides":""}]}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ReceiveInfoBean{" +
                "data=" + data +
                '}';
    }

    public static class DataBean {
        /**
         * pages : 1
         * total : 3
         * next :
         * prev :
         * list : [{"id":19,"no":"HC001-01","customName":"范建海","contractStages":[{"id":23,"contractId":19,"stage":2,"money":43199.7,"orderNo":"CS201806132PWFEOWCWU","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":2,"stagePercent":30,"operator":"","stageTitle":"主体","contractStageDivides":""},{"id":24,"contractId":19,"stage":3,"money":43199.7,"orderNo":"CS201806133PWFEOWCWU","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":3,"stagePercent":30,"operator":"","stageTitle":"软装","contractStageDivides":""}]},{"id":20,"no":"HC001-02","customName":"庄女士","contractStages":[{"id":25,"contractId":20,"stage":1,"money":61198.6,"orderNo":"CS201806131OJONIEFNH","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":1,"stagePercent":40,"operator":"","stageTitle":"隐蔽","contractStageDivides":""},{"id":26,"contractId":20,"stage":2,"money":45899.7,"orderNo":"CS201806132OJONIEFNH","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":2,"stagePercent":30,"operator":"","stageTitle":"主体","contractStageDivides":""},{"id":27,"contractId":20,"stage":3,"money":45899.7,"orderNo":"CS201806133OJONIEFNH","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":3,"stagePercent":30,"operator":"","stageTitle":"软装","contractStageDivides":""}]},{"id":22,"no":"HC001-03","customName":"范建海","contractStages":[{"id":30,"contractId":22,"stage":2,"money":43199.7,"orderNo":"CS201806132IRVEJAVKM","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 11:31:22","updateTime":"2018-06-13 11:31:22","stageId":2,"stagePercent":30,"operator":"","stageTitle":"主体","contractStageDivides":""},{"id":31,"contractId":22,"stage":3,"money":43199.7,"orderNo":"CS201806133IRVEJAVKM","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 11:31:22","updateTime":"2018-06-13 11:31:22","stageId":3,"stagePercent":30,"operator":"","stageTitle":"软装","contractStageDivides":""}]}]
         */

        private int pages;
        private int total;
        private String next;
        private String prev;
        private List<ListBean> list;

        @Override
        public String toString() {
            return "DataBean{" +
                    "pages=" + pages +
                    ", total=" + total +
                    ", next='" + next + '\'' +
                    ", prev='" + prev + '\'' +
                    ", list=" + list +
                    '}';
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrev() {
            return prev;
        }

        public void setPrev(String prev) {
            this.prev = prev;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * id : 19
             * no : HC001-01
             * customName : 范建海
             * contractStages : [{"id":23,"contractId":19,"stage":2,"money":43199.7,"orderNo":"CS201806132PWFEOWCWU","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":2,"stagePercent":30,"operator":"","stageTitle":"主体","contractStageDivides":""},{"id":24,"contractId":19,"stage":3,"money":43199.7,"orderNo":"CS201806133PWFEOWCWU","serialNo":"","payTime":"","payStatus":0,"status":1,"completeTime":"","createTime":"2018-06-13 09:49:12","updateTime":"2018-06-13 09:49:12","stageId":3,"stagePercent":30,"operator":"","stageTitle":"软装","contractStageDivides":""}]
             */

            private int id;
            private String no;
            private String customName;
            private List<ContractStagesBean> contractStages;

            @Override
            public String toString() {
                return "ListBean{" +
                        "id=" + id +
                        ", no='" + no + '\'' +
                        ", customName='" + customName + '\'' +
                        ", contractStages=" + contractStages +
                        '}';
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getNo() {
                return no;
            }

            public void setNo(String no) {
                this.no = no;
            }

            public String getCustomName() {
                return customName;
            }

            public void setCustomName(String customName) {
                this.customName = customName;
            }

            public List<ContractStagesBean> getContractStages() {
                return contractStages;
            }

            public void setContractStages(List<ContractStagesBean> contractStages) {
                this.contractStages = contractStages;
            }

            public static class ContractStagesBean {
                /**
                 * id : 23
                 * contractId : 19
                 * stage : 2
                 * money : 43199.7
                 * orderNo : CS201806132PWFEOWCWU
                 * serialNo :
                 * payTime :
                 * payStatus : 0
                 * status : 1
                 * completeTime :
                 * createTime : 2018-06-13 09:49:12
                 * updateTime : 2018-06-13 09:49:12
                 * stageId : 2
                 * stagePercent : 30
                 * operator :
                 * stageTitle : 主体
                 * contractStageDivides :
                 */

                private int id;
                private int contractId;
                private int stage;
                private double money;
                private String orderNo;
                private String serialNo;
                private String payTime;
                private int payStatus;
                private int status;
                private String completeTime;
                private String createTime;
                private String updateTime;
                private int stageId;
                private int stagePercent;
                private String operator;
                private String stageTitle;
                private String contractStageDivides;

                @Override
                public String toString() {
                    return "ContractStagesBean{" +
                            "id=" + id +
                            ", contractId=" + contractId +
                            ", stage=" + stage +
                            ", money=" + money +
                            ", orderNo='" + orderNo + '\'' +
                            ", serialNo='" + serialNo + '\'' +
                            ", payTime='" + payTime + '\'' +
                            ", payStatus=" + payStatus +
                            ", status=" + status +
                            ", completeTime='" + completeTime + '\'' +
                            ", createTime='" + createTime + '\'' +
                            ", updateTime='" + updateTime + '\'' +
                            ", stageId=" + stageId +
                            ", stagePercent=" + stagePercent +
                            ", operator='" + operator + '\'' +
                            ", stageTitle='" + stageTitle + '\'' +
                            ", contractStageDivides='" + contractStageDivides + '\'' +
                            '}';
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getContractId() {
                    return contractId;
                }

                public void setContractId(int contractId) {
                    this.contractId = contractId;
                }

                public int getStage() {
                    return stage;
                }

                public void setStage(int stage) {
                    this.stage = stage;
                }

                public double getMoney() {
                    return money;
                }

                public void setMoney(double money) {
                    this.money = money;
                }

                public String getOrderNo() {
                    return orderNo;
                }

                public void setOrderNo(String orderNo) {
                    this.orderNo = orderNo;
                }

                public String getSerialNo() {
                    return serialNo;
                }

                public void setSerialNo(String serialNo) {
                    this.serialNo = serialNo;
                }

                public String getPayTime() {
                    return payTime;
                }

                public void setPayTime(String payTime) {
                    this.payTime = payTime;
                }

                public int getPayStatus() {
                    return payStatus;
                }

                public void setPayStatus(int payStatus) {
                    this.payStatus = payStatus;
                }

                public int getStatus() {
                    return status;
                }

                public void setStatus(int status) {
                    this.status = status;
                }

                public String getCompleteTime() {
                    return completeTime;
                }

                public void setCompleteTime(String completeTime) {
                    this.completeTime = completeTime;
                }

                public String getCreateTime() {
                    return createTime;
                }

                public void setCreateTime(String createTime) {
                    this.createTime = createTime;
                }

                public String getUpdateTime() {
                    return updateTime;
                }

                public void setUpdateTime(String updateTime) {
                    this.updateTime = updateTime;
                }

                public int getStageId() {
                    return stageId;
                }

                public void setStageId(int stageId) {
                    this.stageId = stageId;
                }

                public int getStagePercent() {
                    return stagePercent;
                }

                public void setStagePercent(int stagePercent) {
                    this.stagePercent = stagePercent;
                }

                public String getOperator() {
                    return operator;
                }

                public void setOperator(String operator) {
                    this.operator = operator;
                }

                public String getStageTitle() {
                    return stageTitle;
                }

                public void setStageTitle(String stageTitle) {
                    this.stageTitle = stageTitle;
                }

                public String getContractStageDivides() {
                    return contractStageDivides;
                }

                public void setContractStageDivides(String contractStageDivides) {
                    this.contractStageDivides = contractStageDivides;
                }
            }
        }
    }
}
