package gemini.example.com.hcpayment.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.ContractListBean;
import gemini.example.com.hcpayment.entity.QueryCustommadeListBean;
import gemini.example.com.hcpayment.entity.TransactionInfo;
import gemini.example.com.hcpayment.presenter.PayContractPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.utils.ToastUtil;
import gemini.example.com.hcpayment.view.PayContractFeeView;

/**
 * Created by Gemini on 2018/6/15.
 * 收取款项界面(个性定制费用、标准)
 */

public class PayContractFeeActivity extends BaseActivity<PayContractFeeView, PayContractPresenter> implements PayContractFeeView {
    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.tv_title)
    TextView titleText;
    @BindView(R.id.payMoneyNum)
    TextView payMoneyNum;
    int flag = 1;//判断哪个界面跳转来的
    private String orderNum;
    private String money;
    @BindView(R.id.customName)
    TextView customName;
    @BindView(R.id.decorateAddress)
    TextView decoratAddress;
    @BindView(R.id.loginMobile)
    TextView loginMobile;
    private int transcationFlag = 0;
    private int type = 2;

    @Override
    public PayContractPresenter createPresenter() {
        return new PayContractPresenter();
    }

    @Override
    public PayContractFeeView createView() {
        return this;
    }

    @Override
    public void init() {
        Intent intent = getIntent();
        if (intent != null) {
            int flag = intent.getIntExtra(SystemArgs.FLAG, 0);
            if (flag == 1) {
                titleText.setText("收取客户个性定制费用");
                QueryCustommadeListBean.DataBean.ListBean listBean = (QueryCustommadeListBean.DataBean.ListBean) intent.getSerializableExtra(SystemArgs.CUSTOMMADELISTBEAN);
                money = listBean.getMoney() + "";
                orderNum = listBean.getOrderNo();
                customName.setText(listBean.getName());
                decoratAddress.setText(listBean.getAddress());
                loginMobile.setText(listBean.getMobile());
                type = 2;
            } else if (flag == 2) {
                type = 3;
                ContractListBean.DataBean.ListBean contractListBean = (ContractListBean.DataBean.ListBean) intent.getSerializableExtra(SystemArgs.CUSTOMMADELISTBEAN);
                int stage = contractListBean.getStage();
                titleText.setText("收取第" + stage + "期费用");
                money = contractListBean.getMoney() + "";
                orderNum = contractListBean.getOrderNo();
                customName.setText(contractListBean.getCustomName());
                decoratAddress.setText(contractListBean.getHouseAddress());
                loginMobile.setText(contractListBean.getMobile());

            }
            payMoneyNum.setText(money);
        }


    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_deposit_surepay;
    }

    @OnClick({R.id.surePay})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.surePay:
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.shengpay.smartpos.shengpaysdk", "com.shengpay.smartpos.shengpaysdk.activity.MainActivity"));
                intent.putExtra("appId", getPackageName());
                intent.putExtra("transName", TRANSACTION);
                intent.putExtra("barcodeType", CREDIT);
                intent.putExtra("orderNoSFT", orderNum);
                String dealMoney = getPresenter().dealMoney(money);
                intent.putExtra("amount", getPresenter().getAmount(dealMoney));
                startActivityForResult(intent, 0);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {

            Bundle extras1 = data.getExtras();
            Set<String> keySet = extras1.keySet();
//            for (String s : keySet) {
//                Log.d(TAG, "onActivityResult: " + s + ":" + extras1.get(s));
//        }
            String qrCodeScanModel = "";
            String paymentType = "";
            String amount = "";
            String authNo = "";
            String cardNo = "";
            String countN = "";
            String terminalId = "";
            String acquire = "";
            String traceNo = "";
            String transId = "";
            String qrOrderNo = "";
            String batchNo = "";
            String merchantId = "";
            String referenceNo = "";
            String cardType = "";
            String operatorId = "";
            String payreason = "";
            String appId = "";
            String issue = "";
            String model = "";
            String transactionPlatform = "";
            String sumAmount = "";
            String errorCode = "";
            String version = "";
            String merchantName = "";
            String cardSecret = "";
            String orderNoSFT = "";
            String accountType = "";
            String packageName = "";
            String answerCode = "";
            String voucherNo = "";
            String transDate = "";
            String transName = "";
            String transTime = "";
            String transType = "";
            String transactionType = "";
            String qrCodeTransactionState = "";
            String barcodeType = "";

//            if (requestCode == Activity.RESULT_OK) {
            // 打印商户信息
            qrCodeScanModel = extras1.getString("qrCodeScanModel");
            paymentType = extras1.getString("paymentType");
            amount = extras1.getString("amount");
            authNo = extras1.getString("authNo");
            cardNo = extras1.getString("cardNo");
            countN = extras1.getString("countN");
            terminalId = extras1.getString("terminalId");
            acquire = extras1.getString("acquire");
            traceNo = extras1.getString("traceNo");
            transId = extras1.getString("transId");
            qrOrderNo = extras1.getString("qrOrderNo");
            batchNo = extras1.getString("batchNo");
            merchantId = extras1.getString("merchantId");
            referenceNo = extras1.getString("referenceNo");
            cardType = extras1.getString("cardType");
            operatorId = extras1.getString("operatorId");
            payreason = extras1.getString("payreason");
            appId = extras1.getString("appId");
            issue = extras1.getString("issue");
            model = extras1.getString("model");
            transactionPlatform = extras1.getString("transactionPlatform");
            sumAmount = extras1.getString("sumAmount");
            errorCode = extras1.getString("errorCode");
            version = extras1.getString("version");
            merchantName = extras1.getString("merchantName");
            cardSecret = extras1.getString("cardSecret");
            orderNoSFT = extras1.getString("orderNoSFT");
            accountType = extras1.getString("accountType");
            packageName = extras1.getString("packageName");
            answerCode = extras1.getString("answerCode");
            voucherNo = extras1.getString("voucherNo");
            transDate = extras1.getString("transDate");
            transName = extras1.getString("transName");
            transTime = extras1.getString("transTime");
            transType = extras1.getString("transType");
            transactionType = extras1.getString("transactionType");
            qrCodeTransactionState = extras1.getString("qrCodeTransactionState");
            barcodeType = extras1.getString("barcodeType");

            switch (resultCode) {
                case Activity.RESULT_OK:
                    getPresenter().checkTransaction(qrCodeScanModel,
                            paymentType, amount, authNo, cardNo, countN, terminalId, acquire, traceNo,
                            transId, qrOrderNo, batchNo, merchantId, referenceNo, cardType, operatorId, payreason,
                            appId, issue, model, transactionPlatform, sumAmount, errorCode, version, merchantName,
                            cardSecret, orderNoSFT, accountType, packageName, answerCode, voucherNo, transDate,
                            transName, transTime, transType, transactionType, qrCodeTransactionState, barcodeType,
                            1,type);
                    break;
                case Activity.RESULT_CANCELED:
                    String reason = data.getStringExtra("reason");
                    ToastUtil.showShortToast(reason);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void callbackTransactionStatus(TransactionInfo transactionInfo) {
        int status = transactionInfo.getData().getPayStatus();
        switch (status) {
            case 0:
                ToastUtil.showShortToast("支付中请稍候");
                break;
            case 1:
                ToastUtil.showShortToast("支付成功");
                Intent intent = new Intent(this, PayDepositSuccessActivity.class);
                intent.putExtra(SystemArgs.PAYINFO, transactionInfo.getData());
                intent.putExtra(SystemArgs.moneyType, type);
                startActivity(intent);
                break;
            case 2:
                ToastUtil.showShortToast("订单号错误");
                break;
            case 3:
                ToastUtil.showShortToast("系统错误");
                break;
            default:
                break;
        }
    }

    //交易类型
    static final String TRANSACTION = "0";
    //交易方式
    static final String CREDIT = "0";
    static final String WECHAT = "1";
    static final String ALIPAY = "2";
    static final String SDP = "3";
    static final String RICHSCAN = "4";
    static final String UNIONPAY = "5";
    static final String SCAN_WX = "6";
    static final String SCAN_ZFB = "7";
}
