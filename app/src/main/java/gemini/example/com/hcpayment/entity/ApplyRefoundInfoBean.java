package gemini.example.com.hcpayment.entity;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/5/31.
 */

public class ApplyRefoundInfoBean  extends BaseResponse {

    /**
     * data : 1
     */

    private int data;

    @Override
    public String toString() {
        return "ApplyRefoundInfoBean{" +
                "data=" + data +
                '}';
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
