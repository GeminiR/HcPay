package gemini.example.com.hcpayment.adapter;

import java.util.List;

import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.entity.QueryCustommadeListBean;

/**
 * Created by Gemini on 2018/5/31.
 */

public class QueryCustommadeListAdapter extends BaseAdapter<QueryCustommadeListBean.DataBean.ListBean,BaseAdapter.BaseViewHolder> {
    public QueryCustommadeListAdapter(List<QueryCustommadeListBean.DataBean.ListBean> data, int layoutId) {
        super(data, layoutId);
    }

    @Override
    void bindViewHolder(BaseAdapter.BaseViewHolder holder, QueryCustommadeListBean.DataBean.ListBean data, int position) {
        holder.setText(R.id.guestInfoName,data.getName());
        holder.setText(R.id.guestInfoPhone,data.getMobile());
        holder.setText(R.id.guestInfoAddress,data.getAddress());
        holder.setText(R.id.guestInfactFee,data.getMoney()+"");
        holder.setText(R.id.orderTime,data.getCreateTime());
        holder.setText(R.id.guestStatus,data.getOrderNo());
    }
}
