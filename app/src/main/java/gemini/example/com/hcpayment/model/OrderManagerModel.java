package gemini.example.com.hcpayment.model;

import android.content.Context;

import gemini.example.com.hcpayment.api.Api;
import gemini.example.com.hcpayment.base.BaseModel;
import gemini.example.com.hcpayment.progress.ObserverResponseListener;

/**
 * Created by Gemini on 2018/6/1.
 */

public class OrderManagerModel extends BaseModel{
    public void queryContractList(String token,int status, int page, ObserverResponseListener observerResponseListener){
        subscribe(Api.getApiSevice().queryContractInfo(token,status,page),observerResponseListener);
    }
}
