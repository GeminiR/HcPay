package gemini.example.com.hcpayment.adapter;

import java.util.List;

import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.entity.ContractListBean;

/**
 * Created by Gemini on 2018/6/12.
 */

public class StandardFeeAdapter extends BaseAdapter<ContractListBean.DataBean.ListBean, BaseAdapter.BaseViewHolder> {
    public StandardFeeAdapter(List<ContractListBean.DataBean.ListBean> data, int layoutId) {
        super(data, layoutId);
    }

    @Override
    void bindViewHolder(BaseAdapter.BaseViewHolder holder, ContractListBean.DataBean.ListBean data, int position) {
        holder.setText(R.id.guestInfoName, data.getCustomName());
        holder.setText(R.id.guestInfoPhone, data.getMobile());
        holder.setText(R.id.guestInfoAddress, data.getHouseAddress());
        holder.setText(R.id.guestInfactFee, data.getMoney() + "");
        holder.setText(R.id.orderTime, data.getCreateTime());
        switch (data.getStatus()) {
            case 1:
                holder.setText(R.id.guestStatus, "待支付");
                break;
            case 2:
                holder.setText(R.id.guestStatus, "支付完成");
                break;
            case 3:
            case 4:
                holder.setText(R.id.guestStatus, "申请退款");
                break;
            case 6:
                holder.setText(R.id.guestStatus, "已冲抵");
                break;
            default:
                break;
        }
    }
}
