package gemini.example.com.hcpayment.entity;

import java.io.Serializable;
import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/5/31.
 */

public class QueryCustommadeListBean extends BaseResponse implements Serializable{

    /**
     * data : {"pages":1,"total":1,"next":"","prev":"","list":[{"id":1,"contractId":2,"money":2000,"orderNo":"SP20180530120317529MCM","payTime":"","payStatus":"","mobile":"","contractNo":"HC001-0000000002","name":"范建海","address":"杭州市西湖区紫霞街188号西溪蝶园-北区","createTime":"2018-05-25 11:38:13.0"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "QueryCustommadeListBean{" +
                "data=" + data +
                '}';
    }

    public static class DataBean implements Serializable{
        /**
         * pages : 1
         * total : 1
         * next :
         * prev :
         * list : [{"id":1,"contractId":2,"money":2000,"orderNo":"SP20180530120317529MCM","payTime":"","payStatus":"","mobile":"","contractNo":"HC001-0000000002","name":"范建海","address":"杭州市西湖区紫霞街188号西溪蝶园-北区","createTime":"2018-05-25 11:38:13.0"}]
         */

        private int pages;
        private int total;
        private String next;
        private String prev;
        private List<ListBean> list;

        @Override
        public String toString() {
            return "DataBean{" +
                    "pages=" + pages +
                    ", total=" + total +
                    ", next='" + next + '\'' +
                    ", prev='" + prev + '\'' +
                    ", list=" + list +
                    '}';
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrev() {
            return prev;
        }

        public void setPrev(String prev) {
            this.prev = prev;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean implements Serializable {
            /**
             * id : 1
             * contractId : 2
             * money : 2000
             * orderNo : SP20180530120317529MCM
             * payTime :
             * payStatus :
             * mobile :
             * contractNo : HC001-0000000002
             * name : 范建海
             * address : 杭州市西湖区紫霞街188号西溪蝶园-北区
             * createTime : 2018-05-25 11:38:13.0
             */

            private int id;
            private int contractId;
            private int money;
            private String orderNo;
            private String payTime;
            private String payStatus;
            private String mobile;
            private String contractNo;
            private String name;
            private String address;
            private String createTime;

            @Override
            public String toString() {
                return "ListBean{" +
                        "id=" + id +
                        ", contractId=" + contractId +
                        ", money=" + money +
                        ", orderNo='" + orderNo + '\'' +
                        ", payTime='" + payTime + '\'' +
                        ", payStatus='" + payStatus + '\'' +
                        ", mobile='" + mobile + '\'' +
                        ", contractNo='" + contractNo + '\'' +
                        ", name='" + name + '\'' +
                        ", address='" + address + '\'' +
                        ", createTime='" + createTime + '\'' +
                        '}';
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getContractId() {
                return contractId;
            }

            public void setContractId(int contractId) {
                this.contractId = contractId;
            }

            public int getMoney() {
                return money;
            }

            public void setMoney(int money) {
                this.money = money;
            }

            public String getOrderNo() {
                return orderNo;
            }

            public void setOrderNo(String orderNo) {
                this.orderNo = orderNo;
            }

            public String getPayTime() {
                return payTime;
            }

            public void setPayTime(String payTime) {
                this.payTime = payTime;
            }

            public String getPayStatus() {
                return payStatus;
            }

            public void setPayStatus(String payStatus) {
                this.payStatus = payStatus;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getContractNo() {
                return contractNo;
            }

            public void setContractNo(String contractNo) {
                this.contractNo = contractNo;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }
        }
    }
}
