package gemini.example.com.hcpayment.entity;

/**
 * Created by Gemini on 2018/5/28.
 */

public class WheelViewBean {
    private String id;
    private String name;

    public WheelViewBean(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "WheelViewBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
