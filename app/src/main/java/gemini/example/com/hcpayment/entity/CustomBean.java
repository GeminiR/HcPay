package gemini.example.com.hcpayment.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

import gemini.example.com.hcpayment.base.BaseResponse;

/**
 * Created by Gemini on 2018/5/9.
 */

public class CustomBean  extends BaseResponse implements Serializable{

    /**
     * data : {"next":"","total":6,"pages":1,"prev":"","list":[{"id":17,"name":"测试3","mobile":"13655222222","avatar":"","no":"2018033000009","manager":"余梦玲","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":21,"name":"测试客户4","mobile":"5","avatar":"","no":"2018032900007","manager":"余梦玲","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":22,"name":"测试客户3","mobile":"3","avatar":"","no":"2018032700005","manager":"余梦玲","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":23,"name":"虞女士","mobile":"15988191140","avatar":"","no":"2017111100022","manager":"刘颜菘","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":18,"name":"庄女士","mobile":"13810426535","avatar":"","no":"2017101700011","manager":"宋春雷","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":24,"name":"范建海","mobile":"15325812129","avatar":"","no":"2017101100001","manager":"刘颜菘","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }


    public static class DataBean  implements Serializable{
        /**
         * next :
         * total : 6
         * pages : 1
         * prev :
         * list : [{"id":17,"name":"测试3","mobile":"13655222222","avatar":"","no":"2018033000009","manager":"余梦玲","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":21,"name":"测试客户4","mobile":"5","avatar":"","no":"2018032900007","manager":"余梦玲","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":22,"name":"测试客户3","mobile":"3","avatar":"","no":"2018032700005","manager":"余梦玲","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":23,"name":"虞女士","mobile":"15988191140","avatar":"","no":"2017111100022","manager":"刘颜菘","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":18,"name":"庄女士","mobile":"13810426535","avatar":"","no":"2017101700011","manager":"宋春雷","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"},{"id":24,"name":"范建海","mobile":"15325812129","avatar":"","no":"2017101100001","manager":"刘颜菘","createTime":"2018-05-17 09:17:15","updateTime":"2018-05-17 09:17:15"}]
         */

        private String next;
        private int total;
        private int pages;
        private String prev;
        private List<ListBean> list;

        @Override
        public String toString() {
            return "DataBean{" +
                    "next='" + next + '\'' +
                    ", total=" + total +
                    ", pages=" + pages +
                    ", prev='" + prev + '\'' +
                    ", list=" + list +
                    '}';
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public String getPrev() {
            return prev;
        }

        public void setPrev(String prev) {
            this.prev = prev;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean implements Serializable{
            /**
             * id : 17
             * name : 测试3
             * mobile : 13655222222
             * avatar :
             * no : 2018033000009
             * manager : 余梦玲
             * createTime : 2018-05-17 09:17:15
             * updateTime : 2018-05-17 09:17:15
             */

            private int id;
            private String name;
            private String mobile;
            private String avatar;
            private String no;
            private String manager;
            private String createTime;
            private String updateTime;
            private String area;
            private String decorationArea;
            private String address;
            private int  customHouseId;

            @Override
            public String toString() {
                return "ListBean{" +
                        "id=" + id +
                        ", name='" + name + '\'' +
                        ", mobile='" + mobile + '\'' +
                        ", avatar='" + avatar + '\'' +
                        ", no='" + no + '\'' +
                        ", manager='" + manager + '\'' +
                        ", createTime='" + createTime + '\'' +
                        ", updateTime='" + updateTime + '\'' +
                        ", area='" + area + '\'' +
                        ", decorationArea='" + decorationArea + '\'' +
                        ", address='" + address + '\'' +
                        ", customHouseId=" + customHouseId +
                        '}';
            }

            public int getCustomHouseId() {
                return customHouseId;
            }

            public void setCustomHouseId(int customHouseId) {
                this.customHouseId = customHouseId;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getDecorationArea() {
                return decorationArea;
            }

            public void setDecorationArea(String decorationArea) {
                this.decorationArea = decorationArea;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }


            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getNo() {
                return no;
            }

            public void setNo(String no) {
                this.no = no;
            }

            public String getManager() {
                return manager;
            }

            public void setManager(String manager) {
                this.manager = manager;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

        }
    }

    @Override
    public String toString() {
        return "CustomBean{" +
                "data=" + data +
                '}';
    }
}
