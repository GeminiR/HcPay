package gemini.example.com.hcpayment.activity;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.BindView;
import gemini.example.com.hcpayment.R;
import gemini.example.com.hcpayment.base.BaseActivity;
import gemini.example.com.hcpayment.entity.QueryCustomBean;
import gemini.example.com.hcpayment.presenter.CustomDetailPresenter;
import gemini.example.com.hcpayment.utils.SystemArgs;
import gemini.example.com.hcpayment.view.CustomDetailView;

/**
 * Created by Gemini on 2018/6/16.
 */

public class CustomDetailActivity extends BaseActivity<CustomDetailView, CustomDetailPresenter> implements CustomDetailView {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView titleText;

    @Override
    public CustomDetailPresenter createPresenter() {
        return new CustomDetailPresenter();
    }

    @Override
    public CustomDetailView createView() {
        return this;
    }

    @Override
    public void init() {
        titleText.setText("客户详情");
        Intent intent=getIntent();
        if(intent!=null){
            QueryCustomBean.DataBean.ListBean listBean= (QueryCustomBean.DataBean.ListBean) intent.getSerializableExtra(SystemArgs.CUSTOMINFO);
            customName.setText(listBean.getName());
            customaddress.setText(listBean.getAddress());
            customManager.setText(listBean.getManager());
            customMobile.setText(listBean.getMobile());
        }

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_customdetail;
    }

    @BindView(R.id.customName)
    TextView customName;
    @BindView(R.id.decorateAddress)
    TextView customaddress;
    @BindView(R.id.registerMobile)
    TextView customMobile;
    @BindView(R.id.customStatus)
    TextView customStatus;
    @BindView(R.id.marketingStore)
    TextView marketingStore;
    @BindView(R.id.customManager)
    TextView customManager;
    @BindView(R.id.decorationArea)
    TextView decorationArea;
}
