package gemini.example.com.hcpayment.view;

import gemini.example.com.hcpayment.base.BaseView;

/**
 * Created by Gemini on 2018/7/12.
 */

public interface ApplyDepositTwoView extends BaseView {
    void callBackRefoundSucccess();
    void callbackRefoundFail();
    void  callbackBankName(String bankName);
}
