package gemini.example.com.hcpayment.presenter;

import android.view.KeyEvent;
import android.widget.TextView;

import gemini.example.com.hcpayment.base.BasePresenter;
import gemini.example.com.hcpayment.view.GatherDepositView;

/**
 * Created by Gemini on 2018/5/30.
 */

public class GatherDepositPresenter extends BasePresenter<GatherDepositView>  implements TextView.OnEditorActionListener{

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        getView().sureInput();
        return true;
    }
}
